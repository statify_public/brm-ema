
#
# Predict which words are supposed to have been processed for each fixation
# given a window whose size can be manipulated (left to fixation, right to fixation)
# and a percentage of minimum overlap (left or right)
# Compute also various indicators that can be added to the fixation file as additional columns :
# - type of saccade (readmode)
# - word frequency
# - cosine with the theme
#
# Beno�t Lemaire, January 2015, 2016, 2017, 2018
use strict;
use diagnostics;
#use locale;

our $VERBOSE=0;     # verbose mode
our $WRITE=0;       # write output into files
our $ADDCOLUMNS=0;  # add columns to the input file and generate a new file
our $THEME=0;       # indicates if there is a theme in the data
our $FIXVALIDITY=1; # indicates if there is a fixation validity column
our $LSA=0;         # indicates whether a LSA cosine file is used (1) or not (0)
our $ONEWORD=1;      # indicates whether a fixation is associated to only one word or several ones

our $fichierVecteurs;
our %dicotxtlsa;    # dictionnary of conversion word->LSA form to save time
my $headerFile="headerForTagging.header";  # fichier des entetes de colonne (une par ligne)

# New columns are :
#  - readmode : -2, -1, 0, 1 or 2 depending on the direction and length of the saccade jump (0 = same word, 1 = next word, 2 = at least one word is skipped...)
#  - rankDiff : same but indicates the amplitude of the jump in number of words
#  - charDiff : same but indicates the amplitude in number of characters
#  - words    : words predicted to have been processed
#  - allwords : all words predicted to have been processed
our @newColumns=("readmode","rankDiff","charDiff","words","allwords","freq","cosind", "coscum");  # bo18
our $columnsFilledLater="/readmode/rankDiff/charDiff/";
our %column;  # array containing the data for each column to be added
our @printLate; # array containing the output lines (fixation file lines + additional columns) to be printed at the end
our %codesEvents=(
    TextType => 10,                      # type of the text
    TextTypeTarget => 20,
    TextTypeAssocie => 21,
    TextTypeIncong => 22,
    LowSemanticLink => 50,              # lien s�mantique faible entre mot et th�me
    MediumSemanticLink => 51,           # lien s�mantique moyen entre mot et th�me
    StrongSemanticLink => 52,           # lien s�mantique fort entre mot et th�me
    FirstStrongSemanticLink => 53,      # premier lien s�mantique fort du paragraphe
    IncongruentSemanticLink => 54,      # mot faiblement associ� au th�me et peu fr�quent (mot sp�cifique susceptible de d�clencher une prise de d�cision)
    IntraIncongruentSemanticLink => 55,      # mot faiblement associ� au th�me et peu fr�quent (mot sp�cifique susceptible de d�clencher une prise de d�cision)
    LowCumulativeSemanticLink => 60, # lien s�mantique moyen entre les mots d�j� vus et le th�me
    MediumCumulativeSemanticLink => 61, # lien s�mantique moyen entre les mots d�j� vus et le th�me
    StrongCumulativeSemanticLink => 62, # lien s�mantique fort entre les mots d�j� vus et le th�me
    JustBeforeSemanticStep =>63,
    JustAfterSemanticStep => 64,
    BeforeSemanticStep => 65,             # fixation arrivant avant un semantic step
    FirstSemanticStep => 66,              # fixation sur un mot produisant un saut s�mantique important en forme de marche
    AfterSemanticStep => 67,              # fixation arrivant apr�s un semantic step
    ThisBeforeSemanticStep => 68,         # fixation BeforeSemanticStep sp�ciale
    ThisAfterSemanticStep => 69,          # fixation AfterSemanticStep sp�ciale
    ShortWord => 70,                    # mot de petite longueur
    LongWord => 71,                     # mot de grande longueur
    NormalDuration => 80,               # fixation de duree normale
    LongDuration => 81,                 # fixation de longue duree
    NormalDurationMode => 82,           # fixation de duree normale tr�s proche du mode de la distribution des durees normales
    LongDurationMode => 83,             # fixation de duree longue tr�s proche du mode de la distribution des durees longues
    TargetWord => 90,                   # fixation sur un mot presque identique a un des mots du theme
    JustBeforeTargetWord => 91,         # fixation juste avant un mot presque identique a un des mots du theme
    JustAfterTargetWord => 92,          # fixation juste apr�s un mot presque identique a un des mots du theme
    Rank1 => 101,                       # premier mot vu dans le paragraphe
    Rank2 => 102,                       # deuxieme mot vu
    Rank3 => 103,                       # troisieme mot vu
    Rank4 => 104,
    Rank5 => 105,
    Rank6 => 106,
    Rank7 => 107,
    Rank8 => 108,
    Rank9 => 109,
    Rank10 => 110,
    Rank11 => 111,
    Rank12 => 112,
    Rank13 => 113,
    Rank14 => 114,
    Rank15 => 115,
    Rank16 => 116,
    Rank17 => 117,
    Rank18 => 118,
    Rank19 => 119,
    Rank20 => 120,
    Word1 => 121,
    Word2 => 122,
    Word3 => 123,
    Word4 => 124,
    Word5 => 125,
    Word6 => 126,
    Word7 => 127,
    Word8 => 128,
    Word9 => 129,
    Word10 => 130,
    RankNMinus10 => 190,                # N-10
    RankNMinus9 => 191,                 # N-9
    RankNMinus8 => 192,                 # N-8
    RankNMinus7 => 193,                 # N-7
    RankNMinus6 => 194,                 # N-6
    RankNMinus5 => 195,                 # N-5
    RankNMinus4 => 196,                 # N-4
    RankNMinus3 => 197,                 # N-3
    RankNMinus2 => 198,                 # avant-avant-dernier mot vu
    RankNMinus1 => 199,                 # avant-dernier mot vu
    RankN => 200,                        # dernier mot vu
    ForwSacShort => 201,
    ForwSacLong => 202,
    BackSacRefixation => 203,
    BackSacRereading => 204,
    BackSacRegrShort => 205,
    BackSacRegrLong => 206,
    Words => 900
    );

our %consignes=(
    aide_refugies => "aide r�fugi�s",
    allocation_familiale => "allocation familiale",
    art_contemporain => "art contemporain",
    associations_humanitaires => "associations humanitaires",
    chasse_oiseaux => "chasse oiseaux",
    chef_russie => "chef russie",
    conflit_irak => "conflit irak",
    conflit_israelo_palestinien => "conflit isra�lo-palestinien",
    course_voiliers => "course voiliers",
    croissance_economie => "croissance �conomie",
    dechets_nucleaires => "d�chets nucl�aires",
    decollage_fusee => "d�collage fus�e",
    delinquance_juvenile => "d�linquance juv�nile",
    faiblesse_dollar => "faiblesse dollar",
    formation_informatique => "formation informatique",
    greve_cheminot => "gr�ve cheminot",
    hausse_bourse => "hausse bourse",
    observation_planetes => "observation plan�tes",
    plantation_fleurs => "plantation fleurs",
    plantation_fleurs => "plantation fleurs",
    prix_petrole => "prix p�trole",
    prix_litteraire => "prix litt�raire",
    rechauffement_climatique => "r�chauffement climatique",
    reforme_enseignement => "r�forme enseignement",
    reforme_justice => "r�forme justice",
    rehabilitation_logement => "r�habilitation logement",
    salon_aeronautique => "salon a�ronautique",
    syndicat_agricole => "syndicat agricole",
    tourisme_montagne => "tourisme montagne",
    tribunal_international => "tribunal international",
    victoire_footballeurs => "victoire footballeurs"
);


our %functionalWords=("�"=>1,"ainsi"=>1,"alors"=>1,"assez"=>1,"au"=>1,"aucun"=>1,"auquel"=>1,"aussi"=>1,"autant"=>1,"aux"=>1,"avec"=>1,"beaucoup"=>1,"car"=>1,"c"=>1,"ce"=>1,"ceci"=>1,"cela"=>1,"celle"=>1,"celui"=>1,"ces"=>1,"cet"=>1,"cette"=>1,"chacun"=>1,"chacune"=>1,"chaque"=>1,"chez"=>1,"ci"=>1,"comme"=>1,"contre"=>1,"d"=>1,"dans"=>1,"de"=>1,"depuis"=>1,"des"=>1,"dessous"=>1,"dessus"=>1,"devant"=>1,"donc"=>1,"dont"=>1,"du"=>1,"duquel"=>1,"d�s"=>1,"d�j�"=>1,"elle"=>1,"elles"=>1,"en"=>1,"encore"=>1,"enfin"=>1,"entre"=>1,"est"=>1,"et"=>1,"eux"=>1,"ici"=>1,"il"=>1,"ils"=>1,"j"=>1,"je"=>1,"jusque"=>1,"l"=>1,"la"=>1,"laquelle"=>1,"le"=>1,"lequel"=>1,"les"=>1,"lesquels"=>1,"leur"=>1,"leurs"=>1,"lors"=>1,"lorsque"=>1,"lui"=>1,"l�"=>1,"ma"=>1,"mais"=>1,"malgr�"=>1,"me"=>1,"mes"=>1,"mieux"=>1,"moi"=>1,"moins"=>1,"mon"=>1,"m�me"=>1,"n"=>1,"ne"=>1,"ni"=>1,"non"=>1,"nos"=>1,"notre"=>1,"nous"=>1,"n�tre"=>1,"on"=>1,"ont"=>1,"ou"=>1,"oui"=>1,"ouo"=>1,"par"=>1,"parce"=>1,"parfois"=>1,"parmi"=>1,"pas"=>1,"pendant"=>1,"peu"=>1,"peut-�tre"=>1,"plus"=>1,"pour"=>1,"pourquoi"=>1,"pourtant"=>1,"presque"=>1,"puis"=>1,"qu"=>1,"quand"=>1,"quant"=>1,"que"=>1,"quel"=>1,"quelque"=>1,"quelques"=>1,"quels"=>1,"qui"=>1,"quoi"=>1,"rien"=>1,"s"=>1,"sa"=>1,"sans"=>1,"se"=>1,"ses"=>1,"seulement"=>1,"si"=>1,"soi"=>1,"son"=>1,"sous"=>1,"souvent"=>1,"sur"=>1,"surtout"=>1,"t"=>1,"ta"=>1,"tant"=>1,"te"=>1,"tel"=>1,"telle"=>1,"telles"=>1,"tels"=>1,"tes"=>1,"toi"=>1,"toujours"=>1,"tous"=>1,"tout"=>1,"trop"=>1,"tr�s"=>1,"tu"=>1,"un"=>1,"une"=>1,"uns"=>1,"vers"=>1,"voici"=>1,"voil�"=>1,"vos"=>1,"votre"=>1,"vous"=>1,"v�tre"=>1,"y"=>1);


sub within {
    # retourne vrai si le point d�fini par les deux premi�res coordonn�es est
# � l'int�rieur du rectangle d�fini par les 4 suivantes
    return (($_[0]>=$_[2]) && ($_[0]<=$_[4]) && ($_[1]>=$_[3]) && ($_[1]<=$_[5]));
}

sub indcol {
# Retourne dans la ligne la valeur correspondant au nom de la colonne
# Exemple : indcol("toto aide_refugies 200 300 159...", "DUREE", \%entete) --> 159
# si le fichier de description de colonnes comporte DUREE en 5e item
# arg1 = ligne
# arg2 = nom de la colonne
# arg3 = hachage
    my @tabl=split(/ +/,$_[0]);
    my ($hachage)=$_[2];
    unless (exists($hachage->{$_[1]})) {
	print ">>>WARNING: No column $_[1] in header file\n";
	return "";
    }
    if ($hachage->{$_[1]} <= $#tabl) {
	return $tabl[$hachage->{$_[1]}];
    }
    else {
	print ">>>WARNING: No column $_[1] in header file\n";
	return "";
    }
}

sub error {
# print an error message and exit
    print "$_[0]\n";
    exit(0);
}

sub distance {
    # euclidian distance between points
    my $x1=shift;
    my $y1=shift;
    my $x2=shift;
    my $y2=shift;
    return(sqrt(($x2-$x1)**2+($y2-$y1)**2));
}

sub proportionOverlap {
    # compute the proportion of segment 1 which belongs to segment 2
    # for instance proportionOverlap(10,20,14,30) is 60% because 6 units out of 10 belongs to (14,30)
    my $al=shift;
    my $ar=shift;
    my $bl=shift;
    my $br=shift;
    if ($ar<$bl) {
	return 0;}
    elsif ($al<$bl) {
	return (($ar-$bl)/($ar-$al));}
    elsif ($ar<$br) {
	return (1);}
    elsif ($al<$br) {
	return(($br-$al)/($ar-$al));}
    else {
	return(0);}
}

sub code {
    # return the number associated with a word string like "subventions~19"
    my $wordString=shift;
    $wordString =~ /~(.+)$/;
    return $1;
}

sub mot {
    # return the word associated with a word string like "subventions~19"
    my $wordString=shift;
    $wordString =~ /^([^~]+)~/;
    my $word=$1;
    return $1;
}

sub tag {
    #
    my $tagName=shift;
    my $timeStamp=shift;
    my $tagNumber;
    my $prefixe;
    if ($tagName =~ /^EXTRATAG/) {
	$tagName =~ /^EXTRATAG_(.+)$/;
	$tagNumber=999;
	$tagName=$1;
	$prefixe="";
    }
    else {
	$prefixe="ES";
	$tagName =~ /^([A-Za-z0-9]+).*$/;
	my $namePrefix=$1;
	unless (exists($codesEvents{$namePrefix})) {
	    error "Unknown tag $tagName";
	}
	$tagNumber=$codesEvents{$namePrefix};
    }
    print "$tagNumber $prefixe$tagName $timeStamp\n" unless ($WRITE);
    print W1 "$tagNumber $prefixe$tagName $timeStamp\n" if ($WRITE) ;

    if ($ADDCOLUMNS) {

	# READ MODE
	my $readMode;
	my $rankDiff;
	my $charDiff;
	if ($tagName =~ /^ForwSacLong\+(.+)\+(.+)$/) {
	    $readMode=2;
	    $rankDiff="+$1";
	    $charDiff=$2;}
	elsif ($tagName eq "ForwSacShort") {
	    $readMode=1;
	    $rankDiff="+1";
	    $charDiff=1;}
	elsif ($tagName eq "BackSacRefixation") {
	    $readMode=0;
	    $rankDiff=0;
	    $charDiff=0;}
	elsif ($tagName eq "BackSacRegrShort") {
	    $readMode=-1;
	    $rankDiff="-1";
	    $charDiff=-1;}
	elsif ($tagName =~ /^BackSacRegrLong\-(.+)\-(.+)$/) {
	    $readMode=-2;
	    $rankDiff="-$1";
	    $charDiff="-$2";}
	else {
	    $readMode="N/A";
  	    $rankDiff="N/A";
  	    $charDiff="N/A";
	}
	$column{"readmode"}=$readMode;
	$column{"rankDiff"}=$rankDiff;
	$column{"charDiff"}=$charDiff;

	# WORDS
	if ($tagName =~ /^WORDS/) {
	    if ($tagName =~ /^WORDS_/) { # words
		if ($tagName =~ /^WORDS_(.+)$/) {
		    $column{"words"}=$1; }
		else {
		    $column{"words"}="N/A";
		}
	    }
	    elsif ($tagName =~ /^WORDSALL_/) { # several words
		if ($tagName =~ /^WORDSALL_(.+)$/) {
		    $column{"allwords"}=$1; }
		else {
		    $column{"allwords"}="N/A";
		}
	    }
	}

	# WORD FREQUENCY
	elsif ($tagName =~ /^WORDFREQ/) {
	    if ($tagName =~ /^WORDFREQ_(.+)$/) {
		$column{"freq"}= $1;}
	    else {
		$column{"freq"}= "N/A";
	    }
	}

	elsif ($tagName =~ /^COSIND/) {
	    if ($tagName =~ /^COSIND_(.+)$/) {
		$column{"cosind"}= $1;}
	    else {
		$column{"cosind"}= "N/A";
	    }
	}

    elsif ($tagName =~ /^COSCUM/) {
	    if ($tagName =~ /^COSCUM_(.+)$/) {
		$column{"coscum"}= $1;}
	    else {
		$column{"coscum"}= "N/A";
	    }
	}

    }
}


sub txt2lsa {
    my $chaine=shift;
    if (exists($dicotxtlsa{$chaine})) {
	return($dicotxtlsa{$chaine});
    }
    else {
	my $chaineInitiale=$chaine;
	#utf8::decode($chaine);
	if ($chaine =~ /^.'(.+)$/) {
	    $chaine=$1;
	}
	$chaine = lc(substr($chaine,0,1)).substr($chaine,1);  # convert into lower case
	if (`egrep "^$chaine " $fichierVecteurs` eq "") { #unknown word in LSA file
	    $chaine=`egrep "^$chaine " motsDesTextesRiche.csv | head -1 | cut -d " " -f3`; # get the lemma
	    chomp $chaine;
	}
	$dicotxtlsa{$chaineInitiale}=$chaine;
	return $chaine;
    }
}


sub vectAdd {
# Additionne deux vecteurs (tableaux) et retourne un vecteur (tableau)
    my $r1=shift;
    my @v1=@$r1;
    my $r2=shift;
    if ($r2 eq "") {
	return \@v1;
    }
    my @v2=@$r2;
    my @v3;
    my $i=0;
    foreach my $e1 (@v1) {
	my $e2=$v2[$i++];
	exit(0) unless (defined $e2);
	push(@v3,$e1+$e2);
    }
    return \@v3;
}

sub vectCos {
# Calcule le cosinus entre les deux vecteurs
    my $r1=shift;
    my $r2=shift;
    my $sommeCarres1=0.0;
    my $sommeCarres2=0.0;
    my $produitCartesien=0.0;
    my $i=0;
    foreach my $e1 (@$r1) {
	my $e2=@$r2[$i++];
	$sommeCarres1+=($e1*$e1);
	$sommeCarres2+=($e2*$e2);
	$produitCartesien+=$e1*$e2;
    }
    return sprintf("%.2f",$produitCartesien/(sqrt($sommeCarres1)*sqrt($sommeCarres2)));
}

sub vectProduitScalaireNorm {
    my $r1=shift;
    my $r2=shift;
    $r1 = vectNormaliser($r1);
    $r2 = vectNormaliser($r2);
    return sprintf("%.2f", vectProduitScalaire($r1, $r2));
}

sub vectNormaliser {
    my $r=shift;
    my @r2 = @$r;
    my @v3;
    my $sommeCarres=vectNorm($r);
    foreach my $e (@r2) {
        push(@v3, $e / $sommeCarres);
    }
    return \@v3;
}

sub vectProduitScalaire {
    my $r1=shift;
    my $r2=shift;
    my $produitCartesien=0.0;
    my $i=0;
    foreach my $e1 (@$r1) {
    	my $e2=@$r2[$i++];
    	$produitCartesien+=$e1*$e2;
    }
    return sprintf("%.2f",$produitCartesien);
}

sub vectNorm {
    my $r=shift;
    my $sommeCarres=0.0;
    foreach my $e (@$r) {
	       $sommeCarres+=($e*$e);
    }
    return sprintf("%.2f",sqrt($sommeCarres));
}

sub printLater {
    # Store data to be printed for a given fixation number. Operation can be "add"
    # or "replace". In the latter case, the string replaces the one indicated by the 3rd parameter
    my $fixationNb=shift;
    my $operation=shift;
    my $toBeReplaced=shift;
    my $string=shift;
#    print("PRINT LATER $fixationNb) $operation $toBeReplaced $string\n");
    $printLate[$fixationNb]="" unless (exists($printLate[$fixationNb]));
    if ($operation eq "add") {
	$printLate[$fixationNb].=$string;
    }
    elsif ($operation eq "replace") {
	$printLate[$fixationNb] =~ s/$toBeReplaced/$string/;
    }
}


###########################################
#                 M A I N                 #
###########################################

if (($#ARGV<9)||($#ARGV>14)) {
    print "Usage : tagFixations2017.prl [-a(dd columns)] [-w(rite output)] [-v(erbose)] [<[-lsa]>] <fixation file> <coordinate file folder> <user name> <text name> <window width Left> <window width Right> <window height> <overlap threshold left> <overlap threshold right> <file suffix> [<LSA vector file>]\n";
    exit(0);}

my $idxParam=0;
if (($ARGV[0] eq "-v") || ($ARGV[1] eq "-v")  || ($ARGV[2] eq "-v") || ($ARGV[3] eq "-v")) {
    $idxParam++;
    $VERBOSE=1;
}

if (($ARGV[0] eq "-a") || ($ARGV[1] eq "-a")  || ($ARGV[2] eq "-a") || ($ARGV[3] eq "-a")) {
    $idxParam++;
    $ADDCOLUMNS=1;
}

if (($ARGV[0] eq "-w") || ($ARGV[1] eq "-w")  || ($ARGV[2] eq "-w")  || ($ARGV[3] eq "-w")) {
    $idxParam++;
    $WRITE=1;
}

if (($ARGV[0] eq "-lsa") || ($ARGV[1] eq "-lsa")  || ($ARGV[2] eq "-lsa")  || ($ARGV[3] eq "-lsa")) {
    $idxParam++;
    $LSA=1;
}

my $fixationFile=$ARGV[0+$idxParam];
my $coordinateFolder=$ARGV[1+$idxParam];
my $userName=$ARGV[2+$idxParam];
my $textName=$ARGV[3+$idxParam];
my $windowWidthLeft=$ARGV[4+$idxParam];
my $windowWidthRight=$ARGV[5+$idxParam];
my $windowHeight=$ARGV[6+$idxParam];
my $overlapThresholdLeft=$ARGV[7+$idxParam];
my $overlapThresholdRight=$ARGV[8+$idxParam];
my $fileSuffix=$ARGV[9+$idxParam];
error "Error: fileSuffix not defined" unless (defined ($fileSuffix));
if ($LSA) {$fichierVecteurs=$ARGV[10+$idxParam];}

error("Overlap threshold left should be between 0 and 1.") if (($overlapThresholdLeft<0) || ($overlapThresholdLeft>1));
error("Overlap threshold right should be between 0 and 1.") if (($overlapThresholdRight<0) || ($overlapThresholdRight>1));

my $outputFile1="data-$userName-$textName-$fileSuffix.tag.txt";
my $outputFile2="data-$userName-$textName-$fileSuffix.msg.txt";
open(W1,">$outputFile1") || die("Erreur sur $outputFile1: $!") if ($WRITE);
open(W2,">$outputFile2") || die("Erreur sur $outputFile2: $!") if ($WRITE);


my (%coordParagX1,%coordParagX2,%coordParagY1,%coordParagY2,%freq);

%dicotxtlsa=();

#-------------------------------------------------
# Stockage des entete de colonnes (fichier header)
#-------------------------------------------------

# On stocke dans le hachage les noms de colonne et leurs num�ros de colonne
unless (-e $headerFile) {
    error("Le fichier d'entete $headerFile n'existe pas...");
}
open(C,$headerFile) || die("Erreur sur $headerFile: $!");

# On reconstruit les fichiers des entetes (extension=.header)
# On stocke dans le hachage les noms de colonne et leurs num�ros de colonne
open(C,$headerFile) || die("Erreur sur $headerFile: $!");
my %entete;
my $cpt=0;
while (my $ligne=<C>) {
    chomp($ligne);
    $entete{$ligne}=$cpt++;
}
close(C);

#-----------------------------------------------------------------
# stockage des coordonnees LSA des vecteurs dans un hachage de tableau
#-----------------------------------------------------------------
my %vecteurs;
my $vecteurBut;
if ($LSA) {
    open(FVEC,$fichierVecteurs) || die("Erreur sur le fichier $fichierVecteurs: $!\n");
    while(my $ligne=<FVEC>) {
	chomp $ligne;
	if ($ligne =~ / /) {
	    (my $mot,my $vecteur)=split(/ +/,$ligne,2);
	    my @tvect=split(/ +/,$vecteur);
	    $vecteurs{$mot}=[ @tvect ];
	}
    }
    close(FVEC);
    $textName=~/^([a-z_]+)\-/;
#    my $but=$consignes{$1};  # does not work for words with accents
    my $but=`grep $1 consignes.txt | cut -d " " -f2-`;
    chomp $but;
    my @tabBut=split(/ +/,$but);
    $vecteurBut="";
    foreach my $unMot (@tabBut) {
	my $motlsa=txt2lsa($unMot);
	if ($vecteurBut eq "") {
	    if (exists($vecteurs{$motlsa})) {
		$vecteurBut=$vecteurs{$motlsa};
	    }
	}
	else {
	    if (exists($vecteurs{$motlsa})) {
		$vecteurBut=vectAdd($vecteurBut,$vecteurs{$motlsa});
	    }
	}
    }
}

#-----------------------------------------------------
# Stockage des coordonn�es des mots et des paragraphes
#-----------------------------------------------------
open(F,"$coordinateFolder/$textName.coord.txt.withFreq.txt") || die("Erreur avec le fichier $coordinateFolder/$textName.coord.txt.withFreq.txt: $!");
my $lastLineY;

# find the Y coordinate of the last line
my $ligne;
while ($ligne=<F>) {
    chomp($ligne);
    (my $mot,my $x,my $y,my $l,my $h,my $f)=split(/ +/,$ligne);
    $lastLineY=$y;
}
close(F);

my $cptmot=0;
my @words=();
my $firstWord=1;   # flag indicating that the first word has not been processed yet
my $firstLineY;    # Y coordinate of the first word
open(F,"$coordinateFolder/$textName.coord.txt.withFreq.txt") || die("Erreur avec le fichier $coordinateFolder/$textName.coord.txt.withFreq.txt: $!");
while ($ligne=<F>) {
    chomp($ligne);
    (my $mot,my $x,my $y,my $l,my $h,my $f)=split(/ +/,$ligne);
    if ($firstWord==1) {
	$firstLineY=$y;  # Y coordinate of the first line
	$firstWord=0;
    }
    if ($y == $firstLineY) {  # the current word is on the first line of the text
	$y-=50;      # extend the height of the first word to catch fixations recorded too high
	$h+=50;
	#$y-=0;      # extend the height of the first word to catch fixations recorded too high
	#$h+=0;
    }
    if ($y == $lastLineY) {  # the current word is on the last line of the text
	$h+=50;
	#$h+=0;
    }
    $coordParagX1{"$mot~$cptmot"}=$x;
    $coordParagY1{"$mot~$cptmot"}=$y;
    $coordParagX2{"$mot~$cptmot"}=$x+$l;
    $coordParagY2{"$mot~$cptmot"}=$y+$h;
    $freq{"$mot~$cptmot"}=$f;
    push @words, "$mot~$cptmot";
    $cptmot++;
}
close(F);

#-----------------------------------------------------
# Mark all words as being not processed yet
#-----------------------------------------------------
my @wordToFix;
foreach my $i (0..@words-1) {
    $wordToFix[$i]="";
}

#-----------------------------------------------------
# Parcours des fixations
#-----------------------------------------------------
my ($x,$y,$text,$user,$timeStamp,$prevTimeStamp,$themeType,$onsetTrigText,$fixValidity);
open(F,$fixationFile) || die("Erreur avec le fichier $fixationFile: $!");
my $cptFix=1;
my $cptWord=1;
my @fixToWords;
my @lastFixations=();    # array containing fixations in reverse order to easily generate the N-1, N-2,... tags
my ($dejavuRefixation,$dejavuRelecture,$dejavuRegression);
#my @timeStampFixations;
my $themeTypeTagged=0;
my @nonValidLines;   # array containing fixation number for lines that are off duration (to be stil printed)
my $wordCumVector="";  # bo18
$timeStamp="N/A";
while ($ligne=<F>) {
    %column=() if ($ADDCOLUMNS); # new line therefore new column values
    chomp($ligne);
    next if ($ligne =~ /^#/);  # comment
    $text=indcol($ligne,"TextName",\%entete);
    next if ($text ne $textName);
    $user=indcol($ligne,"Subject",\%entete);
    next if ($user ne $userName);
    $x=indcol($ligne,"FixX",\%entete);
    error("X coordinate should be a float (found \"$x\")") if ($x !~ /^\-?[0-9]+(\.[0-9]+)?$/);
    $y=indcol($ligne,"FixY",\%entete);
    error("Y coordinate should be a float (found \"$y\")") if ($y !~ /^\-?[0-9]+(\.[0-9]+)?$/);
    $prevTimeStamp=$timeStamp;
    $timeStamp=indcol($ligne,"FixTime",\%entete);
    error("Time stamp should be a number (found \"$timeStamp\")") if ($timeStamp !~ /^[0-9]+$/);
    if ($THEME) {
	$themeType=indcol($ligne,"ThemeType",\%entete);
	$onsetTrigText=indcol($ligne,"OnsetTrigText",\%entete);
	error("Onset trigger should be a number (found \"$onsetTrigText\")") if ($onsetTrigText !~ /^[0-9]+$/);
    }
    if ($FIXVALIDITY) {
	$fixValidity=indcol($ligne,"OffDuration",\%entete);
	error("OffDuration should be 0 or 1 (found $fixValidity)") if (($fixValidity != 0) && ($fixValidity != 1));
	if ($fixValidity == 1) {
	    $nonValidLines[$cptFix]="" unless (exists($nonValidLines[$cptFix]));
	    $nonValidLines[$cptFix].="$ligne\n";
	    next;
	}
    }

    if ($THEME) {
	unless ($themeTypeTagged) {
	    tag("TextType",$onsetTrigText);
	    tag("TextTypeTarget",$onsetTrigText) if ($themeType eq "T");
	    tag("TextTypeAssocie",$onsetTrigText) if ($themeType eq "A");
	    tag("TextTypeIncong",$onsetTrigText) if ($themeType eq "I");
	    $themeTypeTagged=1;
	}
    }

    #$timeStampFixations[$cptFix]=$timeStamp;

    # Size of the current window given the fixation coordinates
    my $x1Window=$x-$windowWidthLeft;
    my $y1Window=$y-$windowHeight/2;
    my $x2Window=$x+$windowWidthRight;
    my $y2Window=$y+$windowHeight/2;

    $dejavuRefixation=0;
    $dejavuRelecture=0;
    $dejavuRegression=0;

    print "######### Fixation $cptFix ($x,$y)###############\n" if (!$WRITE || $VERBOSE);
    print W2 "# $timeStamp Fixation $cptFix ($x,$y):" if ($WRITE);

    $fixToWords[$cptFix]="";
    my $allWords="";   # all words associated to a given fixation. Used only when a single word is stored
    my $wordFound=0;

    # Scan all words to identify which ones are in the current window
    my $distanceMin=999999;
    my $storedwordIsFunctional=0;
    foreach my $k (sort {code($a) <=> code($b)} (keys(%coordParagX1))) {
	# check whether the word is inside the window

	my $overlapY=proportionOverlap($coordParagY1{$k},$coordParagY2{$k},$y1Window,$y2Window);
#	print "$k (".mot($k).") : overlapY = ".$overlapY."\n";
	next if ($overlapY==0);

	my $overlapX=proportionOverlap($coordParagX1{$k},$coordParagX2{$k},$x1Window,$x2Window);
	next if ($overlapX==0);

	print "# Non-zero overlap for word /$k/ [$coordParagX1{$k},$coordParagX2{$k}], center is ".(($coordParagX1{$k}+$coordParagX2{$k})/2)."\n" if ($VERBOSE);
	my $overlapLocation;

	# check if the word overlaps on its left side or its right side
	if  ($coordParagX1{$k} < $x1Window) {
	    $overlapLocation="right";
	}
	else{
	    $overlapLocation="left";
	}
	if ((($overlapLocation eq "left") && ($overlapX > $overlapThresholdLeft)) ||
	    (($overlapLocation eq "right") && ($overlapX > $overlapThresholdRight))) {
	    if ($ONEWORD) {
		if ($functionalWords{txt2lsa(mot($k))} && ($distanceMin!=999999)) {
		    print "   Functional word is ruled out because another one is already considered\n" if ($VERBOSE);
		    next;   # do not keep a functional word if another one is already considered
		}
		# compute distance between fixation and center of the word
		my $d=distance($x,$y,($coordParagX1{$k}+$coordParagX2{$k})/2,($coordParagY1{$k}+$coordParagY2{$k})/2);
		if (($d > $distanceMin) && (($storedwordIsFunctional==0) || ($functionalWords{txt2lsa(mot($k))}))) { # new word is not closer than the previous one
		    print "   Word is further (d=$d) than a previous one\n" if ($VERBOSE);
		    next;
		}
		print "   Word is closer (".$d.") to fixation ($x,$y) than the previous one\n" if ($VERBOSE);
		$distanceMin=$d;
		if ($functionalWords{txt2lsa(mot($k))}) {
		    $storedwordIsFunctional=1;}
		else {
		    $storedwordIsFunctional=0;}
	    }

	    $k=~/\~([0-9]+)/;
	    my $idxWord=$1;
	    if ($ONEWORD) {
		$wordToFix[$idxWord]="$cptFix/";   # replace previous word by new word
		$fixToWords[$cptFix]="$idxWord/";
		$allWords.="$k\_";
	    }
	    else {
 		$wordToFix[$idxWord].="$cptFix/";  # add new word to previous words
		$fixToWords[$cptFix].="$idxWord/";
	    }
	    $wordFound=1;
	    print " $k" unless ($WRITE);
	    print W2 " $k" if ($WRITE);
	    print "# It is inside the window [$x1Window,$x2Window]"  if ($VERBOSE);
	}
	print "\n" if ($VERBOSE);
    }

    print "\n" unless ($WRITE);
    print W2 "\n" if ($WRITE);

    #######################
    # Tag words processed #
    #######################
    my $wordsAsNumbers=$fixToWords[$cptFix];
    my @wordsAsNumbers=split("/",$wordsAsNumbers);
    my $wordsAsLetters="";
    my $wordFreq="";
    foreach my $w (@wordsAsNumbers) {
	my $wal=$words[$w];
	$wordsAsLetters.="$wal\_";
	$wordFreq.=$freq{$wal};
    }
    chop $wordsAsLetters;
    tag("EXTRATAG_WORDS_$wordsAsLetters",$timeStamp);
    tag("EXTRATAG_WORDFREQ_$wordFreq",$timeStamp);

    if ($ONEWORD) {  # still indicates all words in case only one is kept for computing all other data
	chop $allWords if ($allWords ne "");
	tag("EXTRATAG_WORDSALL_$allWords",$timeStamp);
    }

    ######################################
    # Tag cosine between words and theme #
    ######################################
    if ($LSA) {
	my $cos;
    my $cosCum;
	my $vectCumule="";
	my $cosineString="";
    my $cosCumString="";
	my @currentWords=split(/\//, $fixToWords[$cptFix]);
	foreach my $word (@currentWords) {
        my $m = txt2lsa(mot($words[$word]));
	    if (exists($vecteurs{txt2lsa(mot($words[$word]))})) {
		my $wordVector=$vecteurs{txt2lsa(mot($words[$word]))};
		$cos=vectCos($wordVector,$vecteurBut);
        if($wordCumVector eq "") {
            $wordCumVector=$wordVector;
            $cosCum=$cos;
        }
        else {
            $wordCumVector=vectAdd($wordCumVector, $wordVector);
        }
        $cosCum=vectCos($wordCumVector, $vecteurBut);
		if ($vectCumule eq "") {
		    $vectCumule=$wordVector;
		}
		else {
		    $vectCumule=vectAdd($vectCumule,$wordVector);
		}
		$cosineString.="$cos\_";
        $cosCumString.="$cosCum\_";
	    }
	    else {
    		$cosineString.="N/A_";
            $cosCumString.="N/A_";
	    }
	}
	# Tag individual cosines
	chop($cosineString);
    chop($cosCumString);
	tag("EXTRATAG_COSIND_$cosineString",$timeStamp) if ($cosineString);
    tag("EXTRATAG_COSCUM_$cosCumString", $timeStamp) if ($cosCumString);
    # Tag cosine between all words and goal
	if ($vectCumule ne "") {
        $cos=vectCos($vectCumule,$vecteurBut);
	    tag("EXTRATAG_COSGEN_$cos",$timeStamp);
	}

    }

    #####################
    # Tag fixation ranks
    #####################
    if ($cptFix<=20) {
	tag("Rank$cptFix",$timeStamp);
    }
    unshift @lastFixations, $timeStamp;

    ###########################
    # Tag progressive saccade #
    ###########################
    if (($wordFound) && ($cptFix != 0) && (exists($fixToWords[$cptFix-1]))) {
	my @currentWords=split(/\//, $fixToWords[$cptFix]);
	my @previousWords=split(/\//, $fixToWords[$cptFix-1]);
	if ((@currentWords>0) && (@previousWords>0) && ($previousWords[@previousWords-1] < $currentWords[0])) { # current words are later in the text than previous words
	    my $iw=0;
	    while (($iw <= @currentWords-1) && ($wordToFix[$currentWords[$iw]] eq "$cptFix/")) { # while current words are new words
		$iw++;
	    }
	    if ($iw > @currentWords-1) {  # no current words has been processed before
		if ($previousWords[@previousWords-1] +1 == $currentWords[0]) {
		    tag("ForwSacShort",$timeStamp);
		}
		else {
		    my $diff=$currentWords[0]-$previousWords[@previousWords-1];
		    my $sumInBetweenChars=$diff;
		    for(my $i=$previousWords[@previousWords-1]+1;$i<$currentWords[0];$i++) {
			$sumInBetweenChars+=length(mot($words[$i]));
		    }
		    tag("ForwSacLong+$diff+$sumInBetweenChars",$timeStamp);
		}
	    }
	}
    }

    ##################
    # Tag refixation
    ##################
    # Tag if the current fixation processes the exact same words as the previous fixation
    if (($dejavuRefixation==0) && ($wordFound) && ($cptFix>1) && ($fixToWords[$cptFix] eq $fixToWords[$cptFix-1])) {
	tag("BackSacRefixation",$timeStamp);
	$dejavuRefixation=1;
    }

    #################
    # Tag relecture
    #################
    # Tag if the current fixation processes a word that has been processed earlier than the previous one but not in-between (01001 is OK but not 01011)
    if (($dejavuRelecture==0) && ($wordFound)) {
	my @currentWords=split(/\//, $fixToWords[$cptFix]);
	foreach my $word (@currentWords) {
	    next if (($word eq "") || ($dejavuRelecture==1));
	    if (exists($wordToFix[$word])) {  # word has already been processed
		my @previousFixations=split(/\//,$wordToFix[$word]); # get the previous fixations of that word
		if (@previousFixations > 1) {
		    my $previousLastFixation=$previousFixations[@previousFixations-2];
		    if ($cptFix-$previousLastFixation>1) {
			tag("BackSacRereading",$timeStamp);
			$dejavuRelecture=1;
		    }
		}
	    }
	}
    }

    ##################
    # Tag regression
    ##################
    # Tag if the current fixation goes back to a previous part of the text not already processed
    # The current set of words should not intersect with the previous set of words
    if (($dejavuRegression==0) && ($wordFound) && ($cptFix != 0) && (exists($fixToWords[$cptFix-1]))) {
	my @currentWords=split(/\//, $fixToWords[$cptFix]);
	my @previousWords=split(/\//, $fixToWords[$cptFix-1]);
	if ((@currentWords>0) && (@previousWords>0) && ($currentWords[@currentWords-1] < $previousWords[0])) { # current words are earlier in the text than previous words
	    my $iw=0;
	    while (($iw <= @currentWords-1) && ($wordToFix[$currentWords[$iw]] eq "$cptFix/")) { # while current words are new words
		$iw++;
	    }
	    if ($iw > @currentWords-1) {  # no current words has been processed before
		if ($currentWords[@currentWords-1] +1 == $previousWords[0]) {
		    tag("BackSacRegrShort",$timeStamp);
		}
		else {
		    my $diff=$previousWords[0] - $currentWords[@currentWords-1];
		    my $sumInBetweenChars=$diff;
		    for(my $i=$previousWords[0]-1;$i>$currentWords[@currentWords-1];$i--) {
			$sumInBetweenChars+=length(mot($words[$i]));
		    }
		    tag("BackSacRegrLong-$diff-$sumInBetweenChars",$timeStamp);
		}
	    }
	}
    }

    ##############################################
    # ADD EXTRA COLUMNS IN A TO BE PRINTED ARRAY #
    ##############################################
    if ($ADDCOLUMNS) {

	# extra columns are put into a String array to be printed later.

	printLater($cptFix,"add","",$ligne);
	foreach my $nc (@newColumns) { # new column names are considered one by one

	    if ($columnsFilledLater =~ /\/$nc\//) { # for those columns, data is known at the next fixation
    		printLater($cptFix,"add",""," _TOBEREPLACED_"); # therefore, a mark is added to be replaced later
    		if (($cptFix>1) && (exists($column{$nc}))) {  # data is added if it exists
    		    printLater($cptFix-1,"replace","_TOBEREPLACED_",$column{$nc}); # previous mark is replaced
    		}
	    }
	    else {
		if (exists($column{$nc})) {
		    printLater($cptFix,"add",""," $column{$nc}");
		}
		else {
		    printLater($cptFix,"add",""," N/A");
		}
	    }
	}
	printLater($cptFix,"add","","\n");
    }

    $cptFix++;
}


if (@lastFixations>0) {  # There is at least one fixation

    ###################################################
    # Output the fixation file and additional columns #
    ###################################################
    if ($ADDCOLUMNS) {
	open(WEC,">>$fixationFile.extraColumns") || die("Erreur sur fichier des extra colonnes: $!");
	for(my $i=1;$i<(@printLate);$i++) {
	    if (exists($nonValidLines[$i])) {  # off duration lines have to be printed
		print WEC $nonValidLines[$i];
	    }
	    print WEC $printLate[$i];
	}
	if (exists($nonValidLines[(@printLate)])) {  # off duration lines have to be printed
	    print WEC $nonValidLines[(@printLate)];
	}
	close (WEC);
    }

    ######################
    # Tag last fixations #
    ######################
    tag("RankN",shift @lastFixations);
    my $k=1;
    foreach (@lastFixations) {
	my $timeStamp=$_;
	tag("RankNMinus$k",$_);
	$k++;
	last if ($k>10);
    }

    print "-------------------------------------\n"  if ($VERBOSE);
    my $nbProcessed=0;

    #####################
    # Print information #
    #####################
    # Print fixations for each word
    my $lastProcessed=0;  # keep the rank of the last processed word to compute another percentage
    my $debugMSG="";
    print W2 "%%\n" if ($WRITE);
    foreach my $i (0..@words-1) {
	print "# $words[$i] Fixations= \"$wordToFix[$i]\"\n" unless ($WRITE);
	print W2 "# $words[$i] Fixations= \"$wordToFix[$i]\"\n" if ($WRITE);
	if ($wordToFix[$i] eq "") {
	    $debugMSG.=" ".(mot($words[$i]));
	    }
	else {
	    my $tmp=$wordToFix[$i];
	    chop($tmp);
	    $debugMSG.=" ".(uc(mot($words[$i])))."[$tmp]";
	}
	if ($wordToFix[$i] ne "") {

	    ##################
	    # TAG WORD RANKS #
	    ##################
	    my $m=mot($words[$i]);
	    if (($cptWord<=10) && (length($m)>2) && (!(exists($functionalWords{txt2lsa($m)})))) {
		$wordToFix[$i] =~ /^([0-9]+)\//;
		#my $probableFix=$1; # most probable fixation is first one. TO BE CHANGED
		#tag("Word$cptWord",$timeStampFixations[$probableFix]);
		$cptWord++;
	    }
	    $nbProcessed++;
	    $lastProcessed=$i;
	}
    }
    print W2 "%%\n" if ($WRITE);
    print "# $debugMSG\n" unless ($WRITE);
    print W2 "# $debugMSG\n" if ($WRITE);

    # Print words processed for each fixation
#    foreach my $i (1..$cptFix-1) {
#	print "# $i ";
#	my @w=split(/\//,$fixToWords[$i]);
#	foreach my $w (@w) {
#	    print "$words[$w] ";
#	}
#	print "\n";
#    }

    my $percentageOfWordsProcessed=$nbProcessed/@words;
    my $percentageOfWordsReallyProcessed=$nbProcessed/($lastProcessed+1);
    print "# $userName $textName PercentageOfWordsProcessed= $percentageOfWordsProcessed PercentageOfWordsReallyProcessed= $percentageOfWordsReallyProcessed\n";
    print W2 "# $userName $textName PercentageOfWordsProcessed= $percentageOfWordsProcessed PercentageOfWordsReallyProcessed= $percentageOfWordsReallyProcessed\n" if ($WRITE);

    }

close(F);
close(W1) if ($WRITE);
close(W2) if ($WRITE);
