# 195 Fixation 1 (295.1,293.1): Les~0 galeries~1
# 226 Fixation 2 (433.7,275.2): d'anatomie~2
# 121 Fixation 3 (470.3,279.3): d'anatomie~2
# 144 Fixation 4 (580.5,278.7): comparée~3
# 144 Fixation 5 (386.2,269): galeries~1
# 82 Fixation 6 (340.7,270.9): galeries~1
# 150 Fixation 7 (310.4,277.7): Les~0 galeries~1
# 372 Fixation 8 (441,281.4): d'anatomie~2
# 395 Fixation 9 (485.8,284.3): d'anatomie~2
# 284 Fixation 10 (596.4,282.1): comparée~3
# 244 Fixation 11 (682.7,284.9): comparée~3
# 158 Fixation 12 (308,352.4): paléontologie~6
# 277 Fixation 13 (276.6,352.6): paléontologie~6
# 136 Fixation 14 (331.3,354.9): paléontologie~6
# 242 Fixation 15 (486.7,351.4): du~7 musée~8
# 95 Fixation 16 (539.5,348.4): musée~8 national~9
# 192 Fixation 17 (431.5,358): du~7 musée~8
# 222 Fixation 18 (582.9,356.6): national~9
# 256 Fixation 19 (686.9,361.4): d'histoire~10
# 208 Fixation 20 (295.5,419): naturelle~11
# 124 Fixation 21 (414.7,417): sont~12
# 169 Fixation 22 (499.6,416.7): de~13 nouveau~14
# 172 Fixation 23 (604.7,417): ouvertes~15
# 188 Fixation 24 (688.5,425.5): ouvertes~15
# 177 Fixation 25 (305.8,479.4): public~17
# 139 Fixation 26 (285.6,476.7): public~17
# 160 Fixation 27 (370.9,483.7): depuis~18 18~20
# 179 Fixation 28 (466.9,482): le~19 18~20
# 142 Fixation 29 (521.2,483.5): 18~20 décembre~21
# 161 Fixation 30 (641.9,477.1): après~22
# 245 Fixation 31 (715.6,480): après~22 trois~23
# 183 Fixation 32 (313.6,535.1): mois~24
# 172 Fixation 33 (265.9,532.8): mois~24
# 193 Fixation 34 (355.6,542.1): de~25 travaux~26
# 220 Fixation 35 (338.5,378.9): paléontologie~6
# 129 Fixation 36 (350.1,307.3): galeries~1
# 208 Fixation 37 (337.5,273.6): galeries~1
# 195 Fixation 38 (440.2,273.5): d'anatomie~2
%%
# Les~0 Fixations= "7/"
# galeries~1 Fixations= "37/"
# d'anatomie~2 Fixations= "38/"
# comparée~3 Fixations= "11/"
# et~4 Fixations= ""
# de~5 Fixations= ""
# paléontologie~6 Fixations= "35/"
# du~7 Fixations= "17/"
# musée~8 Fixations= "17/"
# national~9 Fixations= "18/"
# d'histoire~10 Fixations= "19/"
# naturelle~11 Fixations= "20/"
# sont~12 Fixations= "21/"
# de~13 Fixations= "22/"
# nouveau~14 Fixations= "22/"
# ouvertes~15 Fixations= "24/"
# au~16 Fixations= ""
# public~17 Fixations= "26/"
# depuis~18 Fixations= "27/"
# le~19 Fixations= "28/"
# 18~20 Fixations= "29/"
# décembre~21 Fixations= "29/"
# après~22 Fixations= "31/"
# trois~23 Fixations= "31/"
# mois~24 Fixations= "33/"
# de~25 Fixations= "34/"
# travaux~26 Fixations= "34/"
%%
#  LES[7] GALERIES[37] D'ANATOMIE[38] COMPARéE[11] et de PALéONTOLOGIE[35] DU[17] MUSéE[17] NATIONAL[18] D'HISTOIRE[19] NATURELLE[20] SONT[21] DE[22] NOUVEAU[22] OUVERTES[24] au PUBLIC[26] DEPUIS[27] LE[28] 18[29] DéCEMBRE[29] APRèS[31] TROIS[31] MOIS[33] DE[34] TRAVAUX[34]
# s17 art_contemporain-m2 PercentageOfWordsProcessed= 0.888888888888889 PercentageOfWordsReallyProcessed= 0.888888888888889
