# 129 Fixation 1 (295,289.6): Le~0 revenu~1
# 160 Fixation 2 (405.5,294.3): moyen~2
# 143 Fixation 3 (483.7,293.4): imposable~3
# 189 Fixation 4 (615.3,287.5): par~4 foyer~5
# 100 Fixation 5 (636.9,291.9): par~4 foyer~5
# 113 Fixation 6 (713.4,287.2): foyer~5 fiscal~6
# 144 Fixation 7 (317.2,332.5): est~7 plus~9
# 170 Fixation 8 (391.9,340.9): plus~9 faible~10
# 166 Fixation 9 (540.3,354.7): des~11 communes~12
# 131 Fixation 10 (365.3,382.6): l'agglomération~14
# 124 Fixation 11 (323.8,382): l'agglomération~14
# 173 Fixation 12 (508.9,399.9): Riches-pauvres~15
# 168 Fixation 13 (579.4,404.5): Riches-pauvres~15
# 130 Fixation 14 (656.5,406.5): on~16
# 118 Fixation 15 (367.3,378): le~8 plus~9
# 227 Fixation 16 (320.3,377.9): est~7 plus~9
# 157 Fixation 17 (313.2,445.8): n'échappe~17
# 124 Fixation 18 (450.8,451.4): pas~18 face-à-face~20
# 154 Fixation 19 (523.3,459.5): face-à-face~20
# 111 Fixation 20 (574.9,464.4): face-à-face~20
# 169 Fixation 21 (669.8,474.3): C'est~21
# 197 Fixation 22 (335.6,492): n'échappe~17
# 174 Fixation 23 (457.6,502.2): pas~18 face-à-face~20
# 204 Fixation 24 (602.8,514): des~27 convulsions~28
# 134 Fixation 25 (644.6,520.3): convulsions~28
# 121 Fixation 26 (724,524.9): convulsions~28
# 200 Fixation 27 (309.8,544.5): qui~23 embrase~24
%%
# Le~0 Fixations= "1/"
# revenu~1 Fixations= "1/"
# moyen~2 Fixations= "2/"
# imposable~3 Fixations= "3/"
# par~4 Fixations= "5/"
# foyer~5 Fixations= "6/"
# fiscal~6 Fixations= "6/"
# est~7 Fixations= "16/"
# le~8 Fixations= "15/"
# plus~9 Fixations= "16/"
# faible~10 Fixations= "8/"
# des~11 Fixations= "9/"
# communes~12 Fixations= "9/"
# de~13 Fixations= ""
# l'agglomération~14 Fixations= "11/"
# Riches-pauvres~15 Fixations= "13/"
# on~16 Fixations= "14/"
# n'échappe~17 Fixations= "22/"
# pas~18 Fixations= "23/"
# au~19 Fixations= ""
# face-à-face~20 Fixations= "23/"
# C'est~21 Fixations= "21/"
# celui~22 Fixations= ""
# qui~23 Fixations= "27/"
# embrase~24 Fixations= "27/"
# la~25 Fixations= ""
# plupart~26 Fixations= ""
# des~27 Fixations= "24/"
# convulsions~28 Fixations= "26/"
# de~29 Fixations= ""
# l'histoire~30 Fixations= ""
%%
#  LE[1] REVENU[1] MOYEN[2] IMPOSABLE[3] PAR[5] FOYER[6] FISCAL[6] EST[16] LE[15] PLUS[16] FAIBLE[8] DES[9] COMMUNES[9] de L'AGGLOMéRATION[11] RICHES-PAUVRES[13] ON[14] N'éCHAPPE[22] PAS[23] au FACE-à-FACE[23] C'EST[21] celui QUI[27] EMBRASE[27] la plupart DES[24] CONVULSIONS[26] de l'histoire
# s08 rehabilitation_logement-m1 PercentageOfWordsProcessed= 0.774193548387097 PercentageOfWordsReallyProcessed= 0.827586206896552
