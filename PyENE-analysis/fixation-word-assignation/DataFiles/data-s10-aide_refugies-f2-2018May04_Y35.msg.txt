# 120 Fixation 1 (289.4,279.6): L'état~0
# 135 Fixation 2 (375.3,273.8): de~1 l'unique~2
# 116 Fixation 3 (426.4,278.2): l'unique~2
# 172 Fixation 4 (530.9,276.9): route~3
# 137 Fixation 5 (636.6,273.2): ne~4 peut~5
# 142 Fixation 6 (693.2,281.3): peut~5 supporter~6
# 133 Fixation 7 (377.8,358.7): afflux~8 simultané~9
# 145 Fixation 8 (419.2,342.5): simultané~9
# 122 Fixation 9 (605.5,330.1): tous~11 réfugiés~13
# 126 Fixation 10 (673.1,328.6): les~12 réfugiés~13
# 171 Fixation 11 (306.5,396.6): Le~14 HCR~15
# 170 Fixation 12 (319.4,399.9): Le~14 HCR~15
# 111 Fixation 13 (439.7,400.1): prévoit~16
# 138 Fixation 14 (546.4,407): campagnes~18
# 200 Fixation 15 (654.6,417.2): d'information~19
%%
# L'état~0 Fixations= "1/"
# de~1 Fixations= "2/"
# l'unique~2 Fixations= "3/"
# route~3 Fixations= "4/"
# ne~4 Fixations= "5/"
# peut~5 Fixations= "6/"
# supporter~6 Fixations= "6/"
# un~7 Fixations= ""
# afflux~8 Fixations= "7/"
# simultané~9 Fixations= "8/"
# de~10 Fixations= ""
# tous~11 Fixations= "9/"
# les~12 Fixations= "10/"
# réfugiés~13 Fixations= "10/"
# Le~14 Fixations= "12/"
# HCR~15 Fixations= "12/"
# prévoit~16 Fixations= "13/"
# des~17 Fixations= ""
# campagnes~18 Fixations= "14/"
# d'information~19 Fixations= "15/"
# de~20 Fixations= ""
# masse~21 Fixations= ""
# dans~22 Fixations= ""
# les~23 Fixations= ""
# camps~24 Fixations= ""
# pour~25 Fixations= ""
# sensibiliser~26 Fixations= ""
# au~27 Fixations= ""
# danger~28 Fixations= ""
# des~29 Fixations= ""
# mines~30 Fixations= ""
%%
#  L'éTAT[1] DE[2] L'UNIQUE[3] ROUTE[4] NE[5] PEUT[6] SUPPORTER[6] un AFFLUX[7] SIMULTANé[8] de TOUS[9] LES[10] RéFUGIéS[10] LE[12] HCR[12] PRéVOIT[13] des CAMPAGNES[14] D'INFORMATION[15] de masse dans les camps pour sensibiliser au danger des mines
# s10 aide_refugies-f2 PercentageOfWordsProcessed= 0.548387096774194 PercentageOfWordsReallyProcessed= 0.85
