# 244 Fixation 1 (403.6,294.4): pluies~1 intenses~2
# 132 Fixation 2 (561.5,291): ont~3 duré~4
# 149 Fixation 3 (645,296.8): duré~4 plusieurs~5
# 189 Fixation 4 (283.9,345.7): heures~6
# 208 Fixation 5 (383.1,347.9): causant~7
# 221 Fixation 6 (511.8,357.9): l'interruption~8
# 136 Fixation 7 (626,366.4): l'interruption~8
# 281 Fixation 8 (298.2,420.1): circulation~11
# 179 Fixation 9 (430.7,415.4): et~12 isolant~13
# 157 Fixation 10 (504.2,414.7): isolant~13
# 161 Fixation 11 (640.4,416.8): certain~15
# 144 Fixation 12 (712.9,416.7): nombre~16
# 133 Fixation 13 (299.2,478.5): régions~18
# 90 Fixation 14 (305.3,472): régions~18
# 195 Fixation 15 (396.8,461.4): Le~19 bureau~20
# 210 Fixation 16 (523.4,470.9): prévoit~21
# 137 Fixation 17 (312.3,532.7): de~24 dérèglements~26
# 166 Fixation 18 (372,533.7): ces~25 dérèglements~26
# 128 Fixation 19 (544.1,545): climatiques~27
# 120 Fixation 20 (617.4,549.9): climatiques~27
# 149 Fixation 21 (712.7,553.9): au~28 niveau~29
# 142 Fixation 22 (293.9,613.7): de~30 planète~32
%%
# Les~0 Fixations= ""
# pluies~1 Fixations= "1/"
# intenses~2 Fixations= "1/"
# ont~3 Fixations= "2/"
# duré~4 Fixations= "3/"
# plusieurs~5 Fixations= "3/"
# heures~6 Fixations= "4/"
# causant~7 Fixations= "5/"
# l'interruption~8 Fixations= "7/"
# de~9 Fixations= ""
# la~10 Fixations= ""
# circulation~11 Fixations= "8/"
# et~12 Fixations= "9/"
# isolant~13 Fixations= "10/"
# un~14 Fixations= ""
# certain~15 Fixations= "11/"
# nombre~16 Fixations= "12/"
# de~17 Fixations= ""
# régions~18 Fixations= "14/"
# Le~19 Fixations= "15/"
# bureau~20 Fixations= "15/"
# prévoit~21 Fixations= "16/"
# une~22 Fixations= ""
# augmentation~23 Fixations= ""
# de~24 Fixations= "17/"
# ces~25 Fixations= "18/"
# dérèglements~26 Fixations= "18/"
# climatiques~27 Fixations= "20/"
# au~28 Fixations= "21/"
# niveau~29 Fixations= "21/"
# de~30 Fixations= "22/"
# la~31 Fixations= ""
# planète~32 Fixations= "22/"
%%
#  Les PLUIES[1] INTENSES[1] ONT[2] DURé[3] PLUSIEURS[3] HEURES[4] CAUSANT[5] L'INTERRUPTION[7] de la CIRCULATION[8] ET[9] ISOLANT[10] un CERTAIN[11] NOMBRE[12] de RéGIONS[14] LE[15] BUREAU[15] PRéVOIT[16] une augmentation DE[17] CES[18] DéRèGLEMENTS[18] CLIMATIQUES[20] AU[21] NIVEAU[21] DE[22] la PLANèTE[22]
# s02 rechauffement_climatique-m1 PercentageOfWordsProcessed= 0.757575757575758 PercentageOfWordsReallyProcessed= 0.757575757575758
