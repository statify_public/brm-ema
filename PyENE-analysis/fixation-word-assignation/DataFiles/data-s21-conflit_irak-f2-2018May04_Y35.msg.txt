# 183 Fixation 1 (319.2,238): L'Irak~0 a~1
# 141 Fixation 2 (280.2,233.6): L'Irak~0
# 189 Fixation 3 (450.3,288.9): demandé~2 à~3
# 212 Fixation 4 (567.2,332): et~9 britanniques~10
# 152 Fixation 5 (627,344.8): britanniques~10
# 228 Fixation 6 (471.5,397.9): ses~15 avions~16
# 144 Fixation 7 (551,394.4): avions~16 civils~17
# 217 Fixation 8 (344,465.9): transporter~19
# 162 Fixation 9 (577.9,447.6): scientifiques~21
# 145 Fixation 10 (690.9,450.7): étrangers~22
# 302 Fixation 11 (337.5,551.6): L'Irak~23
# 174 Fixation 12 (565.4,548.3): un~27 embargo~28
# 167 Fixation 13 (318.2,587.4): depuis~30 1990~31
%%
# L'Irak~0 Fixations= "2/"
# a~1 Fixations= "1/"
# demandé~2 Fixations= "3/"
# à~3 Fixations= "3/"
# l'ONU~4 Fixations= ""
# d'empêcher~5 Fixations= ""
# les~6 Fixations= ""
# survols~7 Fixations= ""
# américains~8 Fixations= ""
# et~9 Fixations= "4/"
# britanniques~10 Fixations= "5/"
# et~11 Fixations= ""
# de~12 Fixations= ""
# permettre~13 Fixations= ""
# à~14 Fixations= ""
# ses~15 Fixations= "6/"
# avions~16 Fixations= "7/"
# civils~17 Fixations= "7/"
# de~18 Fixations= ""
# transporter~19 Fixations= "8/"
# des~20 Fixations= ""
# scientifiques~21 Fixations= "9/"
# étrangers~22 Fixations= "10/"
# L'Irak~23 Fixations= "11/"
# est~24 Fixations= ""
# soumis~25 Fixations= ""
# à~26 Fixations= ""
# un~27 Fixations= "12/"
# embargo~28 Fixations= "12/"
# aérien~29 Fixations= ""
# depuis~30 Fixations= "13/"
# 1990~31 Fixations= "13/"
%%
#  L'IRAK[2] A[1] DEMANDé[3] à[3] l'ONU d'empêcher les survols américains ET[4] BRITANNIQUES[5] et de permettre à SES[6] AVIONS[7] CIVILS[7] de TRANSPORTER[8] des SCIENTIFIQUES[9] éTRANGERS[10] L'IRAK[11] est soumis à UN[12] EMBARGO[12] aérien DEPUIS[13] 1990[13]
# s21 conflit_irak-f2 PercentageOfWordsProcessed= 0.53125 PercentageOfWordsReallyProcessed= 0.53125
