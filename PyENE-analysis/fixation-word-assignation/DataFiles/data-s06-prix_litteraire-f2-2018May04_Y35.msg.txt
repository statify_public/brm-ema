# 122 Fixation 1 (284.6,298.4): Destiné~0
# 154 Fixation 2 (416.2,299.7): à~1 récompenser~2
# 142 Fixation 3 (618.1,295.9): une~3 oeuvre~4
# 103 Fixation 4 (598.6,296): une~3 oeuvre~4
# 231 Fixation 5 (716.9,297.7): de~5 fiction~6
# 147 Fixation 6 (273.2,336.6): inédite~7
# 144 Fixation 7 (248.2,339.8): inédite~7
# 149 Fixation 8 (305.4,339.9): inédite~7
# 231 Fixation 9 (394.3,342.8): en~8 prose~9
# 306 Fixation 10 (510.5,350.4): écrite~10
# 135 Fixation 11 (621.4,356.7): par~11 auteur~13
# 199 Fixation 12 (680.3,358.6): un~12 auteur~13
# 232 Fixation 13 (265.9,382.2): moins~15
%%
# Destiné~0 Fixations= "1/"
# à~1 Fixations= "2/"
# récompenser~2 Fixations= "2/"
# une~3 Fixations= "4/"
# oeuvre~4 Fixations= "4/"
# de~5 Fixations= "5/"
# fiction~6 Fixations= "5/"
# inédite~7 Fixations= "8/"
# en~8 Fixations= "9/"
# prose~9 Fixations= "9/"
# écrite~10 Fixations= "10/"
# par~11 Fixations= "11/"
# un~12 Fixations= "12/"
# auteur~13 Fixations= "12/"
# de~14 Fixations= ""
# moins~15 Fixations= "13/"
# de~16 Fixations= ""
# vingt-cinq~17 Fixations= ""
# ans~18 Fixations= ""
# le~19 Fixations= ""
# quinzième~20 Fixations= ""
# prix~21 Fixations= ""
# du~22 Fixations= ""
# jeune~23 Fixations= ""
# écrivain~24 Fixations= ""
# a~25 Fixations= ""
# été~26 Fixations= ""
# décerné~27 Fixations= ""
# à~28 Fixations= ""
# Bernard~29 Fixations= ""
# Magnan~30 Fixations= ""
%%
#  DESTINé[1] à[2] RéCOMPENSER[2] UNE[4] OEUVRE[4] DE[5] FICTION[5] INéDITE[8] EN[9] PROSE[9] éCRITE[10] PAR[11] UN[12] AUTEUR[12] de MOINS[13] de vingt-cinq ans le quinzième prix du jeune écrivain a été décerné à Bernard Magnan
# s06 prix_litteraire-f2 PercentageOfWordsProcessed= 0.483870967741935 PercentageOfWordsReallyProcessed= 0.9375
