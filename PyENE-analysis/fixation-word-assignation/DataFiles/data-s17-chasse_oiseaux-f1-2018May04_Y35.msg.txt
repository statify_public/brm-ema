# 94 Fixation 1 (302.3,261.1): Dans~0 Pas-de-Calais~2
# 153 Fixation 2 (372.4,257): le~1 Pas-de-Calais~2
# 140 Fixation 3 (452.1,259.3): Pas-de-Calais~2
# 173 Fixation 4 (589.7,248): les~3 chasseurs~4
# 112 Fixation 5 (636.7,251.2): chasseurs~4
# 189 Fixation 6 (317.7,331.3): réclament~5
# 92 Fixation 7 (437,307.9): Pas-de-Calais~2
# 228 Fixation 8 (279,331.9): réclament~5
# 164 Fixation 9 (439.5,314.3): Pas-de-Calais~2
# 172 Fixation 10 (294.6,342.9): réclament~5
# 183 Fixation 11 (416.6,322): le~6 droit~7
# 254 Fixation 12 (558.3,306.4): les~3 chasseurs~4
# 239 Fixation 13 (698.6,322.5): à~10 chasser~11
# 167 Fixation 14 (324.7,398.9): dans~12 huttes~14
# 149 Fixation 15 (401.6,405.7): huttes~14
# 158 Fixation 16 (563.3,396.7): France~16
# 87 Fixation 17 (541.4,405.3): La~15 France~16
# 144 Fixation 18 (647.3,409.1): risque~17
# 103 Fixation 19 (717.5,407.6): pourtant~18
# 197 Fixation 20 (324.7,457.1): une~19 lourde~20
# 134 Fixation 21 (427.7,458.5): amende~21
# 141 Fixation 22 (625.9,464): contrevient~24
# 84 Fixation 23 (704.8,459.4): contrevient~24
# 153 Fixation 24 (350.4,524.3): directives~26
# 105 Fixation 25 (686.7,524.1): protègent~29
# 155 Fixation 26 (358.8,587): oiseaux~31 migrateurs~32
%%
# Dans~0 Fixations= "1/"
# le~1 Fixations= "2/"
# Pas-de-Calais~2 Fixations= "9/"
# les~3 Fixations= "12/"
# chasseurs~4 Fixations= "12/"
# réclament~5 Fixations= "10/"
# le~6 Fixations= "11/"
# droit~7 Fixations= "11/"
# de~8 Fixations= ""
# continuer~9 Fixations= ""
# à~10 Fixations= "13/"
# chasser~11 Fixations= "13/"
# dans~12 Fixations= "14/"
# les~13 Fixations= ""
# huttes~14 Fixations= "15/"
# La~15 Fixations= "17/"
# France~16 Fixations= "17/"
# risque~17 Fixations= "18/"
# pourtant~18 Fixations= "19/"
# une~19 Fixations= "20/"
# lourde~20 Fixations= "20/"
# amende~21 Fixations= "21/"
# si~22 Fixations= ""
# elle~23 Fixations= ""
# contrevient~24 Fixations= "23/"
# aux~25 Fixations= ""
# directives~26 Fixations= "24/"
# européennes~27 Fixations= ""
# qui~28 Fixations= ""
# protègent~29 Fixations= "25/"
# les~30 Fixations= ""
# oiseaux~31 Fixations= "26/"
# migrateurs~32 Fixations= "26/"
%%
#  DANS[1] LE[2] PAS-DE-CALAIS[9] LES[12] CHASSEURS[12] RéCLAMENT[10] LE[11] DROIT[11] de continuer à[13] CHASSER[13] DANS[14] les HUTTES[15] LA[17] FRANCE[17] RISQUE[18] POURTANT[19] UNE[20] LOURDE[20] AMENDE[21] si elle CONTREVIENT[23] aux DIRECTIVES[24] européennes qui PROTèGENT[25] les OISEAUX[26] MIGRATEURS[26]
# s17 chasse_oiseaux-f1 PercentageOfWordsProcessed= 0.727272727272727 PercentageOfWordsReallyProcessed= 0.727272727272727
