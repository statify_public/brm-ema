# 145 Fixation 1 (259,298.3): Proche-orient~0
# 161 Fixation 2 (315.5,297.6): Proche-orient~0
# 99 Fixation 3 (342.5,302.6): Proche-orient~0
# 148 Fixation 4 (445.2,299.1): le~1 chef~2
# 133 Fixation 5 (500.9,296.5): le~1 chef~2
# 186 Fixation 6 (584.9,303): chef~2 gouvernement~4
# 216 Fixation 7 (291.7,344.8): promis~6
# 216 Fixation 8 (401.7,344.6): de~7 retirer~8
# 197 Fixation 9 (518.1,352): l'armée~9
# 254 Fixation 10 (612.4,358.1): israélienne~10
# 137 Fixation 11 (698.2,358.9): israélienne~10
# 177 Fixation 12 (348.7,400.4): Liban~12 sud~13
# 92 Fixation 13 (305.9,403.5): Liban~12 sud~13
# 203 Fixation 14 (270.8,407.4): Liban~12
# 166 Fixation 15 (342.5,409): Liban~12 sud~13
# 157 Fixation 16 (456.3,415.6): dans~14 délai~16
# 188 Fixation 17 (493,415.6): un~15 délai~16
# 214 Fixation 18 (582.5,417.9): délai~16 an~18
# 144 Fixation 19 (652.8,421.9): d'un~17 an~18
# 143 Fixation 20 (350.6,457.7): envisage~20
# 189 Fixation 21 (304.7,463.5): envisage~20
# 212 Fixation 22 (445.1,462.8): de~21 négocier~22
# 287 Fixation 23 (545,471.1): simultanément~23
# 128 Fixation 24 (594.3,520.4): un~29 dispositif~30
# 98 Fixation 25 (597.5,536): un~29 dispositif~30
# 178 Fixation 26 (721.4,478): avec~24
# 111 Fixation 27 (417.1,550.5): entretient~28
# 120 Fixation 28 (376.6,578.1): militaire~31 propice~32
# 98 Fixation 29 (376.3,592.9): militaire~31 propice~32
# 165 Fixation 30 (281.8,607.7): militaire~31
# 261 Fixation 31 (279.4,528.9): Syrie~26
# 149 Fixation 32 (461.2,522.9): entretient~28
# 252 Fixation 33 (603.2,541.9): dispositif~30
# 177 Fixation 34 (671.7,613.4): avec~35 Israël~36
%%
# Proche-orient~0 Fixations= "3/"
# le~1 Fixations= "5/"
# chef~2 Fixations= "6/"
# du~3 Fixations= ""
# gouvernement~4 Fixations= "6/"
# a~5 Fixations= ""
# promis~6 Fixations= "7/"
# de~7 Fixations= "8/"
# retirer~8 Fixations= "8/"
# l'armée~9 Fixations= "9/"
# israélienne~10 Fixations= "11/"
# du~11 Fixations= ""
# Liban~12 Fixations= "15/"
# sud~13 Fixations= "15/"
# dans~14 Fixations= "16/"
# un~15 Fixations= "17/"
# délai~16 Fixations= "18/"
# d'un~17 Fixations= "19/"
# an~18 Fixations= "19/"
# Il~19 Fixations= ""
# envisage~20 Fixations= "21/"
# de~21 Fixations= "22/"
# négocier~22 Fixations= "22/"
# simultanément~23 Fixations= "23/"
# avec~24 Fixations= "26/"
# la~25 Fixations= ""
# Syrie~26 Fixations= "31/"
# qui~27 Fixations= ""
# entretient~28 Fixations= "32/"
# un~29 Fixations= "25/"
# dispositif~30 Fixations= "33/"
# militaire~31 Fixations= "30/"
# propice~32 Fixations= "29/"
# au~33 Fixations= ""
# conflit~34 Fixations= ""
# avec~35 Fixations= "34/"
# Israël~36 Fixations= "34/"
%%
#  PROCHE-ORIENT[3] LE[5] CHEF[6] du GOUVERNEMENT[6] a PROMIS[7] DE[8] RETIRER[8] L'ARMéE[9] ISRAéLIENNE[11] du LIBAN[15] SUD[15] DANS[16] UN[17] DéLAI[18] D'UN[19] AN[19] Il ENVISAGE[21] DE[22] NéGOCIER[22] SIMULTANéMENT[23] AVEC[26] la SYRIE[31] qui ENTRETIENT[32] UN[25] DISPOSITIF[33] MILITAIRE[30] PROPICE[29] au conflit AVEC[34] ISRAëL[34]
# s05 conflit_israelo_palestinien-f2 PercentageOfWordsProcessed= 0.783783783783784 PercentageOfWordsReallyProcessed= 0.783783783783784
