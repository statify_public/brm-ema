# 138 Fixation 1 (300.4,299.8): Facile~0 à~1
# 170 Fixation 2 (403.6,299.1): à~1 cultiver~2
# 172 Fixation 3 (521.9,294.8): décorative~3
# 117 Fixation 4 (565.9,289.6): décorative~3
# 148 Fixation 5 (666.5,279.2): variée~4
# 170 Fixation 6 (378.3,347.7): à~1 cultiver~2 tellement~6 savoureuse~7
# 129 Fixation 7 (483.9,348.2): cultiver~2 décorative~3 savoureuse~7
# 144 Fixation 8 (569.6,351.8): quand~8 cueille~11 tomate~17
# 174 Fixation 9 (673.6,352.8): on~9 cueille~11
# 275 Fixation 10 (487,403): savoureuse~7 tomate~17
# 181 Fixation 11 (626,417.6): est~18 incontournable~19
# 401 Fixation 12 (681.4,419.4): incontournable~19
# 228 Fixation 13 (656,475.7): incontournable~19 centrale~24
# 136 Fixation 14 (625.5,518.5): d'Amérique~23 centrale~24
# 103 Fixation 15 (717.4,523.2): centrale~24 bonne~34
# 196 Fixation 16 (330.5,544.6): faut~26 fournir~28
# 147 Fixation 17 (377.5,430.4): le~14 pied~15
# 168 Fixation 18 (497,433.9): la~16 tomate~17
%%
# Facile~0 Fixations= "1/"
# à~1 Fixations= "6/"
# cultiver~2 Fixations= "7/"
# décorative~3 Fixations= "7/"
# variée~4 Fixations= "5/"
# et~5 Fixations= ""
# tellement~6 Fixations= "6/"
# savoureuse~7 Fixations= "10/"
# quand~8 Fixations= "8/"
# on~9 Fixations= "9/"
# la~10 Fixations= ""
# cueille~11 Fixations= "9/"
# à~12 Fixations= ""
# même~13 Fixations= ""
# le~14 Fixations= "17/"
# pied~15 Fixations= "17/"
# la~16 Fixations= "18/"
# tomate~17 Fixations= "18/"
# est~18 Fixations= "11/"
# incontournable~19 Fixations= "13/"
# au~20 Fixations= ""
# potager~21 Fixations= ""
# Venue~22 Fixations= ""
# d'Amérique~23 Fixations= "14/"
# centrale~24 Fixations= "15/"
# il~25 Fixations= ""
# faut~26 Fixations= "16/"
# lui~27 Fixations= ""
# fournir~28 Fixations= "16/"
# un~29 Fixations= ""
# sol~30 Fixations= ""
# riche~31 Fixations= ""
# et~32 Fixations= ""
# une~33 Fixations= ""
# bonne~34 Fixations= "15/"
# dose~35 Fixations= ""
# de~36 Fixations= ""
# soleil~37 Fixations= ""
%%
#  FACILE[1] à[6] CULTIVER[7] DéCORATIVE[7] VARIéE[5] et TELLEMENT[6] SAVOUREUSE[10] QUAND[8] ON[9] la CUEILLE[9] à même LE[17] PIED[17] LA[18] TOMATE[18] EST[11] INCONTOURNABLE[13] au potager Venue D'AMéRIQUE[14] CENTRALE[15] il FAUT[16] lui FOURNIR[16] un sol riche et une BONNE[15] dose de soleil
# s08 formation_informatique-a2 PercentageOfWordsProcessed= 0.552631578947368 PercentageOfWordsReallyProcessed= 0.6
