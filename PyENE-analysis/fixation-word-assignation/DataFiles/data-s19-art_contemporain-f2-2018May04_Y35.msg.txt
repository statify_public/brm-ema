# 150 Fixation 1 (313,276.5): Le~0 musée~1
# 157 Fixation 2 (415.4,269.7): d'art~2
# 159 Fixation 3 (598.3,268.6): de~4 ville~6
# 157 Fixation 4 (721.8,279.9): ville~6 Paris~8
# 87 Fixation 5 (319.6,338.9): présente~9
# 120 Fixation 6 (281.7,356.6): présente~9
# 131 Fixation 7 (452.8,362.2): jusqu'au~10 18~11
# 239 Fixation 8 (665.7,345.7): soixante-neuf~13
# 104 Fixation 9 (347.6,407.3): peintures~14
# 141 Fixation 10 (262.3,412.8): peintures~14
# 141 Fixation 11 (549.3,411.9): Rothko~17
# 137 Fixation 12 (666.2,400.7): des~19 grands~20
# 138 Fixation 13 (369.7,505.3): classiques~21
# 116 Fixation 14 (315.4,538.3): l'après-guerre~27
# 82 Fixation 15 (530.3,525.9): aux~28 Etats-unis~29
%%
# Le~0 Fixations= "1/"
# musée~1 Fixations= "1/"
# d'art~2 Fixations= "2/"
# moderne~3 Fixations= ""
# de~4 Fixations= "3/"
# la~5 Fixations= ""
# ville~6 Fixations= "4/"
# de~7 Fixations= ""
# Paris~8 Fixations= "4/"
# présente~9 Fixations= "6/"
# jusqu'au~10 Fixations= "7/"
# 18~11 Fixations= "7/"
# avril~12 Fixations= ""
# soixante-neuf~13 Fixations= "8/"
# peintures~14 Fixations= "10/"
# de~15 Fixations= ""
# Mark~16 Fixations= ""
# Rothko~17 Fixations= "11/"
# un~18 Fixations= ""
# des~19 Fixations= "12/"
# grands~20 Fixations= "12/"
# classiques~21 Fixations= "13/"
# de~22 Fixations= ""
# la~23 Fixations= ""
# peinture~24 Fixations= ""
# abstraite~25 Fixations= ""
# de~26 Fixations= ""
# l'après-guerre~27 Fixations= "14/"
# aux~28 Fixations= "15/"
# Etats-unis~29 Fixations= "15/"
%%
#  LE[1] MUSéE[1] D'ART[2] moderne DE[3] la VILLE[4] de PARIS[4] PRéSENTE[6] JUSQU'AU[7] 18[7] avril SOIXANTE-NEUF[8] PEINTURES[10] de Mark ROTHKO[11] un DES[12] GRANDS[12] CLASSIQUES[13] de la peinture abstraite de L'APRèS-GUERRE[14] AUX[15] ETATS-UNIS[15]
# s19 art_contemporain-f2 PercentageOfWordsProcessed= 0.6 PercentageOfWordsReallyProcessed= 0.6
