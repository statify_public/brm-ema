# 152 Fixation 1 (247.7,288.4): La~0 pianiste~1
# 483 Fixation 2 (281.5,284.9): La~0 pianiste~1
# 88 Fixation 3 (306.8,288.3): La~0 pianiste~1
# 246 Fixation 4 (413.4,287.8): pianiste~1 américaine~2
# 158 Fixation 5 (531.4,288.6): américaine~2 Rosalyn~3
# 128 Fixation 6 (561.6,291.6): Rosalyn~3
# 182 Fixation 7 (648,289.4): Rosalyn~3 Tureck~4
# 204 Fixation 8 (711.4,288.4): Tureck~4 fait~5
# 160 Fixation 9 (313.9,343.6): La~0 pianiste~1 entrée~7
# 131 Fixation 10 (287.1,337.7): La~0 pianiste~1 entrée~7
# 170 Fixation 11 (430.2,341.3): américaine~2 à~8
# 146 Fixation 12 (480.6,345.1): américaine~2 quatre-vingt-quatre~9
# 103 Fixation 13 (520.1,344.9): américaine~2 Rosalyn~3 quatre-vingt-quatre~9
# 204 Fixation 14 (610.2,353.1): quatre-vingt-quatre~9
# 137 Fixation 15 (690.9,356.1): ans~10
# 123 Fixation 16 (725.9,354.9): ans~10
# 176 Fixation 17 (370.6,399.2): entrée~7 à~8 répertoire~13
# 170 Fixation 18 (319.9,397.9): son~6 entrée~7 répertoire~13
%%
# La~0 Fixations= "10/"
# pianiste~1 Fixations= "10/"
# américaine~2 Fixations= "13/"
# Rosalyn~3 Fixations= "13/"
# Tureck~4 Fixations= "8/"
# fait~5 Fixations= "8/"
# son~6 Fixations= "18/"
# entrée~7 Fixations= "18/"
# à~8 Fixations= "17/"
# quatre-vingt-quatre~9 Fixations= "14/"
# ans~10 Fixations= "16/"
# dans~11 Fixations= ""
# le~12 Fixations= ""
# répertoire~13 Fixations= "18/"
# discographique~14 Fixations= ""
# de~15 Fixations= ""
# Deutsche~16 Fixations= ""
# Grammophon~17 Fixations= ""
# Son~18 Fixations= ""
# interprétation~19 Fixations= ""
# des~20 Fixations= ""
# variations~21 Fixations= ""
# Goldberg~22 Fixations= ""
# de~23 Fixations= ""
# Bach~24 Fixations= ""
# va~25 Fixations= ""
# la~26 Fixations= ""
# faire~27 Fixations= ""
# connaître~28 Fixations= ""
# au~29 Fixations= ""
# grand~30 Fixations= ""
# public~31 Fixations= ""
%%
#  LA[10] PIANISTE[10] AMéRICAINE[13] ROSALYN[13] TURECK[8] FAIT[8] SON[18] ENTRéE[18] à[17] QUATRE-VINGT-QUATRE[14] ANS[16] dans le RéPERTOIRE[18] discographique de Deutsche Grammophon Son interprétation des variations Goldberg de Bach va la faire connaître au grand public
# s05 allocation_familiale-a2 PercentageOfWordsProcessed= 0.375 PercentageOfWordsReallyProcessed= 0.857142857142857
