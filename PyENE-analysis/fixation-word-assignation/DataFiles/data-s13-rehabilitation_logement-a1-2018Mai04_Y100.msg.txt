# 462 Fixation 1 (296.2,287.2): La~0 baie~1
# 504 Fixation 2 (382.1,283.8): baie~1 Somme~3
# 149 Fixation 3 (280.1,294.9): La~0 baie~1
# 206 Fixation 4 (364.8,280.3): baie~1 Somme~3
# 185 Fixation 5 (405.6,279.4): de~2 Somme~3
# 254 Fixation 6 (478.1,283.9): Somme~3 hiver~5
# 92 Fixation 7 (509.3,285.3): en~4 hiver~5
# 259 Fixation 8 (604.7,288.9): hiver~5 l'espace~6
# 202 Fixation 9 (628.3,289.7): l'espace~6
# 134 Fixation 10 (718.6,290): l'espace~6
# 214 Fixation 11 (302.5,344.5): La~0 baie~1 silence~8
# 254 Fixation 12 (276,350.8): silence~8
# 269 Fixation 13 (412.7,347.7): de~2 Somme~3 instants~10
# 326 Fixation 14 (440.7,349.6): des~9 instants~10
# 430 Fixation 15 (550.8,355.5): fugaces~11
# 162 Fixation 16 (657.9,355.9): et~12 coucher~18
# 231 Fixation 17 (293.7,396.8): silence~8 lumières~14
# 149 Fixation 18 (286,410.2): silence~8 lumières~14
# 268 Fixation 19 (401.1,411.7): des~9 instants~10 magiques~15
# 247 Fixation 20 (522.8,409.7): instants~10 fugaces~11
# 421 Fixation 21 (614.6,408.3): fugaces~11 coucher~18
# 371 Fixation 22 (279.3,488.7): soleil~20
%%
# La~0 Fixations= "11/"
# baie~1 Fixations= "11/"
# de~2 Fixations= "13/"
# Somme~3 Fixations= "13/"
# en~4 Fixations= "7/"
# hiver~5 Fixations= "8/"
# l'espace~6 Fixations= "10/"
# le~7 Fixations= ""
# silence~8 Fixations= "18/"
# des~9 Fixations= "19/"
# instants~10 Fixations= "20/"
# fugaces~11 Fixations= "21/"
# et~12 Fixations= "16/"
# des~13 Fixations= ""
# lumières~14 Fixations= "18/"
# magiques~15 Fixations= "19/"
# Comme~16 Fixations= ""
# ce~17 Fixations= ""
# coucher~18 Fixations= "21/"
# de~19 Fixations= ""
# soleil~20 Fixations= "22/"
# sur~21 Fixations= ""
# la~22 Fixations= ""
# pointe~23 Fixations= ""
# du~24 Fixations= ""
# Hourdel~25 Fixations= ""
%%
#  LA[11] BAIE[11] DE[13] SOMME[13] EN[7] HIVER[8] L'ESPACE[10] le SILENCE[18] DES[19] INSTANTS[20] FUGACES[21] ET[16] des LUMIèRES[18] MAGIQUES[19] Comme ce COUCHER[21] de SOLEIL[22] sur la pointe du Hourdel
# s13 rehabilitation_logement-a1 PercentageOfWordsProcessed= 0.615384615384615 PercentageOfWordsReallyProcessed= 0.761904761904762
