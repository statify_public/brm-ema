# 145 Fixation 1 (368.9,302.8): voiture~1
# 115 Fixation 2 (313,296.1): La~0 voiture~1
# 249 Fixation 3 (475.3,293.3): la~3 présidente~4
# 155 Fixation 4 (619.7,285.5): de~5 cour~7 été~12
# 270 Fixation 5 (443.7,288.8): de~2 présidente~4 Haute-Corse~10
# 119 Fixation 6 (495.9,286.9): la~3 présidente~4
# 143 Fixation 7 (543,289.9): présidente~4
# 175 Fixation 8 (387.7,292.1): voiture~1
# 147 Fixation 9 (614,285.3): présidente~4 cour~7 a~11 été~12
# 193 Fixation 10 (317.1,335.7): La~0 voiture~1 d'assises~8
# 102 Fixation 11 (300.3,342.1): La~0 voiture~1 d'assises~8
# 108 Fixation 12 (413.5,344.5): de~2 présidente~4 Haute-Corse~10
# 161 Fixation 13 (493.4,348.4): la~3 présidente~4 Haute-Corse~10
# 165 Fixation 14 (611,350.1): a~11 été~12
# 112 Fixation 15 (654.6,344.1): de~5 cour~7 été~12 incendiée~13
# 150 Fixation 16 (315,396.7): d'assises~8 nuit~16
# 139 Fixation 17 (406.9,401.1): de~9 Haute-Corse~10 nuit~16
# 153 Fixation 18 (500.1,400.1): Haute-Corse~10 samedi~18
# 187 Fixation 19 (572.1,403.2): Haute-Corse~10 a~11 à~19 dimanche~20
# 119 Fixation 20 (660,402.8): été~12 incendiée~13 dimanche~20 à~21
# 223 Fixation 21 (319,457.4): dans~14 nuit~16 Bastia~22 Selon~23
# 189 Fixation 22 (278.6,452.9): dans~14 nuit~16 Bastia~22
# 209 Fixation 23 (454.4,463.6): de~17 samedi~18
# 96 Fixation 24 (510,460.7): samedi~18 à~19 enquêteurs~25
# 212 Fixation 25 (664.6,461.6): dimanche~20 à~21 s'agirait~27
# 163 Fixation 26 (316,520.4): Bastia~22 acte~29
# 103 Fixation 27 (403.7,521.1): Selon~23 criminel~30
# 531 Fixation 28 (421.8,342.1): de~2 présidente~4 Haute-Corse~10
# 92 Fixation 29 (327,309.2): La~0 voiture~1
# 130 Fixation 30 (515.3,349.3): Haute-Corse~10
# 196 Fixation 31 (461.6,352.5): Haute-Corse~10
# 211 Fixation 32 (627.6,359.2): a~11 été~12
# 213 Fixation 33 (347.7,407.8): d'assises~8 nuit~16
# 180 Fixation 34 (465.2,411.7): Haute-Corse~10 samedi~18
# 269 Fixation 35 (571,414.7): à~19 dimanche~20
# 103 Fixation 36 (469.9,387.6): Haute-Corse~10 samedi~18
# 250 Fixation 37 (493.6,351.8): Haute-Corse~10
%%
# La~0 Fixations= "29/"
# voiture~1 Fixations= "29/"
# de~2 Fixations= "28/"
# la~3 Fixations= "13/"
# présidente~4 Fixations= "28/"
# de~5 Fixations= "15/"
# la~6 Fixations= ""
# cour~7 Fixations= "15/"
# d'assises~8 Fixations= "33/"
# de~9 Fixations= "17/"
# Haute-Corse~10 Fixations= "37/"
# a~11 Fixations= "32/"
# été~12 Fixations= "32/"
# incendiée~13 Fixations= "20/"
# dans~14 Fixations= "22/"
# la~15 Fixations= ""
# nuit~16 Fixations= "33/"
# de~17 Fixations= "23/"
# samedi~18 Fixations= "36/"
# à~19 Fixations= "35/"
# dimanche~20 Fixations= "35/"
# à~21 Fixations= "25/"
# Bastia~22 Fixations= "26/"
# Selon~23 Fixations= "27/"
# les~24 Fixations= ""
# enquêteurs~25 Fixations= "24/"
# il~26 Fixations= ""
# s'agirait~27 Fixations= "25/"
# d'un~28 Fixations= ""
# acte~29 Fixations= "26/"
# criminel~30 Fixations= "27/"
%%
#  LA[29] VOITURE[29] DE[28] LA[13] PRéSIDENTE[28] DE[15] la COUR[15] D'ASSISES[33] DE[17] HAUTE-CORSE[37] A[32] éTé[32] INCENDIéE[20] DANS[22] la NUIT[33] DE[23] SAMEDI[36] à[35] DIMANCHE[35] à[25] BASTIA[26] SELON[27] les ENQUêTEURS[24] il S'AGIRAIT[25] d'un ACTE[26] CRIMINEL[27]
# s04 prix_litteraire-a2 PercentageOfWordsProcessed= 0.838709677419355 PercentageOfWordsReallyProcessed= 0.838709677419355
