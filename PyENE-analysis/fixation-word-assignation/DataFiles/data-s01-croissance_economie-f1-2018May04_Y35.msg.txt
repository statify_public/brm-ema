# 146 Fixation 1 (304.2,279.3): Le~0 pacte~1
# 129 Fixation 2 (374.4,276.1): pacte~1 stabilité~3
# 223 Fixation 3 (433.5,280.1): de~2 stabilité~3
# 226 Fixation 4 (590.9,286.2): et~4 croissance~6
# 162 Fixation 5 (639.7,290.1): de~5 croissance~6
# 141 Fixation 6 (327.1,343.5): des~8 limites~9
# 120 Fixation 7 (294.5,355.9): des~8 limites~9
# 206 Fixation 8 (709.3,290.2): croissance~6 pose~7
# 161 Fixation 9 (374.6,357.1): limites~9 à~10
# 108 Fixation 10 (333.2,354.8): des~8 limites~9
# 246 Fixation 11 (505.3,350.4): l'utilisation~11
# 347 Fixation 12 (676.9,351.9): du~12 budget~13
# 207 Fixation 13 (333.2,414.2): relancer~15
# 126 Fixation 14 (303.4,422.6): relancer~15
# 163 Fixation 15 (576.9,418.7): Les~18 économistes~19
# 155 Fixation 16 (636.7,413.1): économistes~19
# 219 Fixation 17 (354.7,471.6): s'interrogent~20
# 144 Fixation 18 (311.3,478.9): s'interrogent~20
# 240 Fixation 19 (548,482.8): la~22 pérennité~23
# 119 Fixation 20 (694.5,469): de~24
%%
# Le~0 Fixations= "1/"
# pacte~1 Fixations= "2/"
# de~2 Fixations= "3/"
# stabilité~3 Fixations= "3/"
# et~4 Fixations= "4/"
# de~5 Fixations= "5/"
# croissance~6 Fixations= "8/"
# pose~7 Fixations= "8/"
# des~8 Fixations= "10/"
# limites~9 Fixations= "10/"
# à~10 Fixations= "9/"
# l'utilisation~11 Fixations= "11/"
# du~12 Fixations= "12/"
# budget~13 Fixations= "12/"
# pour~14 Fixations= ""
# relancer~15 Fixations= "14/"
# une~16 Fixations= ""
# économie~17 Fixations= ""
# Les~18 Fixations= "15/"
# économistes~19 Fixations= "16/"
# s'interrogent~20 Fixations= "18/"
# sur~21 Fixations= ""
# la~22 Fixations= "19/"
# pérennité~23 Fixations= "19/"
# de~24 Fixations= "20/"
# cette~25 Fixations= ""
# croissance~26 Fixations= ""
# qui~27 Fixations= ""
# est~28 Fixations= ""
# étonnante~29 Fixations= ""
%%
#  LE[1] PACTE[2] DE[3] STABILITé[3] ET[4] DE[5] CROISSANCE[8] POSE[8] DES[10] LIMITES[10] à[9] L'UTILISATION[11] DU[12] BUDGET[12] pour RELANCER[14] une économie LES[15] éCONOMISTES[16] S'INTERROGENT[18] sur LA[19] PéRENNITé[19] DE[20] cette croissance qui est étonnante
# s01 croissance_economie-f1 PercentageOfWordsProcessed= 0.7 PercentageOfWordsReallyProcessed= 0.84
