# 103 Fixation 1 (281.6,275.1): La~0 moitié~1
# 110 Fixation 2 (337.2,273.1): moitié~1
# 160 Fixation 3 (440.6,276.8): des~2 tout-petits~3
# 163 Fixation 4 (512.1,281): tout-petits~3
# 299 Fixation 5 (640.2,292.9): sont~4 gardés~5
# 163 Fixation 6 (330.2,354.6): foyer~7
# 226 Fixation 7 (285.5,357.8): foyer~7
# 89 Fixation 8 (413.4,353): par~8 parent~10
# 162 Fixation 9 (577.1,348.1): la~11 maman~12
# 160 Fixation 10 (554.6,348.5): la~11 maman~12
# 187 Fixation 11 (314.7,405.3): quasi-totalité~15
# 166 Fixation 12 (295.9,413.1): quasi-totalité~15
# 214 Fixation 13 (447.8,413.4): des~16 cas~17
# 127 Fixation 14 (517.5,411.3): des~16 cas~17
# 217 Fixation 15 (590.8,407.6): Seuls~18
# 204 Fixation 16 (687.4,407.5): 8%~19 ont~20
# 259 Fixation 17 (332.5,469.8): une~23 crèche~24
# 149 Fixation 18 (435.9,471.6): crèche~24 collective~25
# 86 Fixation 19 (554.7,473): collective~25 parentale~27
%%
# La~0 Fixations= "1/"
# moitié~1 Fixations= "2/"
# des~2 Fixations= "3/"
# tout-petits~3 Fixations= "4/"
# sont~4 Fixations= "5/"
# gardés~5 Fixations= "5/"
# au~6 Fixations= ""
# foyer~7 Fixations= "7/"
# par~8 Fixations= "8/"
# un~9 Fixations= ""
# parent~10 Fixations= "8/"
# la~11 Fixations= "10/"
# maman~12 Fixations= "10/"
# dans~13 Fixations= ""
# la~14 Fixations= ""
# quasi-totalité~15 Fixations= "12/"
# des~16 Fixations= "14/"
# cas~17 Fixations= "14/"
# Seuls~18 Fixations= "15/"
# 8%~19 Fixations= "16/"
# ont~20 Fixations= "16/"
# accès~21 Fixations= ""
# à~22 Fixations= ""
# une~23 Fixations= "17/"
# crèche~24 Fixations= "18/"
# collective~25 Fixations= "19/"
# ou~26 Fixations= ""
# parentale~27 Fixations= "19/"
%%
#  LA[1] MOITIé[2] DES[3] TOUT-PETITS[4] SONT[5] GARDéS[5] au FOYER[7] PAR[8] un PARENT[8] LA[10] MAMAN[10] dans la QUASI-TOTALITé[12] DES[14] CAS[14] SEULS[15] 8%[16] ONT[16] accès à UNE[17] CRèCHE[18] COLLECTIVE[19] ou PARENTALE[19]
# s01 allocation_familiale-m1 PercentageOfWordsProcessed= 0.75 PercentageOfWordsReallyProcessed= 0.75
