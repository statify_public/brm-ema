# 142 Fixation 1 (252.1,265.7): Malgré~0
# 194 Fixation 2 (414.6,276.9): un~1 accord~2
# 252 Fixation 3 (476,346.5): d'une~8 réforme~9
# 223 Fixation 4 (571.1,411.1): élèves~15
# 190 Fixation 5 (622.4,411.6): le~16 syndicat~17
# 112 Fixation 6 (361.8,471.7): enseignants~19
# 287 Fixation 7 (269.6,488.6): enseignants~19
# 163 Fixation 8 (296.9,538.4): abandonner~23
# 168 Fixation 9 (465,556.9): ce~24 projet~25
# 120 Fixation 10 (608.9,533.5): retravailler~27
# 205 Fixation 11 (304.3,625.4): d'autres~29
%%
# Malgré~0 Fixations= "1/"
# un~1 Fixations= "2/"
# accord~2 Fixations= "2/"
# de~3 Fixations= ""
# principe~4 Fixations= ""
# sur~5 Fixations= ""
# la~6 Fixations= ""
# nécessité~7 Fixations= ""
# d'une~8 Fixations= "3/"
# réforme~9 Fixations= "3/"
# comprenant~10 Fixations= ""
# une~11 Fixations= ""
# aide~12 Fixations= ""
# individualisée~13 Fixations= ""
# aux~14 Fixations= ""
# élèves~15 Fixations= "4/"
# le~16 Fixations= "5/"
# syndicat~17 Fixations= "5/"
# des~18 Fixations= ""
# enseignants~19 Fixations= "7/"
# affirmait~20 Fixations= ""
# qu'il~21 Fixations= ""
# fallait~22 Fixations= ""
# abandonner~23 Fixations= "8/"
# ce~24 Fixations= "9/"
# projet~25 Fixations= "9/"
# et~26 Fixations= ""
# retravailler~27 Fixations= "10/"
# sur~28 Fixations= ""
# d'autres~29 Fixations= "11/"
# bases~30 Fixations= ""
%%
#  MALGRé[1] UN[2] ACCORD[2] de principe sur la nécessité D'UNE[3] RéFORME[3] comprenant une aide individualisée aux éLèVES[4] LE[5] SYNDICAT[5] des ENSEIGNANTS[7] affirmait qu'il fallait ABANDONNER[8] CE[9] PROJET[9] et RETRAVAILLER[10] sur D'AUTRES[11] bases
# s21 reforme_enseignement-f2 PercentageOfWordsProcessed= 0.451612903225806 PercentageOfWordsReallyProcessed= 0.466666666666667
