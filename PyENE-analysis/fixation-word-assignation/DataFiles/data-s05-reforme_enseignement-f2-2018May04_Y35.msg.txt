# 171 Fixation 1 (280.5,292.3): Malgré~0
# 182 Fixation 2 (395.5,286.6): un~1 accord~2
# 422 Fixation 3 (536.6,297.1): de~3 principe~4
# 120 Fixation 4 (642.5,306.5): sur~5
# 245 Fixation 5 (322.9,346.3): nécessité~7
# 169 Fixation 6 (476.2,351.8): d'une~8 réforme~9
# 88 Fixation 7 (506.4,351.4): réforme~9
# 201 Fixation 8 (598.1,359.2): comprenant~10
# 155 Fixation 9 (713.9,361.2): une~11 aide~12
# 269 Fixation 10 (335.5,391.6): individualisée~13
# 167 Fixation 11 (490.4,404.4): aux~14 élèves~15
# 118 Fixation 12 (551.9,405.6): élèves~15
# 340 Fixation 13 (659.9,414.7): le~16 syndicat~17
# 142 Fixation 14 (723,417.4): syndicat~17
# 162 Fixation 15 (381.2,465): enseignants~19 affirmait~20
# 159 Fixation 16 (340.8,477.3): enseignants~19
# 173 Fixation 17 (470.2,463.3): affirmait~20
# 191 Fixation 18 (609.4,475.4): qu'il~21 fallait~22
# 229 Fixation 19 (338.7,543.8): abandonner~23
# 186 Fixation 20 (452.4,530.1): ce~24 projet~25
# 98 Fixation 21 (477.6,532.1): projet~25
# 221 Fixation 22 (596.5,530.7): et~26 retravailler~27
# 129 Fixation 23 (578.8,532.9): et~26 retravailler~27
# 109 Fixation 24 (658.8,534.2): retravailler~27
# 166 Fixation 25 (341.6,594.1): d'autres~29 bases~30
# 302 Fixation 26 (380.8,593.7): d'autres~29 bases~30
%%
# Malgré~0 Fixations= "1/"
# un~1 Fixations= "2/"
# accord~2 Fixations= "2/"
# de~3 Fixations= "3/"
# principe~4 Fixations= "3/"
# sur~5 Fixations= "4/"
# la~6 Fixations= ""
# nécessité~7 Fixations= "5/"
# d'une~8 Fixations= "6/"
# réforme~9 Fixations= "7/"
# comprenant~10 Fixations= "8/"
# une~11 Fixations= "9/"
# aide~12 Fixations= "9/"
# individualisée~13 Fixations= "10/"
# aux~14 Fixations= "11/"
# élèves~15 Fixations= "12/"
# le~16 Fixations= "13/"
# syndicat~17 Fixations= "14/"
# des~18 Fixations= ""
# enseignants~19 Fixations= "16/"
# affirmait~20 Fixations= "17/"
# qu'il~21 Fixations= "18/"
# fallait~22 Fixations= "18/"
# abandonner~23 Fixations= "19/"
# ce~24 Fixations= "20/"
# projet~25 Fixations= "21/"
# et~26 Fixations= "23/"
# retravailler~27 Fixations= "24/"
# sur~28 Fixations= ""
# d'autres~29 Fixations= "26/"
# bases~30 Fixations= "26/"
%%
#  MALGRé[1] UN[2] ACCORD[2] DE[3] PRINCIPE[3] SUR[4] la NéCESSITé[5] D'UNE[6] RéFORME[7] COMPRENANT[8] UNE[9] AIDE[9] INDIVIDUALISéE[10] AUX[11] éLèVES[12] LE[13] SYNDICAT[14] des ENSEIGNANTS[16] AFFIRMAIT[17] QU'IL[18] FALLAIT[18] ABANDONNER[19] CE[20] PROJET[21] ET[23] RETRAVAILLER[24] sur D'AUTRES[26] BASES[26]
# s05 reforme_enseignement-f2 PercentageOfWordsProcessed= 0.903225806451613 PercentageOfWordsReallyProcessed= 0.903225806451613
