# 200 Fixation 1 (315.2,292.9): La~0 critique~1
# 258 Fixation 2 (446.1,290.1): s'est~2 exprimée~3
# 92 Fixation 3 (422.8,289.5): s'est~2 exprimée~3
# 205 Fixation 4 (356.2,283.7): critique~1
# 208 Fixation 5 (273.3,288): La~0 critique~1
# 242 Fixation 6 (421.2,292.9): s'est~2 exprimée~3
# 194 Fixation 7 (510.3,288.7): exprimée~3
# 180 Fixation 8 (274.2,366.5): radicalement~5
# 128 Fixation 9 (311.4,364.1): radicalement~5
# 251 Fixation 10 (499.8,357.8): sur~6 nature~8
# 305 Fixation 11 (646.4,354.5): et~9 l'idéologie~10
# 208 Fixation 12 (333.8,421.4): projet~12
# 156 Fixation 13 (305.5,419.6): du~11 projet~12
# 180 Fixation 14 (414.8,416.7): éducatif~13
# 191 Fixation 15 (555.1,420.8): Dans~14 débat~16
# 122 Fixation 16 (585.7,417.4): Dans~14 débat~16
# 123 Fixation 17 (367.5,467.3): déclenché~17 dehors~19
# 272 Fixation 18 (307.5,483.7): déclenché~17
# 133 Fixation 19 (403.8,551.9): les~23 enseignants~24
# 83 Fixation 20 (444.9,543.2): les~23 enseignants~24
# 160 Fixation 21 (500,543): enseignants~24
# 170 Fixation 22 (681.6,550.6): s'en~25 prennent~26
# 137 Fixation 23 (646.4,546.3): s'en~25 prennent~26
# 437 Fixation 24 (330.5,594.9): aux~27 orientations~28
# 469 Fixation 25 (505,600): fondamentales~29
# 187 Fixation 26 (546.6,592.1): fondamentales~29
# 160 Fixation 27 (359,594.3): orientations~28
# 144 Fixation 28 (315.6,588): aux~27 orientations~28
# 172 Fixation 29 (273.9,586.3): aux~27 orientations~28
%%
# La~0 Fixations= "5/"
# critique~1 Fixations= "5/"
# s'est~2 Fixations= "6/"
# exprimée~3 Fixations= "7/"
# plus~4 Fixations= ""
# radicalement~5 Fixations= "9/"
# sur~6 Fixations= "10/"
# la~7 Fixations= ""
# nature~8 Fixations= "10/"
# et~9 Fixations= "11/"
# l'idéologie~10 Fixations= "11/"
# du~11 Fixations= "13/"
# projet~12 Fixations= "13/"
# éducatif~13 Fixations= "14/"
# Dans~14 Fixations= "16/"
# ce~15 Fixations= ""
# débat~16 Fixations= "16/"
# déclenché~17 Fixations= "18/"
# en~18 Fixations= ""
# dehors~19 Fixations= "17/"
# des~20 Fixations= ""
# organisations~21 Fixations= ""
# syndicales~22 Fixations= ""
# les~23 Fixations= "20/"
# enseignants~24 Fixations= "21/"
# s'en~25 Fixations= "23/"
# prennent~26 Fixations= "23/"
# aux~27 Fixations= "29/"
# orientations~28 Fixations= "29/"
# fondamentales~29 Fixations= "26/"
%%
#  LA[5] CRITIQUE[5] S'EST[6] EXPRIMéE[7] plus RADICALEMENT[9] SUR[10] la NATURE[10] ET[11] L'IDéOLOGIE[11] DU[13] PROJET[13] éDUCATIF[14] DANS[16] ce DéBAT[16] DéCLENCHé[18] en DEHORS[17] des organisations syndicales LES[20] ENSEIGNANTS[21] S'EN[23] PRENNENT[23] AUX[29] ORIENTATIONS[29] FONDAMENTALES[26]
# s01 reforme_enseignement-f1 PercentageOfWordsProcessed= 0.766666666666667 PercentageOfWordsReallyProcessed= 0.766666666666667
