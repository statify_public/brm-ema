# 85 Fixation 1 (263.1,271.7): Pour~0
# 163 Fixation 2 (346.8,272.5): les~1 vacanciers~2
# 223 Fixation 3 (424.4,275.1): vacanciers~2
# 209 Fixation 4 (522.3,270.3): qui~3 souhaitent~4
# 234 Fixation 5 (592.9,276.7): qui~3 souhaitent~4
# 125 Fixation 6 (716.6,286.4): avant~5
# 169 Fixation 7 (276.9,335.9): tout~6 reposer~8
# 191 Fixation 8 (361,338.3): se~7 reposer~8
# 153 Fixation 9 (521.5,347.3): dans~9 cadre~11
# 143 Fixation 10 (590.4,351.5): un~10 cadre~11
# 135 Fixation 11 (660.2,350.3): cadre~11 bucolique~12
# 177 Fixation 12 (640.1,352.6): cadre~11 bucolique~12
# 112 Fixation 13 (301,373.1): tout~6 reposer~8
# 126 Fixation 14 (250.9,378.5): tout~6 reposer~8
# 140 Fixation 15 (321.3,382.3): les~13 résidences~14
# 238 Fixation 16 (477.5,399.7): proposent~15
# 306 Fixation 17 (627.3,407.4): un~16 hébergement~17
# 372 Fixation 18 (247.4,442.2): les~13 résidences~14
# 161 Fixation 19 (370.4,453.1): leur~19 permettant~20
# 141 Fixation 20 (453.8,455.5): permettant~20
# 168 Fixation 21 (536.9,469.7): permettant~20 partir~22
# 210 Fixation 22 (616.4,474.1): de~21 partir~22
# 151 Fixation 23 (705.7,477.9): avec~23
# 150 Fixation 24 (260,497.2): adapté~18
# 301 Fixation 25 (320.1,502.1): adapté~18
# 150 Fixation 26 (228.7,496.6): adapté~18
# 206 Fixation 27 (437.6,511.4): sportifs~26
# 178 Fixation 28 (427.4,517.7): sportifs~26
# 119 Fixation 29 (521.4,517.3): sans~27 rester~30
# 155 Fixation 30 (591.5,525.6): autant~29 rester~30
# 144 Fixation 31 (665.3,525.9): autant~29 rester~30
# 487 Fixation 32 (241.8,568.5): amis~25
%%
# Pour~0 Fixations= "1/"
# les~1 Fixations= "2/"
# vacanciers~2 Fixations= "3/"
# qui~3 Fixations= "5/"
# souhaitent~4 Fixations= "5/"
# avant~5 Fixations= "6/"
# tout~6 Fixations= "14/"
# se~7 Fixations= "8/"
# reposer~8 Fixations= "14/"
# dans~9 Fixations= "9/"
# un~10 Fixations= "10/"
# cadre~11 Fixations= "12/"
# bucolique~12 Fixations= "12/"
# les~13 Fixations= "18/"
# résidences~14 Fixations= "18/"
# proposent~15 Fixations= "16/"
# un~16 Fixations= "17/"
# hébergement~17 Fixations= "17/"
# adapté~18 Fixations= "26/"
# leur~19 Fixations= "19/"
# permettant~20 Fixations= "21/"
# de~21 Fixations= "22/"
# partir~22 Fixations= "22/"
# avec~23 Fixations= "23/"
# des~24 Fixations= ""
# amis~25 Fixations= "32/"
# sportifs~26 Fixations= "28/"
# sans~27 Fixations= "29/"
# pour~28 Fixations= ""
# autant~29 Fixations= "31/"
# rester~30 Fixations= "31/"
# inactifs~31 Fixations= ""
%%
#  POUR[1] LES[2] VACANCIERS[3] QUI[5] SOUHAITENT[5] AVANT[6] TOUT[14] SE[8] REPOSER[14] DANS[9] UN[10] CADRE[12] BUCOLIQUE[12] LES[18] RéSIDENCES[18] PROPOSENT[16] UN[17] HéBERGEMENT[17] ADAPTé[26] LEUR[19] PERMETTANT[21] DE[22] PARTIR[22] AVEC[23] des AMIS[32] SPORTIFS[28] SANS[29] pour AUTANT[31] RESTER[31] inactifs
# s06 tourisme_montagne-m1 PercentageOfWordsProcessed= 0.90625 PercentageOfWordsReallyProcessed= 0.935483870967742
