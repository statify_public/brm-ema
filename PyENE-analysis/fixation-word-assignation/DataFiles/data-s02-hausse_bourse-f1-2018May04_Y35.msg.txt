# 222 Fixation 1 (414.1,289.2): de~2 Tokyo~3
# 137 Fixation 2 (508.3,293.8): Tokyo~3 a~4
# 158 Fixation 3 (555.1,290.8): a~4 clôturé~5
# 151 Fixation 4 (645.3,295.1): la~6 séance~7
# 120 Fixation 5 (699.2,297.4): la~6 séance~7
# 191 Fixation 6 (268.6,349.6): mardi~9
# 136 Fixation 7 (373.2,354.9): en~10 légère~11
# 118 Fixation 8 (562,362.7): hausse~12 soutenue~13
# 98 Fixation 9 (616,363.7): soutenue~13
# 167 Fixation 10 (698.2,363.5): par~14
# 203 Fixation 11 (292.2,406.9): achats~16
# 148 Fixation 12 (389.1,409.6): des~17 investisseurs~18
# 148 Fixation 13 (587.7,414.2): étrangers~19
# 189 Fixation 14 (281.6,470.6): L'indice~20
# 101 Fixation 15 (346.3,473.5): L'indice~20 Nikkei~21
# 143 Fixation 16 (433.6,474.1): Nikkei~21
# 204 Fixation 17 (487.7,475.2): affichait~22
# 128 Fixation 18 (600,479.8): un~23 gain~24
# 309 Fixation 19 (702,483.5): gain~24 important~25
%%
# La~0 Fixations= ""
# bourse~1 Fixations= ""
# de~2 Fixations= "1/"
# Tokyo~3 Fixations= "2/"
# a~4 Fixations= "3/"
# clôturé~5 Fixations= "3/"
# la~6 Fixations= "5/"
# séance~7 Fixations= "5/"
# de~8 Fixations= ""
# mardi~9 Fixations= "6/"
# en~10 Fixations= "7/"
# légère~11 Fixations= "7/"
# hausse~12 Fixations= "8/"
# soutenue~13 Fixations= "9/"
# par~14 Fixations= "10/"
# les~15 Fixations= ""
# achats~16 Fixations= "11/"
# des~17 Fixations= "12/"
# investisseurs~18 Fixations= "12/"
# étrangers~19 Fixations= "13/"
# L'indice~20 Fixations= "15/"
# Nikkei~21 Fixations= "16/"
# affichait~22 Fixations= "17/"
# un~23 Fixations= "18/"
# gain~24 Fixations= "19/"
# important~25 Fixations= "19/"
# à~26 Fixations= ""
# la~27 Fixations= ""
# clôture~28 Fixations= ""
%%
#  La bourse DE[1] TOKYO[2] A[3] CLôTURé[3] LA[5] SéANCE[5] de MARDI[6] EN[7] LéGèRE[7] HAUSSE[8] SOUTENUE[9] PAR[10] les ACHATS[11] DES[12] INVESTISSEURS[12] éTRANGERS[13] L'INDICE[15] NIKKEI[16] AFFICHAIT[17] UN[18] GAIN[19] IMPORTANT[19] à la clôture
# s02 hausse_bourse-f1 PercentageOfWordsProcessed= 0.758620689655172 PercentageOfWordsReallyProcessed= 0.846153846153846
