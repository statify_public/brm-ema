# 171 Fixation 1 (302.8,287.2): Le~0 6e~1
# 227 Fixation 2 (338,284.6): 6e~1 festival~2
# 99 Fixation 3 (289.9,282.7): Le~0 6e~1
# 140 Fixation 4 (357.6,284.5): 6e~1 festival~2
# 180 Fixation 5 (472.6,291.3): de~3 Gérardmer~4
# 167 Fixation 6 (532.3,298): Gérardmer~4
# 250 Fixation 7 (510.6,295.3): de~3 Gérardmer~4
# 180 Fixation 8 (653.2,301): consacré~5
# 146 Fixation 9 (670.5,302.4): consacré~5
# 177 Fixation 10 (265,352): genre~7
# 337 Fixation 11 (346.9,344.3): genre~7 fantastique~8
# 168 Fixation 12 (368,340.2): fantastique~8
# 263 Fixation 13 (511.6,341.9): présente~9
# 183 Fixation 14 (703.5,359.5): quinzaine~11
# 175 Fixation 15 (262.2,390.1): de~12 longs~13
# 139 Fixation 16 (310,391.7): de~12 longs~13
# 364 Fixation 17 (395.8,391.2): métrages~14
# 140 Fixation 18 (516.6,403.2): inédits~15
# 258 Fixation 19 (494.4,406.8): métrages~14 inédits~15
# 135 Fixation 20 (614.8,415.7): En~16 ouverture~17
# 113 Fixation 21 (667.7,419.2): En~16 ouverture~17
# 204 Fixation 22 (269.9,441.7): de~12 longs~13
# 193 Fixation 23 (344.6,439.1): longs~13
# 186 Fixation 24 (519.5,455.4): Psycho~20
%%
# Le~0 Fixations= "3/"
# 6e~1 Fixations= "4/"
# festival~2 Fixations= "4/"
# de~3 Fixations= "7/"
# Gérardmer~4 Fixations= "7/"
# consacré~5 Fixations= "9/"
# au~6 Fixations= ""
# genre~7 Fixations= "11/"
# fantastique~8 Fixations= "12/"
# présente~9 Fixations= "13/"
# une~10 Fixations= ""
# quinzaine~11 Fixations= "14/"
# de~12 Fixations= "22/"
# longs~13 Fixations= "23/"
# métrages~14 Fixations= "19/"
# inédits~15 Fixations= "19/"
# En~16 Fixations= "21/"
# ouverture~17 Fixations= "21/"
# hors~18 Fixations= ""
# compétition~19 Fixations= ""
# Psycho~20 Fixations= "24/"
# de~21 Fixations= ""
# Gus~22 Fixations= ""
# van~23 Fixations= ""
# Sant~24 Fixations= ""
# revisite~25 Fixations= ""
# le~26 Fixations= ""
# classique~27 Fixations= ""
# d'Hitchcock~28 Fixations= ""
%%
#  LE[3] 6E[4] FESTIVAL[4] DE[7] GéRARDMER[7] CONSACRé[9] au GENRE[11] FANTASTIQUE[12] PRéSENTE[13] une QUINZAINE[14] DE[22] LONGS[23] MéTRAGES[19] INéDITS[19] EN[21] OUVERTURE[21] hors compétition PSYCHO[24] de Gus van Sant revisite le classique d'Hitchcock
# s06 prix_litteraire-m2 PercentageOfWordsProcessed= 0.586206896551724 PercentageOfWordsReallyProcessed= 0.80952380952381
