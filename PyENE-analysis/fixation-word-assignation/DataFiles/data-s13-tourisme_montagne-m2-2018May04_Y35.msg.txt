# 119 Fixation 1 (282.2,277.2): Une~0 semaine~1
# 166 Fixation 2 (317.1,285): Une~0 semaine~1
# 207 Fixation 3 (426.8,284.4): à~2
# 156 Fixation 4 (473.4,286.7): à~2 Santorin~3
# 240 Fixation 5 (457.9,284.1): à~2 Santorin~3
# 168 Fixation 6 (473.4,279.1): à~2 Santorin~3
# 130 Fixation 7 (488.5,282): Santorin~3
# 241 Fixation 8 (570.7,285.1): l'une~4 plus~6
# 156 Fixation 9 (645.3,285.6): l'une~4 plus~6
# 93 Fixation 10 (677.2,284.2): des~5 plus~6
# 275 Fixation 11 (286.7,340.5): belles~7
# 286 Fixation 12 (411.4,341.6): et~8 assurément~9
# 322 Fixation 13 (591.9,351.3): en~10 raison~11
# 137 Fixation 14 (704.8,351): de~12
# 218 Fixation 15 (328.2,407.2): caractère~14
# 202 Fixation 16 (313.7,399.5): caractère~14
# 241 Fixation 17 (425,402.6): volcanique~15
# 136 Fixation 18 (409.3,405.5): volcanique~15
# 252 Fixation 19 (288.1,405.4): caractère~14
# 207 Fixation 20 (272.3,403.4): caractère~14
# 242 Fixation 21 (561.6,410.9): la~16 plus~17
%%
# Une~0 Fixations= "2/"
# semaine~1 Fixations= "2/"
# à~2 Fixations= "6/"
# Santorin~3 Fixations= "7/"
# l'une~4 Fixations= "9/"
# des~5 Fixations= "10/"
# plus~6 Fixations= "10/"
# belles~7 Fixations= "11/"
# et~8 Fixations= "12/"
# assurément~9 Fixations= "12/"
# en~10 Fixations= "13/"
# raison~11 Fixations= "13/"
# de~12 Fixations= "14/"
# son~13 Fixations= ""
# caractère~14 Fixations= "20/"
# volcanique~15 Fixations= "18/"
# la~16 Fixations= "21/"
# plus~17 Fixations= "21/"
# spectaculaire~18 Fixations= ""
# des~19 Fixations= ""
# îles~20 Fixations= ""
# grecques~21 Fixations= ""
# est~22 Fixations= ""
# proposée~23 Fixations= ""
# par~24 Fixations= ""
# l'agence~25 Fixations= ""
# de~26 Fixations= ""
# voyage~27 Fixations= ""
%%
#  UNE[2] SEMAINE[2] à[6] SANTORIN[7] L'UNE[9] DES[10] PLUS[10] BELLES[11] ET[12] ASSURéMENT[12] EN[13] RAISON[13] DE[14] son CARACTèRE[20] VOLCANIQUE[18] LA[21] PLUS[21] spectaculaire des îles grecques est proposée par l'agence de voyage
# s13 tourisme_montagne-m2 PercentageOfWordsProcessed= 0.607142857142857 PercentageOfWordsReallyProcessed= 0.944444444444444
