# 163 Fixation 1 (406,282.1): à~1 récompenser~2
# 160 Fixation 2 (616.3,293.4): une~3 oeuvre~4
# 173 Fixation 3 (289.7,353.9): inédite~7
# 155 Fixation 4 (436.4,355.8): prose~9
# 171 Fixation 5 (531.4,354.6): écrite~10
# 155 Fixation 6 (647,354.5): par~11 auteur~13
# 122 Fixation 7 (371.3,396): de~16 vingt-cinq~17
%%
# Destiné~0 Fixations= ""
# à~1 Fixations= "1/"
# récompenser~2 Fixations= "1/"
# une~3 Fixations= "2/"
# oeuvre~4 Fixations= "2/"
# de~5 Fixations= ""
# fiction~6 Fixations= ""
# inédite~7 Fixations= "3/"
# en~8 Fixations= ""
# prose~9 Fixations= "4/"
# écrite~10 Fixations= "5/"
# par~11 Fixations= "6/"
# un~12 Fixations= ""
# auteur~13 Fixations= "6/"
# de~14 Fixations= ""
# moins~15 Fixations= ""
# de~16 Fixations= "7/"
# vingt-cinq~17 Fixations= "7/"
# ans~18 Fixations= ""
# le~19 Fixations= ""
# quinzième~20 Fixations= ""
# prix~21 Fixations= ""
# du~22 Fixations= ""
# jeune~23 Fixations= ""
# écrivain~24 Fixations= ""
# a~25 Fixations= ""
# été~26 Fixations= ""
# décerné~27 Fixations= ""
# à~28 Fixations= ""
# Bernard~29 Fixations= ""
# Magnan~30 Fixations= ""
%%
#  Destiné à[1] RéCOMPENSER[1] UNE[2] OEUVRE[2] de fiction INéDITE[3] en PROSE[4] éCRITE[5] PAR[6] un AUTEUR[6] de moins DE[7] VINGT-CINQ[7] ans le quinzième prix du jeune écrivain a été décerné à Bernard Magnan
# s07 prix_litteraire-f2 PercentageOfWordsProcessed= 0.354838709677419 PercentageOfWordsReallyProcessed= 0.611111111111111
