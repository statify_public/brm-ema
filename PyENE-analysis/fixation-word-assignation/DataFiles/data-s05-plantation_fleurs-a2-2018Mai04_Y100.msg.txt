# 151 Fixation 1 (245.3,270.4): Trois~0
# 158 Fixation 2 (330.8,278.9): Trois~0 jours~1
# 216 Fixation 3 (410.4,280.8): jours~1 après~2
# 208 Fixation 4 (489,280): après~2 tir~4
# 283 Fixation 5 (590.2,284.7): tir~4 missile~6
# 172 Fixation 6 (685,286.3): missile~6
# 158 Fixation 7 (282.4,339.6): Trois~0 Agni-2~8
# 103 Fixation 8 (261.5,337.3): Trois~0 Agni-2~8
# 208 Fixation 9 (391.9,334.2): jours~1 après~2 Pakistan~10
# 214 Fixation 10 (538.3,341.5): le~3 tir~4 a~11 répliqué~12
# 294 Fixation 11 (661.9,350.6): en~13 tirant~14
# 150 Fixation 12 (301.1,405.6): Agni-2~8 version~16
# 122 Fixation 13 (271.5,399.9): Agni-2~8 version~16
# 260 Fixation 14 (377.9,396.2): le~9 Pakistan~10 améliorée~17
# 186 Fixation 15 (512.9,397.9): Pakistan~10 a~11
# 160 Fixation 16 (573.8,403.6): a~11 répliqué~12 missile~20
# 186 Fixation 17 (697.6,417.9): balistique~21
# 124 Fixation 18 (612.9,486.9): Ghauri~26
%%
# Trois~0 Fixations= "8/"
# jours~1 Fixations= "9/"
# après~2 Fixations= "9/"
# le~3 Fixations= "10/"
# tir~4 Fixations= "10/"
# du~5 Fixations= ""
# missile~6 Fixations= "6/"
# indien~7 Fixations= ""
# Agni-2~8 Fixations= "13/"
# le~9 Fixations= "14/"
# Pakistan~10 Fixations= "15/"
# a~11 Fixations= "16/"
# répliqué~12 Fixations= "16/"
# en~13 Fixations= "11/"
# tirant~14 Fixations= "11/"
# une~15 Fixations= ""
# version~16 Fixations= "13/"
# améliorée~17 Fixations= "14/"
# de~18 Fixations= ""
# son~19 Fixations= ""
# missile~20 Fixations= "16/"
# balistique~21 Fixations= "17/"
# de~22 Fixations= ""
# moyenne~23 Fixations= ""
# portée~24 Fixations= ""
# le~25 Fixations= ""
# Ghauri~26 Fixations= "18/"
# depuis~27 Fixations= ""
# le~28 Fixations= ""
# centre~29 Fixations= ""
# d'essai~30 Fixations= ""
# de~31 Fixations= ""
# Jhelum~32 Fixations= ""
%%
#  TROIS[8] JOURS[9] APRèS[9] LE[10] TIR[10] du MISSILE[6] indien AGNI-2[13] LE[14] PAKISTAN[15] A[16] RéPLIQUé[16] EN[11] TIRANT[11] une VERSION[13] AMéLIORéE[14] de son MISSILE[16] BALISTIQUE[17] de moyenne portée le GHAURI[18] depuis le centre d'essai de Jhelum
# s05 plantation_fleurs-a2 PercentageOfWordsProcessed= 0.545454545454545 PercentageOfWordsReallyProcessed= 0.666666666666667
