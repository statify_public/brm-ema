# 169 Fixation 1 (262.2,285.4): Pour~0 obtenir~1
# 148 Fixation 2 (374.1,279.7): obtenir~1
# 169 Fixation 3 (474.1,286.5): de~2 belles~3
# 120 Fixation 4 (600.2,296.6): roses~4
# 106 Fixation 5 (705.6,300.9): le~5 jardinier~6
# 190 Fixation 6 (310.1,324.8): plante~7
# 189 Fixation 7 (461.4,345.8): rosiers~9 dès~10
%%
# Pour~0 Fixations= "1/"
# obtenir~1 Fixations= "2/"
# de~2 Fixations= "3/"
# belles~3 Fixations= "3/"
# roses~4 Fixations= "4/"
# le~5 Fixations= "5/"
# jardinier~6 Fixations= "5/"
# plante~7 Fixations= "6/"
# les~8 Fixations= ""
# rosiers~9 Fixations= "7/"
# dès~10 Fixations= "7/"
# l'automne~11 Fixations= ""
# de~12 Fixations= ""
# façon~13 Fixations= ""
# à~14 Fixations= ""
# ce~15 Fixations= ""
# que~16 Fixations= ""
# leurs~17 Fixations= ""
# racines~18 Fixations= ""
# aient~19 Fixations= ""
# le~20 Fixations= ""
# temps~21 Fixations= ""
# de~22 Fixations= ""
# s'ancrer~23 Fixations= ""
# dans~24 Fixations= ""
# un~25 Fixations= ""
# sol~26 Fixations= ""
# encore~27 Fixations= ""
# chaud~28 Fixations= ""
%%
#  POUR[1] OBTENIR[2] DE[3] BELLES[3] ROSES[4] LE[5] JARDINIER[5] PLANTE[6] les ROSIERS[7] DèS[7] l'automne de façon à ce que leurs racines aient le temps de s'ancrer dans un sol encore chaud
# s07 plantation_fleurs-f2 PercentageOfWordsProcessed= 0.344827586206897 PercentageOfWordsReallyProcessed= 0.909090909090909
