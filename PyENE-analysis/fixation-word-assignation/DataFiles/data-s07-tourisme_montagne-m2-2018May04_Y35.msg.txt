# 152 Fixation 1 (289.3,291.6): Une~0 semaine~1
# 177 Fixation 2 (445.8,305.3): à~2 Santorin~3
# 158 Fixation 3 (581.5,275.9): l'une~4 plus~6
# 217 Fixation 4 (265.9,330.5): belles~7
# 202 Fixation 5 (394.9,345.1): et~8 assurément~9
# 211 Fixation 6 (586.9,332.5): en~10 raison~11
# 85 Fixation 7 (337.4,398.1): caractère~14
# 189 Fixation 8 (271.9,402.6): caractère~14
# 190 Fixation 9 (567,423.7): la~16 plus~17
# 87 Fixation 10 (652.9,418.3): plus~17 spectaculaire~18
# 218 Fixation 11 (298.2,460): des~19 îles~20
# 160 Fixation 12 (453,472.1): grecques~21
# 128 Fixation 13 (558.4,487.1): est~22 proposée~23
# 168 Fixation 14 (687.8,493): par~24 l'agence~25
# 273 Fixation 15 (276.7,538.2): de~26 voyage~27
# 130 Fixation 16 (347.1,341.3): belles~7
%%
# Une~0 Fixations= "1/"
# semaine~1 Fixations= "1/"
# à~2 Fixations= "2/"
# Santorin~3 Fixations= "2/"
# l'une~4 Fixations= "3/"
# des~5 Fixations= ""
# plus~6 Fixations= "3/"
# belles~7 Fixations= "16/"
# et~8 Fixations= "5/"
# assurément~9 Fixations= "5/"
# en~10 Fixations= "6/"
# raison~11 Fixations= "6/"
# de~12 Fixations= ""
# son~13 Fixations= ""
# caractère~14 Fixations= "8/"
# volcanique~15 Fixations= ""
# la~16 Fixations= "9/"
# plus~17 Fixations= "10/"
# spectaculaire~18 Fixations= "10/"
# des~19 Fixations= "11/"
# îles~20 Fixations= "11/"
# grecques~21 Fixations= "12/"
# est~22 Fixations= "13/"
# proposée~23 Fixations= "13/"
# par~24 Fixations= "14/"
# l'agence~25 Fixations= "14/"
# de~26 Fixations= "15/"
# voyage~27 Fixations= "15/"
%%
#  UNE[1] SEMAINE[1] à[2] SANTORIN[2] L'UNE[3] des PLUS[3] BELLES[16] ET[5] ASSURéMENT[5] EN[6] RAISON[6] de son CARACTèRE[8] volcanique LA[9] PLUS[10] SPECTACULAIRE[10] DES[11] îLES[11] GRECQUES[12] EST[13] PROPOSéE[13] PAR[14] L'AGENCE[14] DE[15] VOYAGE[15]
# s07 tourisme_montagne-m2 PercentageOfWordsProcessed= 0.857142857142857 PercentageOfWordsReallyProcessed= 0.857142857142857
