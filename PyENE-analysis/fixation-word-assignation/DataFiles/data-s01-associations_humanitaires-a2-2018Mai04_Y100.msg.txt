# 186 Fixation 1 (295.6,272.2): La~0 nageuse~1
# 108 Fixation 2 (361.9,277.5): nageuse~1 a~2
# 306 Fixation 3 (449,285.3): a~2 décroché~3
# 286 Fixation 4 (593.1,293.3): sa~4 deuxième~5
# 171 Fixation 5 (681,295.1): deuxième~5 médaille~6
# 221 Fixation 6 (349.9,359.9): aux~8 championnats~9 petit~14 bassin~15
# 90 Fixation 7 (421.1,353.1): championnats~9
# 162 Fixation 8 (605.9,357.8): d'Europe~10
# 192 Fixation 9 (619.2,420.8): s'imposant~17 Française~24
%%
# La~0 Fixations= "1/"
# nageuse~1 Fixations= "2/"
# a~2 Fixations= "3/"
# décroché~3 Fixations= "3/"
# sa~4 Fixations= "4/"
# deuxième~5 Fixations= "5/"
# médaille~6 Fixations= "5/"
# d'or~7 Fixations= ""
# aux~8 Fixations= "6/"
# championnats~9 Fixations= "7/"
# d'Europe~10 Fixations= "8/"
# de~11 Fixations= ""
# natation~12 Fixations= ""
# en~13 Fixations= ""
# petit~14 Fixations= "6/"
# bassin~15 Fixations= "6/"
# en~16 Fixations= ""
# s'imposant~17 Fixations= "9/"
# dans~18 Fixations= ""
# son~19 Fixations= ""
# épreuve~20 Fixations= ""
# de~21 Fixations= ""
# prédilection~22 Fixations= ""
# La~23 Fixations= ""
# Française~24 Fixations= "9/"
# l'a~25 Fixations= ""
# emporté~26 Fixations= ""
# facilement~27 Fixations= ""
# reléguant~28 Fixations= ""
# sa~29 Fixations= ""
# grande~30 Fixations= ""
# rivale~31 Fixations= ""
%%
#  LA[1] NAGEUSE[2] A[3] DéCROCHé[3] SA[4] DEUXIèME[5] MéDAILLE[5] d'or AUX[6] CHAMPIONNATS[7] D'EUROPE[8] de natation en PETIT[6] BASSIN[6] en S'IMPOSANT[9] dans son épreuve de prédilection La FRANçAISE[9] l'a emporté facilement reléguant sa grande rivale
# s01 associations_humanitaires-a2 PercentageOfWordsProcessed= 0.4375 PercentageOfWordsReallyProcessed= 0.56
