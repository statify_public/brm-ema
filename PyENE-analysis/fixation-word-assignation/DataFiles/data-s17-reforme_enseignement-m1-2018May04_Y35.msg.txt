# 146 Fixation 1 (304.6,280.8): Afin~0
# 130 Fixation 2 (351.9,281.3): d'apprendre~1
# 203 Fixation 3 (546.3,291.4): une~2 langue~3
# 187 Fixation 4 (641.4,285.6): étrangère~4
# 150 Fixation 5 (550.2,291.5): une~2 langue~3
# 196 Fixation 6 (309.9,334.4): ministère~6
# 166 Fixation 7 (434.2,348.9): entend~7
# 133 Fixation 8 (538.7,363.2): embaucher~8
# 209 Fixation 9 (660.8,368.2): des~9 locuteurs~10
# 175 Fixation 10 (314.3,408.2): natifs~11
# 151 Fixation 11 (436.5,417.8): faire~13
# 156 Fixation 12 (517.6,413.6): pratiquer~14
# 187 Fixation 13 (651.7,427.9): l'oral~15
# 176 Fixation 14 (330.4,460.9): élèves~17
# 183 Fixation 15 (472.2,474.6): politique~19
# 180 Fixation 16 (645.6,475.6): est~20 généralisée~21
# 164 Fixation 17 (335.8,532.1): pour~22 enseignements~24
# 142 Fixation 18 (437.8,536.2): enseignements~24
# 158 Fixation 19 (581.1,533.5): communs~25
# 217 Fixation 20 (714.7,542.1): les~26 options~27
# 199 Fixation 21 (346.8,589.6): obligatoires~28
# 238 Fixation 22 (481.5,602.2): et~29 facultatives~30
%%
# Afin~0 Fixations= "1/"
# d'apprendre~1 Fixations= "2/"
# une~2 Fixations= "5/"
# langue~3 Fixations= "5/"
# étrangère~4 Fixations= "4/"
# le~5 Fixations= ""
# ministère~6 Fixations= "6/"
# entend~7 Fixations= "7/"
# embaucher~8 Fixations= "8/"
# des~9 Fixations= "9/"
# locuteurs~10 Fixations= "9/"
# natifs~11 Fixations= "10/"
# pour~12 Fixations= ""
# faire~13 Fixations= "11/"
# pratiquer~14 Fixations= "12/"
# l'oral~15 Fixations= "13/"
# aux~16 Fixations= ""
# élèves~17 Fixations= "14/"
# Cette~18 Fixations= ""
# politique~19 Fixations= "15/"
# est~20 Fixations= "16/"
# généralisée~21 Fixations= "16/"
# pour~22 Fixations= "17/"
# les~23 Fixations= ""
# enseignements~24 Fixations= "18/"
# communs~25 Fixations= "19/"
# les~26 Fixations= "20/"
# options~27 Fixations= "20/"
# obligatoires~28 Fixations= "21/"
# et~29 Fixations= "22/"
# facultatives~30 Fixations= "22/"
%%
#  AFIN[1] D'APPRENDRE[2] UNE[5] LANGUE[5] éTRANGèRE[4] le MINISTèRE[6] ENTEND[7] EMBAUCHER[8] DES[9] LOCUTEURS[9] NATIFS[10] pour FAIRE[11] PRATIQUER[12] L'ORAL[13] aux éLèVES[14] Cette POLITIQUE[15] EST[16] GéNéRALISéE[16] POUR[17] les ENSEIGNEMENTS[18] COMMUNS[19] LES[20] OPTIONS[20] OBLIGATOIRES[21] ET[22] FACULTATIVES[22]
# s17 reforme_enseignement-m1 PercentageOfWordsProcessed= 0.838709677419355 PercentageOfWordsReallyProcessed= 0.838709677419355
