# 145 Fixation 1 (231.2,267): La~0 science~1
# 172 Fixation 2 (281.6,276.2): La~0 science~1
# 214 Fixation 3 (402.4,297.8): science~1 n'apporte~2
# 81 Fixation 4 (479.3,301.8): n'apporte~2 a~8 apporté~9
# 177 Fixation 5 (555,296.7): pas~3 apporté~9
# 157 Fixation 6 (596.6,295.8): pas~3 apporté~9 armes~11
# 113 Fixation 7 (675.7,298.9): seulement~4 armes~11
# 176 Fixation 8 (326.6,343.7): La~0 science~1 bienfaits~6
# 161 Fixation 9 (296.3,340.8): La~0 science~1 bienfaits~6
# 149 Fixation 10 (446.8,345): n'apporte~2 a~8
# 149 Fixation 11 (501.2,349): a~8 apporté~9
# 168 Fixation 12 (621.5,357.2): des~10 armes~11
# 139 Fixation 13 (639.6,356.5): des~10 armes~11
# 239 Fixation 14 (310.8,397.3): bienfaits~6 destruction~13
# 226 Fixation 15 (434.8,399): mais~7 a~8 massive~14
# 195 Fixation 16 (598.9,393.8): apporté~9 armes~11 possibilités~17
# 119 Fixation 17 (573.3,391.3): apporté~9
# 118 Fixation 18 (545.6,390.9): apporté~9
# 148 Fixation 19 (732.4,402.5): armes~11 possibilités~17
# 143 Fixation 20 (412.6,432.3): massive~14
# 160 Fixation 21 (310.6,439.6): destruction~13
# 99 Fixation 22 (289.9,442.1): destruction~13
# 147 Fixation 23 (597.6,451.5): des~16 possibilités~17 technique~22
# 153 Fixation 24 (650.8,456.7): possibilités~17 technique~22
# 143 Fixation 25 (325.7,523.2): manipulation~19 l'économie~24
# 162 Fixation 26 (284.2,521.1): manipulation~19 l'économie~24
%%
# La~0 Fixations= "9/"
# science~1 Fixations= "9/"
# n'apporte~2 Fixations= "10/"
# pas~3 Fixations= "6/"
# seulement~4 Fixations= "7/"
# des~5 Fixations= ""
# bienfaits~6 Fixations= "14/"
# mais~7 Fixations= "15/"
# a~8 Fixations= "15/"
# apporté~9 Fixations= "18/"
# des~10 Fixations= "13/"
# armes~11 Fixations= "19/"
# de~12 Fixations= ""
# destruction~13 Fixations= "22/"
# massive~14 Fixations= "20/"
# et~15 Fixations= ""
# des~16 Fixations= "23/"
# possibilités~17 Fixations= "24/"
# de~18 Fixations= ""
# manipulation~19 Fixations= "26/"
# biologique~20 Fixations= ""
# La~21 Fixations= ""
# technique~22 Fixations= "24/"
# et~23 Fixations= ""
# l'économie~24 Fixations= "26/"
# concourent~25 Fixations= ""
# à~26 Fixations= ""
# la~27 Fixations= ""
# dégradation~28 Fixations= ""
# de~29 Fixations= ""
# la~30 Fixations= ""
# biosphère~31 Fixations= ""
%%
#  LA[9] SCIENCE[9] N'APPORTE[10] PAS[6] SEULEMENT[7] des BIENFAITS[14] MAIS[15] A[15] APPORTé[18] DES[13] ARMES[19] de DESTRUCTION[22] MASSIVE[20] et DES[23] POSSIBILITéS[24] de MANIPULATION[26] biologique La TECHNIQUE[24] et L'éCONOMIE[26] concourent à la dégradation de la biosphère
# s05 reforme_justice-a1 PercentageOfWordsProcessed= 0.5625 PercentageOfWordsReallyProcessed= 0.72
