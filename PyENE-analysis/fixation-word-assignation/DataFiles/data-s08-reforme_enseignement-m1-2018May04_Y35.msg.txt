# 120 Fixation 1 (294.4,296.8): Afin~0
# 143 Fixation 2 (371.6,299.5): d'apprendre~1
# 154 Fixation 3 (518.7,298.4): une~2 langue~3
# 151 Fixation 4 (645.7,299.2): étrangère~4
# 152 Fixation 5 (730,290): étrangère~4
# 217 Fixation 6 (310.3,340.7): ministère~6
# 136 Fixation 7 (449.5,350.5): entend~7
# 153 Fixation 8 (513,348.5): embaucher~8
# 183 Fixation 9 (660.9,348.3): des~9 locuteurs~10
# 187 Fixation 10 (690.6,356.3): locuteurs~10
# 305 Fixation 11 (328.5,383): natifs~11
# 167 Fixation 12 (426.5,407.8): pour~12 faire~13
# 152 Fixation 13 (521.6,419.3): pratiquer~14
# 211 Fixation 14 (635.2,423.8): l'oral~15
# 116 Fixation 15 (698,429.5): l'oral~15
# 182 Fixation 16 (340,459.9): élèves~17
# 171 Fixation 17 (299,464.2): élèves~17
# 553 Fixation 18 (448.2,462): Cette~18 politique~19
# 161 Fixation 19 (562.3,472.5): politique~19
# 150 Fixation 20 (650.1,476.1): généralisée~21
# 147 Fixation 21 (348.6,503.6): élèves~17
# 145 Fixation 22 (436,514.1): enseignements~24
# 167 Fixation 23 (586.1,527.4): communs~25
# 175 Fixation 24 (713,536.1): les~26 options~27
# 135 Fixation 25 (359.1,579.5): obligatoires~28
# 176 Fixation 26 (321.7,585): obligatoires~28
# 154 Fixation 27 (532,588.8): facultatives~30
# 114 Fixation 28 (445.2,329.7): entend~7
%%
# Afin~0 Fixations= "1/"
# d'apprendre~1 Fixations= "2/"
# une~2 Fixations= "3/"
# langue~3 Fixations= "3/"
# étrangère~4 Fixations= "5/"
# le~5 Fixations= ""
# ministère~6 Fixations= "6/"
# entend~7 Fixations= "28/"
# embaucher~8 Fixations= "8/"
# des~9 Fixations= "9/"
# locuteurs~10 Fixations= "10/"
# natifs~11 Fixations= "11/"
# pour~12 Fixations= "12/"
# faire~13 Fixations= "12/"
# pratiquer~14 Fixations= "13/"
# l'oral~15 Fixations= "15/"
# aux~16 Fixations= ""
# élèves~17 Fixations= "21/"
# Cette~18 Fixations= "18/"
# politique~19 Fixations= "19/"
# est~20 Fixations= ""
# généralisée~21 Fixations= "20/"
# pour~22 Fixations= ""
# les~23 Fixations= ""
# enseignements~24 Fixations= "22/"
# communs~25 Fixations= "23/"
# les~26 Fixations= "24/"
# options~27 Fixations= "24/"
# obligatoires~28 Fixations= "26/"
# et~29 Fixations= ""
# facultatives~30 Fixations= "27/"
%%
#  AFIN[1] D'APPRENDRE[2] UNE[3] LANGUE[3] éTRANGèRE[5] le MINISTèRE[6] ENTEND[28] EMBAUCHER[8] DES[9] LOCUTEURS[10] NATIFS[11] POUR[12] FAIRE[12] PRATIQUER[13] L'ORAL[15] aux éLèVES[21] CETTE[18] POLITIQUE[19] est GéNéRALISéE[20] pour les ENSEIGNEMENTS[22] COMMUNS[23] LES[24] OPTIONS[24] OBLIGATOIRES[26] et FACULTATIVES[27]
# s08 reforme_enseignement-m1 PercentageOfWordsProcessed= 0.806451612903226 PercentageOfWordsReallyProcessed= 0.806451612903226
