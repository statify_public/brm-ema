# 251 Fixation 1 (278.2,300.6): Facile~0
# 430 Fixation 2 (351.2,290): Facile~0 à~1
# 105 Fixation 3 (338.3,288.8): Facile~0 à~1
# 233 Fixation 4 (487.1,278.2): cultiver~2 décorative~3
# 251 Fixation 5 (537.1,280.1): décorative~3
# 390 Fixation 6 (650.3,274): variée~4
# 289 Fixation 7 (287.4,352.7): tellement~6
# 316 Fixation 8 (394.5,352.5): savoureuse~7
# 212 Fixation 9 (331.5,356.3): tellement~6
# 348 Fixation 10 (553,337.8): décorative~3
# 279 Fixation 11 (637,347): décorative~3 variée~4 cueille~11
# 131 Fixation 12 (670.2,351.6): on~9 cueille~11
# 523 Fixation 13 (263.5,413.1): même~13
# 226 Fixation 14 (459.8,414.6): la~16 tomate~17
# 213 Fixation 15 (434.3,419.1): pied~15 tomate~17
# 521 Fixation 16 (576.1,405.8): quand~8 cueille~11 tomate~17
# 203 Fixation 17 (550.5,485.1): d'Amérique~23
# 195 Fixation 18 (493.2,487.3): Venue~22 d'Amérique~23
%%
# Facile~0 Fixations= "3/"
# à~1 Fixations= "3/"
# cultiver~2 Fixations= "4/"
# décorative~3 Fixations= "11/"
# variée~4 Fixations= "11/"
# et~5 Fixations= ""
# tellement~6 Fixations= "9/"
# savoureuse~7 Fixations= "8/"
# quand~8 Fixations= "16/"
# on~9 Fixations= "12/"
# la~10 Fixations= ""
# cueille~11 Fixations= "16/"
# à~12 Fixations= ""
# même~13 Fixations= "13/"
# le~14 Fixations= ""
# pied~15 Fixations= "15/"
# la~16 Fixations= "14/"
# tomate~17 Fixations= "16/"
# est~18 Fixations= ""
# incontournable~19 Fixations= ""
# au~20 Fixations= ""
# potager~21 Fixations= ""
# Venue~22 Fixations= "18/"
# d'Amérique~23 Fixations= "18/"
# centrale~24 Fixations= ""
# il~25 Fixations= ""
# faut~26 Fixations= ""
# lui~27 Fixations= ""
# fournir~28 Fixations= ""
# un~29 Fixations= ""
# sol~30 Fixations= ""
# riche~31 Fixations= ""
# et~32 Fixations= ""
# une~33 Fixations= ""
# bonne~34 Fixations= ""
# dose~35 Fixations= ""
# de~36 Fixations= ""
# soleil~37 Fixations= ""
%%
#  FACILE[3] à[3] CULTIVER[4] DéCORATIVE[11] VARIéE[11] et TELLEMENT[9] SAVOUREUSE[8] QUAND[16] ON[12] la CUEILLE[16] à MêME[13] le PIED[15] LA[14] TOMATE[16] est incontournable au potager VENUE[18] D'AMéRIQUE[18] centrale il faut lui fournir un sol riche et une bonne dose de soleil
# s14 formation_informatique-a2 PercentageOfWordsProcessed= 0.421052631578947 PercentageOfWordsReallyProcessed= 0.666666666666667
