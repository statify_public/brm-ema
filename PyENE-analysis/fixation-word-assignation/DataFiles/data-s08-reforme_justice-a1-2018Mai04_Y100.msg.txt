# 140 Fixation 1 (305.8,301.8): La~0 science~1
# 172 Fixation 2 (432.3,308.3): n'apporte~2 a~8
# 94 Fixation 3 (480.7,302.1): n'apporte~2 a~8 apporté~9
# 132 Fixation 4 (555.1,303.6): pas~3 apporté~9
# 88 Fixation 5 (611.4,301.5): seulement~4 armes~11
# 99 Fixation 6 (679.5,295): seulement~4 armes~11
# 246 Fixation 7 (323.9,348.5): La~0 science~1 bienfaits~6
# 117 Fixation 8 (432.7,357.1): mais~7 a~8
# 237 Fixation 9 (517.4,362.7): a~8 apporté~9
# 131 Fixation 10 (558.1,419.6): et~15 possibilités~17
# 127 Fixation 11 (618.5,427.6): des~16 possibilités~17
# 104 Fixation 12 (726.6,437.3): possibilités~17
# 102 Fixation 13 (326.8,454.5): destruction~13 manipulation~19
# 145 Fixation 14 (300.4,460.1): destruction~13 manipulation~19
# 203 Fixation 15 (447.1,466.2): massive~14 biologique~20
# 194 Fixation 16 (632.4,481.2): La~21 technique~22
# 239 Fixation 17 (321.1,513): manipulation~19 l'économie~24
# 158 Fixation 18 (438.8,513.9): biologique~20 concourent~25
# 182 Fixation 19 (601.1,526.4): La~21 technique~22 dégradation~28
# 89 Fixation 20 (656.3,529.7): technique~22 dégradation~28
# 164 Fixation 21 (329.9,559.1): l'économie~24
%%
# La~0 Fixations= "7/"
# science~1 Fixations= "7/"
# n'apporte~2 Fixations= "3/"
# pas~3 Fixations= "4/"
# seulement~4 Fixations= "6/"
# des~5 Fixations= ""
# bienfaits~6 Fixations= "7/"
# mais~7 Fixations= "8/"
# a~8 Fixations= "9/"
# apporté~9 Fixations= "9/"
# des~10 Fixations= ""
# armes~11 Fixations= "6/"
# de~12 Fixations= ""
# destruction~13 Fixations= "14/"
# massive~14 Fixations= "15/"
# et~15 Fixations= "10/"
# des~16 Fixations= "11/"
# possibilités~17 Fixations= "12/"
# de~18 Fixations= ""
# manipulation~19 Fixations= "17/"
# biologique~20 Fixations= "18/"
# La~21 Fixations= "19/"
# technique~22 Fixations= "20/"
# et~23 Fixations= ""
# l'économie~24 Fixations= "21/"
# concourent~25 Fixations= "18/"
# à~26 Fixations= ""
# la~27 Fixations= ""
# dégradation~28 Fixations= "20/"
# de~29 Fixations= ""
# la~30 Fixations= ""
# biosphère~31 Fixations= ""
%%
#  LA[7] SCIENCE[7] N'APPORTE[3] PAS[4] SEULEMENT[6] des BIENFAITS[7] MAIS[8] A[9] APPORTé[9] des ARMES[6] de DESTRUCTION[14] MASSIVE[15] ET[10] DES[11] POSSIBILITéS[12] de MANIPULATION[17] BIOLOGIQUE[18] LA[19] TECHNIQUE[20] et L'éCONOMIE[21] CONCOURENT[18] à la DéGRADATION[20] de la biosphère
# s08 reforme_justice-a1 PercentageOfWordsProcessed= 0.6875 PercentageOfWordsReallyProcessed= 0.758620689655172
