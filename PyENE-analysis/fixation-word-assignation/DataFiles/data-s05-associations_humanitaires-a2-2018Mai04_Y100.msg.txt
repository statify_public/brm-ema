# 176 Fixation 1 (269.5,272.5): La~0 nageuse~1
# 161 Fixation 2 (389.3,277.4): nageuse~1 a~2
# 170 Fixation 3 (439.1,281.8): a~2 décroché~3
# 119 Fixation 4 (449.2,281.6): a~2 décroché~3
# 212 Fixation 5 (414.3,284.4): a~2
# 207 Fixation 6 (575.9,287): sa~4 deuxième~5
# 185 Fixation 7 (678.2,289.4): deuxième~5 médaille~6
# 211 Fixation 8 (296.4,333.3): La~0 nageuse~1 d'or~7
# 179 Fixation 9 (406.6,331): nageuse~1 a~2 championnats~9
%%
# La~0 Fixations= "8/"
# nageuse~1 Fixations= "9/"
# a~2 Fixations= "9/"
# décroché~3 Fixations= "4/"
# sa~4 Fixations= "6/"
# deuxième~5 Fixations= "7/"
# médaille~6 Fixations= "7/"
# d'or~7 Fixations= "8/"
# aux~8 Fixations= ""
# championnats~9 Fixations= "9/"
# d'Europe~10 Fixations= ""
# de~11 Fixations= ""
# natation~12 Fixations= ""
# en~13 Fixations= ""
# petit~14 Fixations= ""
# bassin~15 Fixations= ""
# en~16 Fixations= ""
# s'imposant~17 Fixations= ""
# dans~18 Fixations= ""
# son~19 Fixations= ""
# épreuve~20 Fixations= ""
# de~21 Fixations= ""
# prédilection~22 Fixations= ""
# La~23 Fixations= ""
# Française~24 Fixations= ""
# l'a~25 Fixations= ""
# emporté~26 Fixations= ""
# facilement~27 Fixations= ""
# reléguant~28 Fixations= ""
# sa~29 Fixations= ""
# grande~30 Fixations= ""
# rivale~31 Fixations= ""
%%
#  LA[8] NAGEUSE[9] A[9] DéCROCHé[4] SA[6] DEUXIèME[7] MéDAILLE[7] D'OR[8] aux CHAMPIONNATS[9] d'Europe de natation en petit bassin en s'imposant dans son épreuve de prédilection La Française l'a emporté facilement reléguant sa grande rivale
# s05 associations_humanitaires-a2 PercentageOfWordsProcessed= 0.28125 PercentageOfWordsReallyProcessed= 0.9
