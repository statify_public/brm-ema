# 217 Fixation 1 (396.8,277.6): à~1 cultiver~2
# 175 Fixation 2 (533.1,274.9): décorative~3
# 112 Fixation 3 (575.7,280.3): décorative~3
# 156 Fixation 4 (675.5,282.6): variée~4
# 145 Fixation 5 (706.6,288.2): variée~4
# 159 Fixation 6 (286.6,352.9): tellement~6
# 84 Fixation 7 (307.7,349.5): tellement~6
# 216 Fixation 8 (407.1,347.1): à~1 cultiver~2 savoureuse~7
# 106 Fixation 9 (583.2,350): quand~8 cueille~11 incontournable~19
# 218 Fixation 10 (704.3,356): la~10 cueille~11
# 140 Fixation 11 (289.4,428): même~13
# 221 Fixation 12 (365.7,406.3): tellement~6 savoureuse~7 pied~15
%%
# Facile~0 Fixations= ""
# à~1 Fixations= "8/"
# cultiver~2 Fixations= "8/"
# décorative~3 Fixations= "3/"
# variée~4 Fixations= "5/"
# et~5 Fixations= ""
# tellement~6 Fixations= "12/"
# savoureuse~7 Fixations= "12/"
# quand~8 Fixations= "9/"
# on~9 Fixations= ""
# la~10 Fixations= "10/"
# cueille~11 Fixations= "10/"
# à~12 Fixations= ""
# même~13 Fixations= "11/"
# le~14 Fixations= ""
# pied~15 Fixations= "12/"
# la~16 Fixations= ""
# tomate~17 Fixations= ""
# est~18 Fixations= ""
# incontournable~19 Fixations= "9/"
# au~20 Fixations= ""
# potager~21 Fixations= ""
# Venue~22 Fixations= ""
# d'Amérique~23 Fixations= ""
# centrale~24 Fixations= ""
# il~25 Fixations= ""
# faut~26 Fixations= ""
# lui~27 Fixations= ""
# fournir~28 Fixations= ""
# un~29 Fixations= ""
# sol~30 Fixations= ""
# riche~31 Fixations= ""
# et~32 Fixations= ""
# une~33 Fixations= ""
# bonne~34 Fixations= ""
# dose~35 Fixations= ""
# de~36 Fixations= ""
# soleil~37 Fixations= ""
%%
#  Facile à[8] CULTIVER[8] DéCORATIVE[3] VARIéE[5] et TELLEMENT[12] SAVOUREUSE[12] QUAND[9] on LA[10] CUEILLE[10] à MêME[11] le PIED[12] la tomate est INCONTOURNABLE[9] au potager Venue d'Amérique centrale il faut lui fournir un sol riche et une bonne dose de soleil
# s02 formation_informatique-a2 PercentageOfWordsProcessed= 0.315789473684211 PercentageOfWordsReallyProcessed= 0.6
