# 214 Fixation 1 (372.3,302): de~1 200~2
# 345 Fixation 2 (441.5,298): infractions~3
# 154 Fixation 3 (331.8,300.1): Plus~0 200~2
# 127 Fixation 4 (557.1,287.2): infractions~3 à~4
# 255 Fixation 5 (641.3,280.7): l'environnement~5
# 153 Fixation 6 (380.1,328.9): été~7 constatées~8
# 166 Fixation 7 (309.4,334.8): ont~6 été~7
# 103 Fixation 8 (413.6,340.2): constatées~8
# 83 Fixation 9 (335.3,343.6): ont~6 été~7
# 168 Fixation 10 (557.7,343.3): dans~9 marais~11
# 164 Fixation 11 (597.9,341.4): le~10 marais~11
# 239 Fixation 12 (398.4,386.2): Fontenay-le-vicomte~13
# 304 Fixation 13 (595.9,339.2): le~10 marais~11
# 126 Fixation 14 (625.8,339.3): le~10 marais~11
# 334 Fixation 15 (330.9,405): Fontenay-le-vicomte~13
# 143 Fixation 16 (294.7,398.5): Fontenay-le-vicomte~13
# 227 Fixation 17 (410.7,399): Fontenay-le-vicomte~13
# 168 Fixation 18 (533.4,393): lors~14
# 153 Fixation 19 (618.3,393.8): d'une~15 opération~16
# 87 Fixation 20 (660.1,388.4): d'une~15 opération~16
# 300 Fixation 21 (427.8,285.1): 200~2 infractions~3
# 142 Fixation 22 (627.4,411.8): d'une~15 opération~16
# 143 Fixation 23 (666.8,390.8): d'une~15 opération~16
# 178 Fixation 24 (374.9,442.6): Fontenay-le-vicomte~13
# 132 Fixation 25 (308.9,440.2): Fontenay-le-vicomte~13
# 236 Fixation 26 (508.9,444.2): lors~14
# 147 Fixation 27 (544.3,450): parquet~20 d'Evry~21
# 224 Fixation 28 (663,454.3): plusieurs~22
# 205 Fixation 29 (357.5,495.8): lancée~17
# 121 Fixation 30 (301.3,497.6): lancée~17
# 190 Fixation 31 (520.1,504): parquet~20 d'Evry~21
# 152 Fixation 32 (570.2,509.5): été~25 recensées~26
# 213 Fixation 33 (689,509.7): dans~27
# 131 Fixation 34 (354.2,546.4): constructions~23
# 121 Fixation 35 (297.4,554.9): constructions~23
# 281 Fixation 36 (421.1,559): constructions~23 ont~24
%%
# Plus~0 Fixations= "3/"
# de~1 Fixations= "1/"
# 200~2 Fixations= "21/"
# infractions~3 Fixations= "21/"
# à~4 Fixations= "4/"
# l'environnement~5 Fixations= "5/"
# ont~6 Fixations= "9/"
# été~7 Fixations= "9/"
# constatées~8 Fixations= "8/"
# dans~9 Fixations= "10/"
# le~10 Fixations= "14/"
# marais~11 Fixations= "14/"
# de~12 Fixations= ""
# Fontenay-le-vicomte~13 Fixations= "25/"
# lors~14 Fixations= "26/"
# d'une~15 Fixations= "23/"
# opération~16 Fixations= "23/"
# lancée~17 Fixations= "30/"
# par~18 Fixations= ""
# le~19 Fixations= ""
# parquet~20 Fixations= "31/"
# d'Evry~21 Fixations= "31/"
# plusieurs~22 Fixations= "28/"
# constructions~23 Fixations= "36/"
# ont~24 Fixations= "36/"
# été~25 Fixations= "32/"
# recensées~26 Fixations= "32/"
# dans~27 Fixations= "33/"
# cette~28 Fixations= ""
# zone~29 Fixations= ""
# non~30 Fixations= ""
# constructible~31 Fixations= ""
%%
#  PLUS[3] DE[1] 200[21] INFRACTIONS[21] à[4] L'ENVIRONNEMENT[5] ONT[9] éTé[9] CONSTATéES[8] DANS[10] LE[14] MARAIS[14] de FONTENAY-LE-VICOMTE[25] LORS[26] D'UNE[23] OPéRATION[23] LANCéE[30] par le PARQUET[31] D'EVRY[31] PLUSIEURS[28] CONSTRUCTIONS[36] ONT[36] éTé[32] RECENSéES[32] DANS[33] cette zone non constructible
# s04 dechets_nucleaires-m2 PercentageOfWordsProcessed= 0.78125 PercentageOfWordsReallyProcessed= 0.892857142857143
