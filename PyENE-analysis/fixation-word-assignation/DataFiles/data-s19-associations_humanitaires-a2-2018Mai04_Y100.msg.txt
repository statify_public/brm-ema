# 145 Fixation 1 (325.1,267.5): La~0 nageuse~1
# 181 Fixation 2 (454.2,262.1): a~2 décroché~3
# 166 Fixation 3 (621.5,269.9): deuxième~5
# 161 Fixation 4 (690.1,264.7): deuxième~5 médaille~6
# 107 Fixation 5 (343.4,344.8): nageuse~1 d'or~7 championnats~9
# 108 Fixation 6 (288.3,343.9): La~0 nageuse~1 d'or~7
# 168 Fixation 7 (420.8,343.9): a~2 décroché~3 championnats~9
# 166 Fixation 8 (590.8,339.7): sa~4 deuxième~5 d'Europe~10
# 199 Fixation 9 (694.2,338.7): deuxième~5 médaille~6 natation~12
# 96 Fixation 10 (404.1,401.1): championnats~9 bassin~15
# 123 Fixation 11 (300,401.1): d'or~7 petit~14
# 205 Fixation 12 (412.8,412.5): championnats~9 bassin~15
# 217 Fixation 13 (548.2,406): d'Europe~10 s'imposant~17
# 133 Fixation 14 (659,410.9): d'Europe~10 natation~12
# 183 Fixation 15 (354.2,474): petit~14 bassin~15
# 137 Fixation 16 (303.6,473.3): en~13 petit~14 épreuve~20
# 154 Fixation 17 (464.8,478.9): prédilection~22
# 218 Fixation 18 (615.2,479.2): La~23 Française~24
# 188 Fixation 19 (316.9,545.7): emporté~26
# 146 Fixation 20 (395.6,542.3): facilement~27
# 191 Fixation 21 (553.1,547.7): reléguant~28
# 203 Fixation 22 (649.9,548.3): sa~29 grande~30
# 112 Fixation 23 (292.5,612): rivale~31
# 85 Fixation 24 (262.8,601.7): emporté~26 rivale~31
# 222 Fixation 25 (292.1,287.1): La~0 nageuse~1
# 209 Fixation 26 (453.7,286.4): a~2 décroché~3
# 270 Fixation 27 (592,275.7): sa~4 deuxième~5
# 95 Fixation 28 (692.4,272.3): deuxième~5 médaille~6
# 208 Fixation 29 (636.5,278.2): deuxième~5
# 340 Fixation 30 (396.8,279.4): nageuse~1 a~2
%%
# La~0 Fixations= "25/"
# nageuse~1 Fixations= "30/"
# a~2 Fixations= "30/"
# décroché~3 Fixations= "26/"
# sa~4 Fixations= "27/"
# deuxième~5 Fixations= "29/"
# médaille~6 Fixations= "28/"
# d'or~7 Fixations= "11/"
# aux~8 Fixations= ""
# championnats~9 Fixations= "12/"
# d'Europe~10 Fixations= "14/"
# de~11 Fixations= ""
# natation~12 Fixations= "14/"
# en~13 Fixations= "16/"
# petit~14 Fixations= "16/"
# bassin~15 Fixations= "15/"
# en~16 Fixations= ""
# s'imposant~17 Fixations= "13/"
# dans~18 Fixations= ""
# son~19 Fixations= ""
# épreuve~20 Fixations= "16/"
# de~21 Fixations= ""
# prédilection~22 Fixations= "17/"
# La~23 Fixations= "18/"
# Française~24 Fixations= "18/"
# l'a~25 Fixations= ""
# emporté~26 Fixations= "24/"
# facilement~27 Fixations= "20/"
# reléguant~28 Fixations= "21/"
# sa~29 Fixations= "22/"
# grande~30 Fixations= "22/"
# rivale~31 Fixations= "24/"
%%
#  LA[25] NAGEUSE[30] A[30] DéCROCHé[26] SA[27] DEUXIèME[29] MéDAILLE[28] D'OR[11] aux CHAMPIONNATS[12] D'EUROPE[14] de NATATION[14] EN[16] PETIT[16] BASSIN[15] en S'IMPOSANT[13] dans son éPREUVE[16] de PRéDILECTION[17] LA[18] FRANçAISE[18] l'a EMPORTé[24] FACILEMENT[20] RELéGUANT[21] SA[22] GRANDE[22] RIVALE[24]
# s19 associations_humanitaires-a2 PercentageOfWordsProcessed= 0.78125 PercentageOfWordsReallyProcessed= 0.78125
