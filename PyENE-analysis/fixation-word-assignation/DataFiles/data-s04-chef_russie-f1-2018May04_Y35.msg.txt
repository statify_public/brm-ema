# 107 Fixation 1 (352,294.8): Moscou~1
# 126 Fixation 2 (306.6,309.2): A~0 Moscou~1
# 167 Fixation 3 (464.8,293.6): les~2 communiqués~3
# 130 Fixation 4 (514.3,291.2): communiqués~3
# 199 Fixation 5 (620.4,288.1): triomphants~4
# 117 Fixation 6 (663.8,297.2): triomphants~4
# 228 Fixation 7 (315.7,337.6): succèdent~6
# 172 Fixation 8 (285.8,341.1): succèdent~6
# 88 Fixation 9 (615.1,293.7): triomphants~4
# 176 Fixation 10 (708.8,304.5): triomphants~4
# 108 Fixation 11 (419,313.7): les~2 communiqués~3
# 82 Fixation 12 (497.4,353.4): le~8 Daghestan~9
# 96 Fixation 13 (594.3,356.3): Daghestan~9
# 210 Fixation 14 (553.4,354.1): Daghestan~9
# 143 Fixation 15 (526.3,359.2): le~8 Daghestan~9
# 214 Fixation 16 (687.5,353.8): les~10 rebelles~11
# 95 Fixation 17 (707.4,351.9): les~10 rebelles~11
# 176 Fixation 18 (352.6,412.9): complètement~13
# 142 Fixation 19 (300.3,408.2): sont~12
# 276 Fixation 20 (491.4,412): encerclés~14
# 95 Fixation 21 (614.8,410.3): encerclés~14 chef~16
# 103 Fixation 22 (662.9,414.3): leur~15 chef~16
# 211 Fixation 23 (311,462.3): militaire~17
# 250 Fixation 24 (450.5,468.2): est~18 blessé~19
# 356 Fixation 25 (608,472.9): les~20 islamistes~21
# 270 Fixation 26 (343.3,526): subi~23 lourdes~25
# 97 Fixation 27 (278.7,530.8): subi~23
# 160 Fixation 28 (707.4,477.5): islamistes~21 ont~22
# 169 Fixation 29 (397.7,533.8): lourdes~25
# 183 Fixation 30 (355.6,288.8): Moscou~1
# 143 Fixation 31 (307,291.6): A~0 Moscou~1
# 218 Fixation 32 (456.6,286.3): les~2 communiqués~3
# 208 Fixation 33 (620.9,286.6): triomphants~4
%%
# A~0 Fixations= "31/"
# Moscou~1 Fixations= "31/"
# les~2 Fixations= "32/"
# communiqués~3 Fixations= "32/"
# triomphants~4 Fixations= "33/"
# se~5 Fixations= ""
# succèdent~6 Fixations= "8/"
# Dans~7 Fixations= ""
# le~8 Fixations= "15/"
# Daghestan~9 Fixations= "15/"
# les~10 Fixations= "17/"
# rebelles~11 Fixations= "17/"
# sont~12 Fixations= "19/"
# complètement~13 Fixations= "18/"
# encerclés~14 Fixations= "21/"
# leur~15 Fixations= "22/"
# chef~16 Fixations= "22/"
# militaire~17 Fixations= "23/"
# est~18 Fixations= "24/"
# blessé~19 Fixations= "24/"
# les~20 Fixations= "25/"
# islamistes~21 Fixations= "28/"
# ont~22 Fixations= "28/"
# subi~23 Fixations= "27/"
# de~24 Fixations= ""
# lourdes~25 Fixations= "29/"
# pertes~26 Fixations= ""
%%
#  A[31] MOSCOU[31] LES[32] COMMUNIQUéS[32] TRIOMPHANTS[33] se SUCCèDENT[8] Dans LE[15] DAGHESTAN[15] LES[17] REBELLES[17] SONT[19] COMPLèTEMENT[18] ENCERCLéS[21] LEUR[22] CHEF[22] MILITAIRE[23] EST[24] BLESSé[24] LES[25] ISLAMISTES[28] ONT[28] SUBI[27] de LOURDES[29] pertes
# s04 chef_russie-f1 PercentageOfWordsProcessed= 0.851851851851852 PercentageOfWordsReallyProcessed= 0.884615384615385
