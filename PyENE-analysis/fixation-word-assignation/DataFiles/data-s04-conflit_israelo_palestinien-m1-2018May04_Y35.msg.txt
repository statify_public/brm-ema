# 126 Fixation 1 (385.3,304.9): du~2 gouvernement~3
# 140 Fixation 2 (329,290.3): chef~1
# 83 Fixation 3 (472.3,281.9): gouvernement~3
# 177 Fixation 4 (598,279.7): est~4 accusé~5
# 114 Fixation 5 (648.8,283.9): accusé~5
# 258 Fixation 6 (332.4,342.1): relations~7
# 167 Fixation 7 (291,339.7): relations~7
# 404 Fixation 8 (430.6,346.2): tendues~8
# 250 Fixation 9 (562.3,349.4): avec~9 États-Unis~11
# 160 Fixation 10 (509.3,347.1): avec~9 États-Unis~11
# 107 Fixation 11 (648.7,350.7): États-Unis~11
# 291 Fixation 12 (330.9,396): l'isolement~13
# 172 Fixation 13 (432.3,406.7): d'Israël~14
# 184 Fixation 14 (292,398.4): l'isolement~13
# 193 Fixation 15 (683.8,370.8): États-Unis~11
# 103 Fixation 16 (726.4,354.3): États-Unis~11
# 136 Fixation 17 (522.9,418.2): d'Israël~14
# 252 Fixation 18 (606.7,411.9): dans~15 monde~17
# 115 Fixation 19 (507,412.5): d'Israël~14
# 208 Fixation 20 (323.4,467.8): chômage~19
# 124 Fixation 21 (399.1,461.1): en~20 montée~21
# 153 Fixation 22 (280,455.1): chômage~19
# 96 Fixation 23 (462.1,461.1): montée~21
# 124 Fixation 24 (397.7,473.2): en~20 montée~21
# 301 Fixation 25 (530.4,466.1): vertigineuse~22
# 88 Fixation 26 (520.3,470.4): vertigineuse~22
# 169 Fixation 27 (673.6,467.9): et~23
# 221 Fixation 28 (327,511.5): détérioration~26
# 161 Fixation 29 (285.9,513.2): détérioration~26
# 245 Fixation 30 (457.9,522.1): de~27 toutes~28
# 121 Fixation 31 (483.9,527.8): de~27 toutes~28
# 235 Fixation 32 (596.5,529.2): les~29 normes~30
# 250 Fixation 33 (315.7,584.4): gouvernement~32
# 200 Fixation 34 (454.1,584.4): démocratique~33
# 102 Fixation 35 (407.8,326.9): tendues~8
# 233 Fixation 36 (337.3,284.2): chef~1
# 112 Fixation 37 (414.2,278.2): du~2 gouvernement~3
# 240 Fixation 38 (592.6,275.5): est~4 accusé~5
# 207 Fixation 39 (302.1,342.9): relations~7
# 202 Fixation 40 (392.8,342.3): tendues~8
# 154 Fixation 41 (308.8,341.6): relations~7
# 268 Fixation 42 (618.2,284.5): est~4 accusé~5
# 93 Fixation 43 (675.2,281.1): accusé~5
# 223 Fixation 44 (399.6,343.1): tendues~8
# 206 Fixation 45 (279.9,343.9): relations~7
# 98 Fixation 46 (506.6,339.2): avec~9 États-Unis~11
# 92 Fixation 47 (565.4,347.6): avec~9 États-Unis~11
# 254 Fixation 48 (408.7,349.2): tendues~8
# 368 Fixation 49 (633.7,338.2): États-Unis~11
# 89 Fixation 50 (489.4,350.3): tendues~8
# 267 Fixation 51 (625.8,346.2): les~10 États-Unis~11
# 125 Fixation 52 (387.5,390.8): l'isolement~13 d'Israël~14
# 174 Fixation 53 (306.4,399.3): l'isolement~13
# 194 Fixation 54 (508.8,404.8): d'Israël~14
# 178 Fixation 55 (602.5,407): dans~15 monde~17
# 203 Fixation 56 (386.4,450.5): en~20 montée~21
# 266 Fixation 57 (293.2,452.5): chômage~19
# 599 Fixation 58 (492.3,467.9): montée~21 vertigineuse~22
# 209 Fixation 59 (350.5,456.6): chômage~19 montée~21
# 158 Fixation 60 (648.7,484): et~23
%%
# Le~0 Fixations= ""
# chef~1 Fixations= "36/"
# du~2 Fixations= "37/"
# gouvernement~3 Fixations= "37/"
# est~4 Fixations= "42/"
# accusé~5 Fixations= "43/"
# des~6 Fixations= ""
# relations~7 Fixations= "45/"
# tendues~8 Fixations= "50/"
# avec~9 Fixations= "47/"
# les~10 Fixations= "51/"
# États-Unis~11 Fixations= "51/"
# de~12 Fixations= ""
# l'isolement~13 Fixations= "53/"
# d'Israël~14 Fixations= "54/"
# dans~15 Fixations= "55/"
# le~16 Fixations= ""
# monde~17 Fixations= "55/"
# du~18 Fixations= ""
# chômage~19 Fixations= "59/"
# en~20 Fixations= "56/"
# montée~21 Fixations= "59/"
# vertigineuse~22 Fixations= "58/"
# et~23 Fixations= "60/"
# de~24 Fixations= ""
# la~25 Fixations= ""
# détérioration~26 Fixations= "29/"
# de~27 Fixations= "31/"
# toutes~28 Fixations= "31/"
# les~29 Fixations= "32/"
# normes~30 Fixations= "32/"
# de~31 Fixations= ""
# gouvernement~32 Fixations= "33/"
# démocratique~33 Fixations= "34/"
%%
#  Le CHEF[36] DU[37] GOUVERNEMENT[37] EST[42] ACCUSé[43] des RELATIONS[45] TENDUES[50] AVEC[47] LES[51] ÉTATS-UNIS[51] de L'ISOLEMENT[53] D'ISRAëL[54] DANS[55] le MONDE[55] du CHôMAGE[59] EN[56] MONTéE[59] VERTIGINEUSE[58] ET[60] de la DéTéRIORATION[29] DE[31] TOUTES[31] LES[32] NORMES[32] de GOUVERNEMENT[33] DéMOCRATIQUE[34]
# s04 conflit_israelo_palestinien-m1 PercentageOfWordsProcessed= 0.764705882352941 PercentageOfWordsReallyProcessed= 0.764705882352941
