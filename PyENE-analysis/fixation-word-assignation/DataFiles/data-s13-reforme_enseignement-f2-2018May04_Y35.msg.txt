# 243 Fixation 1 (297.8,277.9): Malgré~0
# 318 Fixation 2 (389.9,277): un~1 accord~2
# 134 Fixation 3 (445.1,267.5): accord~2
# 244 Fixation 4 (516.9,270.4): de~3 principe~4
# 126 Fixation 5 (544.1,275.8): de~3 principe~4
# 214 Fixation 6 (634.4,281.2): principe~4
# 253 Fixation 7 (302.2,321.3): nécessité~7
# 110 Fixation 8 (300.3,330.9): nécessité~7
# 177 Fixation 9 (420.3,335.1): d'une~8 réforme~9
# 198 Fixation 10 (484.4,334.1): réforme~9
# 332 Fixation 11 (618.9,345.3): comprenant~10
# 144 Fixation 12 (716.2,348.3): une~11 aide~12
# 82 Fixation 13 (745.9,350.1): une~11 aide~12
# 259 Fixation 14 (269.6,399.9): individualisée~13
# 139 Fixation 15 (306.7,396.7): individualisée~13
# 172 Fixation 16 (481,393.7): aux~14 élèves~15
# 180 Fixation 17 (526.5,400.2): aux~14 élèves~15
# 233 Fixation 18 (666.5,406.9): le~16 syndicat~17
# 114 Fixation 19 (643,407): le~16 syndicat~17
# 117 Fixation 20 (277.4,502.3): enseignants~19
# 240 Fixation 21 (319.1,583.9): d'autres~29
# 429 Fixation 22 (372.5,599.5): d'autres~29 bases~30
# 157 Fixation 23 (579.9,537.9): et~26 retravailler~27
# 273 Fixation 24 (621.4,533.2): retravailler~27
%%
# Malgré~0 Fixations= "1/"
# un~1 Fixations= "2/"
# accord~2 Fixations= "3/"
# de~3 Fixations= "5/"
# principe~4 Fixations= "6/"
# sur~5 Fixations= ""
# la~6 Fixations= ""
# nécessité~7 Fixations= "8/"
# d'une~8 Fixations= "9/"
# réforme~9 Fixations= "10/"
# comprenant~10 Fixations= "11/"
# une~11 Fixations= "13/"
# aide~12 Fixations= "13/"
# individualisée~13 Fixations= "15/"
# aux~14 Fixations= "17/"
# élèves~15 Fixations= "17/"
# le~16 Fixations= "19/"
# syndicat~17 Fixations= "19/"
# des~18 Fixations= ""
# enseignants~19 Fixations= "20/"
# affirmait~20 Fixations= ""
# qu'il~21 Fixations= ""
# fallait~22 Fixations= ""
# abandonner~23 Fixations= ""
# ce~24 Fixations= ""
# projet~25 Fixations= ""
# et~26 Fixations= "23/"
# retravailler~27 Fixations= "24/"
# sur~28 Fixations= ""
# d'autres~29 Fixations= "22/"
# bases~30 Fixations= "22/"
%%
#  MALGRé[1] UN[2] ACCORD[3] DE[5] PRINCIPE[6] sur la NéCESSITé[8] D'UNE[9] RéFORME[10] COMPRENANT[11] UNE[13] AIDE[13] INDIVIDUALISéE[15] AUX[17] éLèVES[17] LE[19] SYNDICAT[19] des ENSEIGNANTS[20] affirmait qu'il fallait abandonner ce projet ET[23] RETRAVAILLER[24] sur D'AUTRES[22] BASES[22]
# s13 reforme_enseignement-f2 PercentageOfWordsProcessed= 0.67741935483871 PercentageOfWordsReallyProcessed= 0.67741935483871
