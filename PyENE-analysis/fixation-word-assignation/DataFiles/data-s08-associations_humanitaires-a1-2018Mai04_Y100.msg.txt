# 156 Fixation 1 (349.3,298.6): déflagration~1
# 157 Fixation 2 (474.3,304.1): s'est~2 produite~3 militaire~8
# 188 Fixation 3 (564,305.1): produite~3
# 180 Fixation 4 (339.3,297.9): déflagration~1
# 153 Fixation 5 (690.6,308.3): au~4 passage~5 route~11
# 195 Fixation 6 (338.5,358.1): d'un~6 véhicule~7
# 186 Fixation 7 (457.3,356.9): militaire~8
# 155 Fixation 8 (620.2,358.9): sur~9 route~11
# 123 Fixation 9 (681.8,359.3): une~10 route~11
# 151 Fixation 10 (512.4,403): militaire~8 centaine~16
# 142 Fixation 11 (661,426): de~17 mètres~18
# 204 Fixation 12 (379.9,456.8): centre-ville~13 à~14 base~20
# 148 Fixation 13 (483.9,467.9): à~14 centaine~16 l'armée~22
# 167 Fixation 14 (569,476.1): centaine~16 turque~23
# 119 Fixation 15 (665,482.2): selon~24
# 149 Fixation 16 (283.1,511): d'une~19 base~20 police~26
# 182 Fixation 17 (288.8,319.5): La~0 déflagration~1 véhicule~7
%%
# La~0 Fixations= "17/"
# déflagration~1 Fixations= "17/"
# s'est~2 Fixations= "2/"
# produite~3 Fixations= "3/"
# au~4 Fixations= "5/"
# passage~5 Fixations= "5/"
# d'un~6 Fixations= "6/"
# véhicule~7 Fixations= "17/"
# militaire~8 Fixations= "10/"
# sur~9 Fixations= "8/"
# une~10 Fixations= "9/"
# route~11 Fixations= "9/"
# du~12 Fixations= ""
# centre-ville~13 Fixations= "12/"
# à~14 Fixations= "13/"
# une~15 Fixations= ""
# centaine~16 Fixations= "14/"
# de~17 Fixations= "11/"
# mètres~18 Fixations= "11/"
# d'une~19 Fixations= "16/"
# base~20 Fixations= "16/"
# de~21 Fixations= ""
# l'armée~22 Fixations= "13/"
# turque~23 Fixations= "14/"
# selon~24 Fixations= "15/"
# la~25 Fixations= ""
# police~26 Fixations= "16/"
%%
#  LA[17] DéFLAGRATION[17] S'EST[2] PRODUITE[3] AU[5] PASSAGE[5] D'UN[6] VéHICULE[17] MILITAIRE[10] SUR[8] UNE[9] ROUTE[9] du CENTRE-VILLE[12] à[13] une CENTAINE[14] DE[11] MèTRES[11] D'UNE[16] BASE[16] de L'ARMéE[13] TURQUE[14] SELON[15] la POLICE[16]
# s08 associations_humanitaires-a1 PercentageOfWordsProcessed= 0.851851851851852 PercentageOfWordsReallyProcessed= 0.851851851851852
