# 235 Fixation 1 (353.2,300.7): L'Irak~0 a~1
# 265 Fixation 2 (489,295.7): à~3 l'ONU~4
# 198 Fixation 3 (586.4,290.8): l'ONU~4 d'empêcher~5
# 129 Fixation 4 (633.2,297.5): d'empêcher~5
# 219 Fixation 5 (452,290): demandé~2 à~3
# 121 Fixation 6 (620.6,293.5): d'empêcher~5
# 158 Fixation 7 (299.6,352.5): survols~7
# 103 Fixation 8 (380.9,350.5): américains~8
# 115 Fixation 9 (407,344.1): américains~8
# 325 Fixation 10 (542.1,349.2): et~9 britanniques~10
# 106 Fixation 11 (495.9,345.4): américains~8
# 127 Fixation 12 (692.4,349): britanniques~10
# 170 Fixation 13 (331.8,394): permettre~13 à~14
# 114 Fixation 14 (288.6,393.3): permettre~13
# 310 Fixation 15 (473.9,402.6): ses~15 avions~16
# 143 Fixation 16 (418.5,403): à~14
# 137 Fixation 17 (558.1,410.8): avions~16 civils~17
# 214 Fixation 18 (335.2,443.7): permettre~13 à~14
# 121 Fixation 19 (300.3,462.6): transporter~19
# 248 Fixation 20 (466.5,466.7): des~20 scientifiques~21
# 311 Fixation 21 (613.3,474.6): scientifiques~21 étrangers~22
# 126 Fixation 22 (470.1,471.8): des~20 scientifiques~21
# 112 Fixation 23 (612.9,471.8): scientifiques~21 étrangers~22
# 293 Fixation 24 (325.2,514.8): L'Irak~23
# 171 Fixation 25 (400.4,540): est~24 soumis~25
# 201 Fixation 26 (534,544): à~26 embargo~28
# 253 Fixation 27 (638,538.1): embargo~28 aérien~29
# 193 Fixation 28 (351.8,558.2): L'Irak~23 soumis~25
# 102 Fixation 29 (256.7,582.2): depuis~30 1990~31
# 125 Fixation 30 (335,582.7): depuis~30 1990~31
# 202 Fixation 31 (494.8,529.2): soumis~25 à~26
# 198 Fixation 32 (364.9,559.5): est~24 soumis~25
# 140 Fixation 33 (337.6,575.7): depuis~30 1990~31
%%
# L'Irak~0 Fixations= "1/"
# a~1 Fixations= "1/"
# demandé~2 Fixations= "5/"
# à~3 Fixations= "5/"
# l'ONU~4 Fixations= "3/"
# d'empêcher~5 Fixations= "6/"
# les~6 Fixations= ""
# survols~7 Fixations= "7/"
# américains~8 Fixations= "11/"
# et~9 Fixations= "10/"
# britanniques~10 Fixations= "12/"
# et~11 Fixations= ""
# de~12 Fixations= ""
# permettre~13 Fixations= "18/"
# à~14 Fixations= "18/"
# ses~15 Fixations= "15/"
# avions~16 Fixations= "17/"
# civils~17 Fixations= "17/"
# de~18 Fixations= ""
# transporter~19 Fixations= "19/"
# des~20 Fixations= "22/"
# scientifiques~21 Fixations= "23/"
# étrangers~22 Fixations= "23/"
# L'Irak~23 Fixations= "28/"
# est~24 Fixations= "32/"
# soumis~25 Fixations= "32/"
# à~26 Fixations= "31/"
# un~27 Fixations= ""
# embargo~28 Fixations= "27/"
# aérien~29 Fixations= "27/"
# depuis~30 Fixations= "33/"
# 1990~31 Fixations= "33/"
%%
#  L'IRAK[1] A[1] DEMANDé[5] à[5] L'ONU[3] D'EMPêCHER[6] les SURVOLS[7] AMéRICAINS[11] ET[10] BRITANNIQUES[12] et de PERMETTRE[18] à[18] SES[15] AVIONS[17] CIVILS[17] de TRANSPORTER[19] DES[22] SCIENTIFIQUES[23] éTRANGERS[23] L'IRAK[28] EST[32] SOUMIS[32] à[31] un EMBARGO[27] AéRIEN[27] DEPUIS[33] 1990[33]
# s04 conflit_irak-f2 PercentageOfWordsProcessed= 0.84375 PercentageOfWordsReallyProcessed= 0.84375
