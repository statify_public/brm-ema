# 239 Fixation 1 (360.2,290.8): globalisation~1
# 246 Fixation 2 (528.1,281.7): de~2 l'économie~3
# 149 Fixation 3 (685.6,284.8): est~4 loin~5
# 247 Fixation 4 (353.3,334.7): d'être~6 achevée~7
# 190 Fixation 5 (603.9,275.2): l'économie~3
# 148 Fixation 6 (676.6,281): est~4 loin~5
# 180 Fixation 7 (358.5,345.3): d'être~6 achevée~7
# 135 Fixation 8 (297.8,340): d'être~6
# 307 Fixation 9 (428.3,336.7): achevée~7
# 139 Fixation 10 (523.1,336.7): et~8 contexte~11
# 176 Fixation 11 (498.3,281.9): de~2 l'économie~3
# 232 Fixation 12 (403.2,282.6): globalisation~1
# 242 Fixation 13 (553.6,299.2): l'économie~3
# 121 Fixation 14 (536.9,334.1): dans~9 contexte~11
# 154 Fixation 15 (629.8,339.7): un~10 contexte~11
# 107 Fixation 16 (736.9,342.1): de~12 fort~13
# 167 Fixation 17 (344.4,396.6): chômage~14
# 143 Fixation 18 (300.4,399.9): chômage~14
# 250 Fixation 19 (471.6,404.9): probabilité~16
# 151 Fixation 20 (448.4,406.9): probabilité~16
# 187 Fixation 21 (628.1,410.9): que~17 s'imposent~18
# 93 Fixation 22 (663.7,406.3): s'imposent~18
# 263 Fixation 23 (327,462.6): nouveau~20
# 167 Fixation 24 (442.7,461.9): vieux~22
# 237 Fixation 25 (523.5,465.7): mécanismes~23
# 167 Fixation 26 (706.8,404.8): s'imposent~18 à~19
# 163 Fixation 27 (329.6,463.1): nouveau~20
# 113 Fixation 28 (290.5,464.3): nouveau~20
# 288 Fixation 29 (424.7,476.7): les~21 vieux~22
# 138 Fixation 30 (543.9,483.9): mécanismes~23
# 327 Fixation 31 (669.9,478.6): de~24 hausse~25
# 319 Fixation 32 (340.4,534.3): prix~27 très~29
# 255 Fixation 33 (626.3,488.4): mécanismes~23 hausse~25
# 168 Fixation 34 (298.2,540.2): prix~27
# 129 Fixation 35 (387.1,465.2): les~21 vieux~22
# 129 Fixation 36 (482.8,474.5): vieux~22 mécanismes~23
# 89 Fixation 37 (318.5,558.8): prix~27 très~29
# 231 Fixation 38 (416.1,533.2): très~29 faible~30
# 155 Fixation 39 (346.2,521.8): est~28 très~29
%%
# La~0 Fixations= ""
# globalisation~1 Fixations= "12/"
# de~2 Fixations= "11/"
# l'économie~3 Fixations= "13/"
# est~4 Fixations= "6/"
# loin~5 Fixations= "6/"
# d'être~6 Fixations= "8/"
# achevée~7 Fixations= "9/"
# et~8 Fixations= "10/"
# dans~9 Fixations= "14/"
# un~10 Fixations= "15/"
# contexte~11 Fixations= "15/"
# de~12 Fixations= "16/"
# fort~13 Fixations= "16/"
# chômage~14 Fixations= "18/"
# la~15 Fixations= ""
# probabilité~16 Fixations= "20/"
# que~17 Fixations= "21/"
# s'imposent~18 Fixations= "26/"
# à~19 Fixations= "26/"
# nouveau~20 Fixations= "28/"
# les~21 Fixations= "35/"
# vieux~22 Fixations= "36/"
# mécanismes~23 Fixations= "36/"
# de~24 Fixations= "31/"
# hausse~25 Fixations= "33/"
# des~26 Fixations= ""
# prix~27 Fixations= "37/"
# est~28 Fixations= "39/"
# très~29 Fixations= "39/"
# faible~30 Fixations= "38/"
%%
#  La GLOBALISATION[12] DE[11] L'éCONOMIE[13] EST[6] LOIN[6] D'êTRE[8] ACHEVéE[9] ET[10] DANS[14] UN[15] CONTEXTE[15] DE[16] FORT[16] CHôMAGE[18] la PROBABILITé[20] QUE[21] S'IMPOSENT[26] à[26] NOUVEAU[28] LES[35] VIEUX[36] MéCANISMES[36] DE[31] HAUSSE[33] des PRIX[37] EST[39] TRèS[39] FAIBLE[38]
# s04 croissance_economie-m1 PercentageOfWordsProcessed= 0.903225806451613 PercentageOfWordsReallyProcessed= 0.903225806451613
