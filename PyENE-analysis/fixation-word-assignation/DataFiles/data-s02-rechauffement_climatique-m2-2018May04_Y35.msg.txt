# 84 Fixation 1 (271.1,283.2): Un~0 groupe~1
# 118 Fixation 2 (314.2,284.1): Un~0 groupe~1
# 220 Fixation 3 (408,291.6): d'ingénieurs~2
# 193 Fixation 4 (526,297.8): d'ingénieurs~2
# 173 Fixation 5 (580.1,298.3): météorologistes~3
# 123 Fixation 6 (662.2,294.8): météorologistes~3
# 172 Fixation 7 (708.5,300.1): météorologistes~3 a~4
# 197 Fixation 8 (293.7,341.5): obtenu~5
# 184 Fixation 9 (372.4,343.1): le~6 prix~7
# 182 Fixation 10 (471.4,352.6): prix~7 Nobel~8
# 89 Fixation 11 (562.4,357): pour~9 travaux~11
# 126 Fixation 12 (616.2,357.1): pour~9 travaux~11
# 215 Fixation 13 (296.2,399.3): relatifs~12
# 107 Fixation 14 (415.1,410.5): à~13 prévision~15
# 157 Fixation 15 (475.6,407.6): prévision~15
# 249 Fixation 16 (625.9,416.5): des~16 évènements~17
# 163 Fixation 17 (282.9,469.3): climatiques~18
# 110 Fixation 18 (330.7,472.9): climatiques~18
# 106 Fixation 19 (431.6,472.3): à~19 long~20
# 148 Fixation 20 (507.3,474.5): long~20 terme~21
# 122 Fixation 21 (635.6,477.2): et~22 effets~24
# 95 Fixation 22 (681.1,481.5): aux~23 effets~24
# 188 Fixation 23 (308.8,528.8): la~26 pollution~27
# 158 Fixation 24 (502.8,525): l'atmosphère~29
%%
# Un~0 Fixations= "2/"
# groupe~1 Fixations= "2/"
# d'ingénieurs~2 Fixations= "4/"
# météorologistes~3 Fixations= "7/"
# a~4 Fixations= "7/"
# obtenu~5 Fixations= "8/"
# le~6 Fixations= "9/"
# prix~7 Fixations= "10/"
# Nobel~8 Fixations= "10/"
# pour~9 Fixations= "12/"
# ses~10 Fixations= ""
# travaux~11 Fixations= "12/"
# relatifs~12 Fixations= "13/"
# à~13 Fixations= "14/"
# la~14 Fixations= ""
# prévision~15 Fixations= "15/"
# des~16 Fixations= "16/"
# évènements~17 Fixations= "16/"
# climatiques~18 Fixations= "18/"
# à~19 Fixations= "19/"
# long~20 Fixations= "20/"
# terme~21 Fixations= "20/"
# et~22 Fixations= "21/"
# aux~23 Fixations= "22/"
# effets~24 Fixations= "22/"
# de~25 Fixations= ""
# la~26 Fixations= "23/"
# pollution~27 Fixations= "23/"
# de~28 Fixations= ""
# l'atmosphère~29 Fixations= "24/"
%%
#  UN[2] GROUPE[2] D'INGéNIEURS[4] MéTéOROLOGISTES[7] A[7] OBTENU[8] LE[9] PRIX[10] NOBEL[10] POUR[12] ses TRAVAUX[12] RELATIFS[13] à[14] la PRéVISION[15] DES[16] éVèNEMENTS[16] CLIMATIQUES[18] à[19] LONG[20] TERME[20] ET[21] AUX[22] EFFETS[22] de LA[23] POLLUTION[23] de L'ATMOSPHèRE[24]
# s02 rechauffement_climatique-m2 PercentageOfWordsProcessed= 0.866666666666667 PercentageOfWordsReallyProcessed= 0.866666666666667
