# 105 Fixation 1 (275.1,288.2): La~0 moitié~1
# 177 Fixation 2 (436.2,274.1): des~2 tout-petits~3
# 212 Fixation 3 (479.4,275.3): tout-petits~3
# 378 Fixation 4 (610.2,289.3): sont~4
# 208 Fixation 5 (338.6,345.6): foyer~7
# 149 Fixation 6 (387.6,346): par~8 parent~10
# 272 Fixation 7 (559.2,349.3): la~11 maman~12
# 284 Fixation 8 (359.1,396.2): quasi-totalité~15
# 130 Fixation 9 (455.8,420.1): des~16 cas~17
# 185 Fixation 10 (363,475.4): crèche~24
# 165 Fixation 11 (466.2,465.4): collective~25
# 251 Fixation 12 (588.8,423.6): Seuls~18
# 237 Fixation 13 (661.4,413): Seuls~18 8%~19 ont~20
# 122 Fixation 14 (626.2,506.7): ou~26 parentale~27
%%
# La~0 Fixations= "1/"
# moitié~1 Fixations= "1/"
# des~2 Fixations= "2/"
# tout-petits~3 Fixations= "3/"
# sont~4 Fixations= "4/"
# gardés~5 Fixations= ""
# au~6 Fixations= ""
# foyer~7 Fixations= "5/"
# par~8 Fixations= "6/"
# un~9 Fixations= ""
# parent~10 Fixations= "6/"
# la~11 Fixations= "7/"
# maman~12 Fixations= "7/"
# dans~13 Fixations= ""
# la~14 Fixations= ""
# quasi-totalité~15 Fixations= "8/"
# des~16 Fixations= "9/"
# cas~17 Fixations= "9/"
# Seuls~18 Fixations= "13/"
# 8%~19 Fixations= "13/"
# ont~20 Fixations= "13/"
# accès~21 Fixations= ""
# à~22 Fixations= ""
# une~23 Fixations= ""
# crèche~24 Fixations= "10/"
# collective~25 Fixations= "11/"
# ou~26 Fixations= "14/"
# parentale~27 Fixations= "14/"
%%
#  LA[1] MOITIé[1] DES[2] TOUT-PETITS[3] SONT[4] gardés au FOYER[5] PAR[6] un PARENT[6] LA[7] MAMAN[7] dans la QUASI-TOTALITé[8] DES[9] CAS[9] SEULS[13] 8%[13] ONT[13] accès à une CRèCHE[10] COLLECTIVE[11] OU[14] PARENTALE[14]
# s18 allocation_familiale-m1 PercentageOfWordsProcessed= 0.714285714285714 PercentageOfWordsReallyProcessed= 0.714285714285714
