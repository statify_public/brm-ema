# 162 Fixation 1 (264.1,297.7): En~0 raison~1
# 152 Fixation 2 (286.6,294.7): En~0 raison~1
# 174 Fixation 3 (398.5,294.7): raison~1 travaux~3
# 127 Fixation 4 (447.7,286.5): des~2 travaux~3
# 296 Fixation 5 (569.5,287.3): entrepris~4
# 183 Fixation 6 (699.1,289.6): dans~5 l'aile~6
# 158 Fixation 7 (731.5,296.3): dans~5 l'aile~6
# 221 Fixation 8 (276.6,348.6): nord~7
# 150 Fixation 9 (262.6,347.7): nord~7
# 217 Fixation 10 (355.5,345.1): du~8 château~9
# 242 Fixation 11 (504.2,346.3): de~10 Versailles~11
# 310 Fixation 12 (689.2,353.3): les~12 salles~13
# 99 Fixation 13 (343.5,395.5): croisades~15
# 235 Fixation 14 (313.2,411.4): des~14 croisades~15
# 213 Fixation 15 (333.2,414.8): des~14 croisades~15
# 220 Fixation 16 (491.6,416.8): et~16 circuit~18
# 161 Fixation 17 (517.9,414.8): le~17 circuit~18
# 109 Fixation 18 (626.1,414.9): circuit~18 XVIIe~20
# 138 Fixation 19 (311.5,494.2): siècle~21
# 104 Fixation 20 (284.5,489.2): siècle~21
# 169 Fixation 21 (375.9,478): qui~22 font~23
# 154 Fixation 22 (415,477.2): qui~22 font~23
# 172 Fixation 23 (493.6,478.6): font~23 partie~24
# 279 Fixation 24 (270.1,557.4): l'histoire~28
# 250 Fixation 25 (445.2,534.5): de~29 France~30
# 195 Fixation 26 (564.6,541.5): seront~31
# 232 Fixation 27 (654.6,535.8): fermées~32
%%
# En~0 Fixations= "2/"
# raison~1 Fixations= "3/"
# des~2 Fixations= "4/"
# travaux~3 Fixations= "4/"
# entrepris~4 Fixations= "5/"
# dans~5 Fixations= "7/"
# l'aile~6 Fixations= "7/"
# nord~7 Fixations= "9/"
# du~8 Fixations= "10/"
# château~9 Fixations= "10/"
# de~10 Fixations= "11/"
# Versailles~11 Fixations= "11/"
# les~12 Fixations= "12/"
# salles~13 Fixations= "12/"
# des~14 Fixations= "15/"
# croisades~15 Fixations= "15/"
# et~16 Fixations= "16/"
# le~17 Fixations= "17/"
# circuit~18 Fixations= "18/"
# du~19 Fixations= ""
# XVIIe~20 Fixations= "18/"
# siècle~21 Fixations= "20/"
# qui~22 Fixations= "22/"
# font~23 Fixations= "23/"
# partie~24 Fixations= "23/"
# du~25 Fixations= ""
# musée~26 Fixations= ""
# de~27 Fixations= ""
# l'histoire~28 Fixations= "24/"
# de~29 Fixations= "25/"
# France~30 Fixations= "25/"
# seront~31 Fixations= "26/"
# fermées~32 Fixations= "27/"
%%
#  EN[2] RAISON[3] DES[4] TRAVAUX[4] ENTREPRIS[5] DANS[7] L'AILE[7] NORD[9] DU[10] CHâTEAU[10] DE[11] VERSAILLES[11] LES[12] SALLES[12] DES[15] CROISADES[15] ET[16] LE[17] CIRCUIT[18] du XVIIE[18] SIèCLE[20] QUI[22] FONT[23] PARTIE[23] du musée de L'HISTOIRE[24] DE[25] FRANCE[25] SERONT[26] FERMéES[27]
# s13 art_contemporain-m1 PercentageOfWordsProcessed= 0.878787878787879 PercentageOfWordsReallyProcessed= 0.878787878787879
