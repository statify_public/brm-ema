# 146 Fixation 1 (299.3,294.5): La~0 CRIIRAD~1
# 111 Fixation 2 (368.3,288.5): CRIIRAD~1 a~2
# 158 Fixation 3 (451.4,281.9): a~2 dénoncé~3
# 210 Fixation 4 (570.9,284.6): la~4 sous-évaluation~5
# 171 Fixation 5 (653.8,288.7): sous-évaluation~5
# 240 Fixation 6 (295.6,356): la~7 Cogema~8
# 150 Fixation 7 (350.8,351.9): Cogema~8
# 242 Fixation 8 (485.2,353.4): la~10 radioactivité~11
# 161 Fixation 9 (563.8,354.8): radioactivité~11
# 291 Fixation 10 (658,359.9): produite~12
# 129 Fixation 11 (697.5,360.9): produite~12
# 218 Fixation 12 (285.7,402.8): des~14 eaux~15
# 318 Fixation 13 (414.5,406.6): de~16 ruissellement~17
%%
# La~0 Fixations= "1/"
# CRIIRAD~1 Fixations= "2/"
# a~2 Fixations= "3/"
# dénoncé~3 Fixations= "3/"
# la~4 Fixations= "4/"
# sous-évaluation~5 Fixations= "5/"
# par~6 Fixations= ""
# la~7 Fixations= "6/"
# Cogema~8 Fixations= "7/"
# de~9 Fixations= ""
# la~10 Fixations= "8/"
# radioactivité~11 Fixations= "9/"
# produite~12 Fixations= "11/"
# par~13 Fixations= ""
# des~14 Fixations= "12/"
# eaux~15 Fixations= "12/"
# de~16 Fixations= "13/"
# ruissellement~17 Fixations= "13/"
# d'anciennes~18 Fixations= ""
# mines~19 Fixations= ""
# d'uranium~20 Fixations= ""
# en~21 Fixations= ""
# Loire-atlantique~22 Fixations= ""
%%
#  LA[1] CRIIRAD[2] A[3] DéNONCé[3] LA[4] SOUS-éVALUATION[5] par LA[6] COGEMA[7] de LA[8] RADIOACTIVITé[9] PRODUITE[11] par DES[12] EAUX[12] DE[13] RUISSELLEMENT[13] d'anciennes mines d'uranium en Loire-atlantique
# s06 dechets_nucleaires-f1 PercentageOfWordsProcessed= 0.652173913043478 PercentageOfWordsReallyProcessed= 0.833333333333333
