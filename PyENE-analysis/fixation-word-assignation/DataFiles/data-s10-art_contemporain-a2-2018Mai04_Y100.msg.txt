# 145 Fixation 1 (344.1,291.2): information~1
# 99 Fixation 2 (382.6,285.6): information~1
# 137 Fixation 3 (482.1,282.6): judiciaire~2
# 124 Fixation 4 (635.7,271.3): pour~3 blessures~4
# 121 Fixation 5 (703.8,274.3): blessures~4
# 196 Fixation 6 (342.1,343.4): information~1 involontaires~5
# 162 Fixation 7 (485.2,344.8): judiciaire~2 commises~6
# 127 Fixation 8 (611.9,365.4): par~7 négligences~8
# 123 Fixation 9 (651.5,348.8): pour~3 blessures~4 négligences~8
# 143 Fixation 10 (584.3,458.2): chute~14 wagon~22
# 221 Fixation 11 (642.1,477): non~23 équipé~24
# 107 Fixation 12 (413.8,517.1): tombé~20
# 133 Fixation 13 (296.6,536.7): six~18 ans~19 fermetures~26
# 123 Fixation 14 (509.3,535.7): d'un~21 wagon~22 sécurité~28
# 169 Fixation 15 (589.9,549): du~29 train~30
# 176 Fixation 16 (350.3,619.9): Toulouse-Paris~31
# 151 Fixation 17 (290.2,615.5): Toulouse-Paris~31
# 138 Fixation 18 (519.7,423.9): la~13 chute~14
%%
# Une~0 Fixations= ""
# information~1 Fixations= "6/"
# judiciaire~2 Fixations= "7/"
# pour~3 Fixations= "9/"
# blessures~4 Fixations= "9/"
# involontaires~5 Fixations= "6/"
# commises~6 Fixations= "7/"
# par~7 Fixations= "8/"
# négligences~8 Fixations= "9/"
# a~9 Fixations= ""
# été~10 Fixations= ""
# ouverte~11 Fixations= ""
# après~12 Fixations= ""
# la~13 Fixations= "18/"
# chute~14 Fixations= "18/"
# d'un~15 Fixations= ""
# enfant~16 Fixations= ""
# de~17 Fixations= ""
# six~18 Fixations= "13/"
# ans~19 Fixations= "13/"
# tombé~20 Fixations= "12/"
# d'un~21 Fixations= "14/"
# wagon~22 Fixations= "14/"
# non~23 Fixations= "11/"
# équipé~24 Fixations= "11/"
# de~25 Fixations= ""
# fermetures~26 Fixations= "13/"
# de~27 Fixations= ""
# sécurité~28 Fixations= "14/"
# du~29 Fixations= "15/"
# train~30 Fixations= "15/"
# Toulouse-Paris~31 Fixations= "17/"
%%
#  Une INFORMATION[6] JUDICIAIRE[7] POUR[9] BLESSURES[9] INVOLONTAIRES[6] COMMISES[7] PAR[8] NéGLIGENCES[9] a été ouverte après LA[18] CHUTE[18] d'un enfant de SIX[13] ANS[13] TOMBé[12] D'UN[14] WAGON[14] NON[11] éQUIPé[11] de FERMETURES[13] de SéCURITé[14] DU[15] TRAIN[15] TOULOUSE-PARIS[17]
# s10 art_contemporain-a2 PercentageOfWordsProcessed= 0.6875 PercentageOfWordsReallyProcessed= 0.6875
