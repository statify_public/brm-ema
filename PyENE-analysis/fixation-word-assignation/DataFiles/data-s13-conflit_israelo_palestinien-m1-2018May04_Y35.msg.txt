# 212 Fixation 1 (285.2,277.4): Le~0 chef~1
# 112 Fixation 2 (303.1,278): Le~0 chef~1
# 193 Fixation 3 (395,284.7): du~2 gouvernement~3
# 175 Fixation 4 (570.9,281.7): est~4 accusé~5
# 264 Fixation 5 (609.1,280.1): est~4 accusé~5
# 141 Fixation 6 (693.7,281.2): accusé~5
# 311 Fixation 7 (282.7,339.8): relations~7
# 378 Fixation 8 (412.6,345.2): tendues~8
# 201 Fixation 9 (522,340.4): avec~9 États-Unis~11
# 215 Fixation 10 (590.1,341.6): les~10 États-Unis~11
# 109 Fixation 11 (683,347.3): États-Unis~11
# 132 Fixation 12 (732.5,352.5): États-Unis~11
# 116 Fixation 13 (679,349.9): États-Unis~11
# 119 Fixation 14 (731.9,351.5): États-Unis~11
# 323 Fixation 15 (286.3,398.5): l'isolement~13
# 229 Fixation 16 (442,407.9): d'Israël~14
# 281 Fixation 17 (461.5,403.4): d'Israël~14
# 131 Fixation 18 (583.5,400.1): dans~15 monde~17
%%
# Le~0 Fixations= "2/"
# chef~1 Fixations= "2/"
# du~2 Fixations= "3/"
# gouvernement~3 Fixations= "3/"
# est~4 Fixations= "5/"
# accusé~5 Fixations= "6/"
# des~6 Fixations= ""
# relations~7 Fixations= "7/"
# tendues~8 Fixations= "8/"
# avec~9 Fixations= "9/"
# les~10 Fixations= "10/"
# États-Unis~11 Fixations= "14/"
# de~12 Fixations= ""
# l'isolement~13 Fixations= "15/"
# d'Israël~14 Fixations= "17/"
# dans~15 Fixations= "18/"
# le~16 Fixations= ""
# monde~17 Fixations= "18/"
# du~18 Fixations= ""
# chômage~19 Fixations= ""
# en~20 Fixations= ""
# montée~21 Fixations= ""
# vertigineuse~22 Fixations= ""
# et~23 Fixations= ""
# de~24 Fixations= ""
# la~25 Fixations= ""
# détérioration~26 Fixations= ""
# de~27 Fixations= ""
# toutes~28 Fixations= ""
# les~29 Fixations= ""
# normes~30 Fixations= ""
# de~31 Fixations= ""
# gouvernement~32 Fixations= ""
# démocratique~33 Fixations= ""
%%
#  LE[2] CHEF[2] DU[3] GOUVERNEMENT[3] EST[5] ACCUSé[6] des RELATIONS[7] TENDUES[8] AVEC[9] LES[10] ÉTATS-UNIS[14] de L'ISOLEMENT[15] D'ISRAëL[17] DANS[18] le MONDE[18] du chômage en montée vertigineuse et de la détérioration de toutes les normes de gouvernement démocratique
# s13 conflit_israelo_palestinien-m1 PercentageOfWordsProcessed= 0.441176470588235 PercentageOfWordsReallyProcessed= 0.833333333333333
