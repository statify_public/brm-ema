# 134 Fixation 1 (289,280.9): L'artillerie~0
# 157 Fixation 2 (425.4,274.3): israélienne~1
# 210 Fixation 3 (593.2,262.9): a~2
# 182 Fixation 4 (287.6,344.5): bombardé~4
# 204 Fixation 5 (447,335.7): des~5 régions~6
# 129 Fixation 6 (475.6,399.3): Hezbollah~12
# 214 Fixation 7 (590.6,394.1): au~13 Liban~14
# 147 Fixation 8 (412.4,394.3): du~11 Hezbollah~12
# 160 Fixation 9 (633.5,513.5): intégriste~28
# 160 Fixation 10 (570.1,524.3): milice~27 intégriste~28
# 139 Fixation 11 (704.1,521.5): intégriste~28 Chiite~29
# 118 Fixation 12 (424.2,532.2): de~25 milice~27
# 170 Fixation 13 (320.3,572): meurtrières~24
# 302 Fixation 14 (261.3,576.1): contre~30 Israël~31
# 142 Fixation 15 (391.8,349.7): des~5 régions~6
# 174 Fixation 16 (601.3,471.5): série~22
# 85 Fixation 17 (651.9,479.9): d'attaques~23
# 173 Fixation 18 (370.8,513.5): meurtrières~24
# 207 Fixation 19 (458.1,517.8): de~25 milice~27
# 197 Fixation 20 (609.4,529.9): intégriste~28
# 97 Fixation 21 (731.6,529.5): Chiite~29
%%
# L'artillerie~0 Fixations= "1/"
# israélienne~1 Fixations= "2/"
# a~2 Fixations= "3/"
# massivement~3 Fixations= ""
# bombardé~4 Fixations= "4/"
# des~5 Fixations= "15/"
# régions~6 Fixations= "15/"
# supposées~7 Fixations= ""
# être~8 Fixations= ""
# des~9 Fixations= ""
# bastions~10 Fixations= ""
# du~11 Fixations= "8/"
# Hezbollah~12 Fixations= "8/"
# au~13 Fixations= "7/"
# Liban~14 Fixations= "7/"
# Ces~15 Fixations= ""
# tirs~16 Fixations= ""
# sont~17 Fixations= ""
# une~18 Fixations= ""
# riposte~19 Fixations= ""
# à~20 Fixations= ""
# une~21 Fixations= ""
# série~22 Fixations= "16/"
# d'attaques~23 Fixations= "17/"
# meurtrières~24 Fixations= "18/"
# de~25 Fixations= "19/"
# la~26 Fixations= ""
# milice~27 Fixations= "19/"
# intégriste~28 Fixations= "20/"
# Chiite~29 Fixations= "21/"
# contre~30 Fixations= "14/"
# Israël~31 Fixations= "14/"
%%
#  L'ARTILLERIE[1] ISRAéLIENNE[2] A[3] massivement BOMBARDé[4] DES[15] RéGIONS[15] supposées être des bastions DU[8] HEZBOLLAH[8] AU[7] LIBAN[7] Ces tirs sont une riposte à une SéRIE[16] D'ATTAQUES[17] MEURTRIèRES[18] DE[19] la MILICE[19] INTéGRISTE[20] CHIITE[21] CONTRE[14] ISRAëL[14]
# s19 conflit_israelo_palestinien-f1 PercentageOfWordsProcessed= 0.59375 PercentageOfWordsReallyProcessed= 0.59375
