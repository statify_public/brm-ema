# 140 Fixation 1 (388.8,303.6): habitants~1
# 128 Fixation 2 (339.6,293.1): habitants~1
# 85 Fixation 3 (491.5,290.7): de~2 Tirana~3
# 127 Fixation 4 (611.7,285.4): compatissent~4
# 124 Fixation 5 (492.4,287.7): de~2 Tirana~3
# 156 Fixation 6 (442.9,289.4): de~2 Tirana~3
# 229 Fixation 7 (590.5,288): compatissent~4
# 161 Fixation 8 (697.3,287): compatissent~4
# 168 Fixation 9 (363.3,345.1): malheurs~6
# 155 Fixation 10 (291.8,341.8): malheurs~6
# 178 Fixation 11 (485.7,340.8): leurs~8 frères~9
# 170 Fixation 12 (573.4,341.9): frères~9 kosovars~10
# 148 Fixation 13 (431.4,339.7): de~7 frères~9
# 147 Fixation 14 (548.5,340.8): frères~9
# 246 Fixation 15 (597.5,346): kosovars~10
# 198 Fixation 16 (581.5,347.7): frères~9 kosovars~10
# 160 Fixation 17 (695.2,345.5): kosovars~10
# 217 Fixation 18 (329.6,394.8): déclarent~13
# 151 Fixation 19 (296.6,397.7): déclarent~13
# 200 Fixation 20 (427.8,397.7): ulcérés~14
# 100 Fixation 21 (555.5,406.6): par~15 l'enchaînement~16
# 194 Fixation 22 (383.8,401.8): déclarent~13 ulcérés~14
# 145 Fixation 23 (575,411.5): l'enchaînement~16
# 131 Fixation 24 (497.7,404.4): ulcérés~14
# 228 Fixation 25 (329.3,448.9): d'événements~17
# 225 Fixation 26 (498.4,467.7): qu'ils~18 ressentent~19
# 121 Fixation 27 (637.7,463.9): ressentent~19
# 106 Fixation 28 (577.4,458.9): ressentent~19
# 144 Fixation 29 (693,467.7): comme~20
# 110 Fixation 30 (378.7,483.3): d'événements~17 qu'ils~18
# 157 Fixation 31 (291,510.1): profonde~22
# 176 Fixation 32 (402.9,511.2): injustice~23
# 137 Fixation 33 (603.6,529.4): reprochent~25
# 160 Fixation 34 (684.8,530.3): à~26
# 183 Fixation 35 (499.7,512): injustice~23 reprochent~25
# 101 Fixation 36 (558.7,515.6): et~24 reprochent~25
# 191 Fixation 37 (330.2,581.4): d'avoir~28 lancé~29
# 81 Fixation 38 (301.2,580.8): d'avoir~28
# 150 Fixation 39 (460.6,580.4): des~30 opérations~31
# 139 Fixation 40 (534.1,585.3): opérations~31
# 157 Fixation 41 (625.6,591.4): opérations~31 armées~32
# 164 Fixation 42 (473.4,338.1): leurs~8 frères~9
# 181 Fixation 43 (454.8,307.4): de~2 Tirana~3
# 257 Fixation 44 (340.4,309.3): habitants~1
# 247 Fixation 45 (475.1,306.4): de~2 Tirana~3
# 230 Fixation 46 (592.1,296.8): compatissent~4
# 241 Fixation 47 (487.7,413.7): ulcérés~14
# 248 Fixation 48 (645.9,569.5): reprochent~25 à~26
%%
# Les~0 Fixations= ""
# habitants~1 Fixations= "44/"
# de~2 Fixations= "45/"
# Tirana~3 Fixations= "45/"
# compatissent~4 Fixations= "46/"
# aux~5 Fixations= ""
# malheurs~6 Fixations= "10/"
# de~7 Fixations= "13/"
# leurs~8 Fixations= "42/"
# frères~9 Fixations= "42/"
# kosovars~10 Fixations= "17/"
# Ils~11 Fixations= ""
# se~12 Fixations= ""
# déclarent~13 Fixations= "22/"
# ulcérés~14 Fixations= "47/"
# par~15 Fixations= "21/"
# l'enchaînement~16 Fixations= "23/"
# d'événements~17 Fixations= "30/"
# qu'ils~18 Fixations= "30/"
# ressentent~19 Fixations= "28/"
# comme~20 Fixations= "29/"
# une~21 Fixations= ""
# profonde~22 Fixations= "31/"
# injustice~23 Fixations= "35/"
# et~24 Fixations= "36/"
# reprochent~25 Fixations= "48/"
# à~26 Fixations= "48/"
# l'UCK~27 Fixations= ""
# d'avoir~28 Fixations= "38/"
# lancé~29 Fixations= "37/"
# des~30 Fixations= "39/"
# opérations~31 Fixations= "41/"
# armées~32 Fixations= "41/"
%%
#  Les HABITANTS[44] DE[45] TIRANA[45] COMPATISSENT[46] aux MALHEURS[10] DE[13] LEURS[42] FRèRES[42] KOSOVARS[17] Ils se DéCLARENT[22] ULCéRéS[47] PAR[21] L'ENCHAîNEMENT[23] D'éVéNEMENTS[30] QU'ILS[30] RESSENTENT[28] COMME[29] une PROFONDE[31] INJUSTICE[35] ET[36] REPROCHENT[48] à[48] l'UCK D'AVOIR[38] LANCé[37] DES[39] OPéRATIONS[41] ARMéES[41]
# s04 aide_refugies-m2 PercentageOfWordsProcessed= 0.818181818181818 PercentageOfWordsReallyProcessed= 0.818181818181818
