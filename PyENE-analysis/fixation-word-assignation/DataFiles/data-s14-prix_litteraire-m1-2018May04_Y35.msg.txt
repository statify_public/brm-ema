# 188 Fixation 1 (290.9,287.1): Le~0 jeu~1
# 382 Fixation 2 (385.6,282.7): favori~2
# 223 Fixation 3 (317.7,271.9): Le~0 jeu~1
# 280 Fixation 4 (479.5,276.1): des~3 biographes~4
# 200 Fixation 5 (444.7,296.9): favori~2 biographes~4
# 150 Fixation 6 (434,276.7): favori~2
# 270 Fixation 7 (519.6,272.1): biographes~4
# 337 Fixation 8 (636.7,265): de~5 Conrad~6
# 187 Fixation 9 (661.8,269.6): de~5 Conrad~6
# 155 Fixation 10 (357.5,346.4): célèbre~8 écrivain~9
# 228 Fixation 11 (302.3,354): célèbre~8
# 133 Fixation 12 (427,393.4): l'écart~14
# 118 Fixation 13 (480.9,409.4): entre~15
# 508 Fixation 14 (590.3,429.4): les~16 événements~17
%%
# Le~0 Fixations= "3/"
# jeu~1 Fixations= "3/"
# favori~2 Fixations= "6/"
# des~3 Fixations= "4/"
# biographes~4 Fixations= "7/"
# de~5 Fixations= "9/"
# Conrad~6 Fixations= "9/"
# le~7 Fixations= ""
# célèbre~8 Fixations= "11/"
# écrivain~9 Fixations= "10/"
# anglais~10 Fixations= ""
# consiste~11 Fixations= ""
# à~12 Fixations= ""
# mesurer~13 Fixations= ""
# l'écart~14 Fixations= "12/"
# entre~15 Fixations= "13/"
# les~16 Fixations= "14/"
# événements~17 Fixations= "14/"
# de~18 Fixations= ""
# sa~19 Fixations= ""
# vie~20 Fixations= ""
# réelle~21 Fixations= ""
# et~22 Fixations= ""
# les~23 Fixations= ""
# romans~24 Fixations= ""
# qu'il~25 Fixations= ""
# en~26 Fixations= ""
# a~27 Fixations= ""
# tirés~28 Fixations= ""
%%
#  LE[3] JEU[3] FAVORI[6] DES[4] BIOGRAPHES[7] DE[9] CONRAD[9] le CéLèBRE[11] éCRIVAIN[10] anglais consiste à mesurer L'éCART[12] ENTRE[13] LES[14] éVéNEMENTS[14] de sa vie réelle et les romans qu'il en a tirés
# s14 prix_litteraire-m1 PercentageOfWordsProcessed= 0.448275862068966 PercentageOfWordsReallyProcessed= 0.722222222222222
