# 155 Fixation 1 (377.8,304.8): de~1 dynamique~3
# 182 Fixation 2 (462.1,300.1): la~2 dynamique~3
# 88 Fixation 3 (325.9,298): L'étude~0
# 273 Fixation 4 (618.8,293.2): des~4 anneaux~5
# 198 Fixation 5 (352.9,364.5): entourent~7 toutes~8
# 101 Fixation 6 (310.5,363): entourent~7
# 158 Fixation 7 (569,362.5): planètes~10
# 286 Fixation 8 (671.9,352.5): géantes~11
# 124 Fixation 9 (560.3,350.8): planètes~10
# 97 Fixation 10 (709,350.4): géantes~11
# 187 Fixation 11 (355.2,408.8): orbite~13 autour~14
# 238 Fixation 12 (314.7,414.1): orbite~13
# 245 Fixation 13 (473.2,419.3): du~15 Soleil~16
# 157 Fixation 14 (411.1,411.9): autour~14
# 85 Fixation 15 (599.4,414.4): aide~17
# 198 Fixation 16 (409.8,277.3): de~1 dynamique~3
# 143 Fixation 17 (322.5,284.3): L'étude~0
# 196 Fixation 18 (555.9,282.1): dynamique~3 anneaux~5
# 149 Fixation 19 (623.2,292): des~4 anneaux~5
# 294 Fixation 20 (473,283.2): dynamique~3
# 123 Fixation 21 (325.2,298.7): L'étude~0
# 175 Fixation 22 (335.1,368.4): entourent~7
# 144 Fixation 23 (453.8,354.5): toutes~8
# 155 Fixation 24 (578.7,343.7): planètes~10
# 224 Fixation 25 (681.7,345.2): géantes~11
# 144 Fixation 26 (633.3,346.2): planètes~10 géantes~11
# 208 Fixation 27 (399.9,418): autour~14
# 121 Fixation 28 (306.9,416.4): orbite~13
# 143 Fixation 29 (599.3,407.1): aide~17
# 155 Fixation 30 (719,403.5): astronomes~19
# 168 Fixation 31 (369,478.9): comprendre~21
# 186 Fixation 32 (313.8,471.1): à~20 comprendre~21
# 320 Fixation 33 (470.3,473.2): le~22 processus~23
# 209 Fixation 34 (631.6,474.3): de~24 formation~25
# 82 Fixation 35 (705.4,473.1): formation~25
# 209 Fixation 36 (366.3,530): du~28 système~29
# 111 Fixation 37 (293.8,537.9): lunes~27
# 147 Fixation 38 (621,491.1): de~24 formation~25
# 307 Fixation 39 (536.1,482.9): processus~23
# 223 Fixation 40 (448.3,540.2): système~29 solaire~30
# 222 Fixation 41 (376.6,538.3): du~28 système~29
%%
# L'étude~0 Fixations= "21/"
# de~1 Fixations= "16/"
# la~2 Fixations= "2/"
# dynamique~3 Fixations= "20/"
# des~4 Fixations= "19/"
# anneaux~5 Fixations= "19/"
# qui~6 Fixations= ""
# entourent~7 Fixations= "22/"
# toutes~8 Fixations= "23/"
# les~9 Fixations= ""
# planètes~10 Fixations= "26/"
# géantes~11 Fixations= "26/"
# en~12 Fixations= ""
# orbite~13 Fixations= "28/"
# autour~14 Fixations= "27/"
# du~15 Fixations= "13/"
# Soleil~16 Fixations= "13/"
# aide~17 Fixations= "29/"
# les~18 Fixations= ""
# astronomes~19 Fixations= "30/"
# à~20 Fixations= "32/"
# comprendre~21 Fixations= "32/"
# le~22 Fixations= "33/"
# processus~23 Fixations= "39/"
# de~24 Fixations= "38/"
# formation~25 Fixations= "38/"
# des~26 Fixations= ""
# lunes~27 Fixations= "37/"
# du~28 Fixations= "41/"
# système~29 Fixations= "41/"
# solaire~30 Fixations= "40/"
%%
#  L'éTUDE[21] DE[16] LA[2] DYNAMIQUE[20] DES[19] ANNEAUX[19] qui ENTOURENT[22] TOUTES[23] les PLANèTES[26] GéANTES[26] en ORBITE[28] AUTOUR[27] DU[13] SOLEIL[13] AIDE[29] les ASTRONOMES[30] à[32] COMPRENDRE[32] LE[33] PROCESSUS[39] DE[38] FORMATION[38] des LUNES[37] DU[41] SYSTèME[41] SOLAIRE[40]
# s04 observation_planetes-f1 PercentageOfWordsProcessed= 0.838709677419355 PercentageOfWordsReallyProcessed= 0.838709677419355
