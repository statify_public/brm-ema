# 103 Fixation 1 (257.2,295.3): Aux~0 abords~1
# 191 Fixation 2 (315.8,294.2): Aux~0 abords~1
# 177 Fixation 3 (417.2,294.7): de~2 gare~4
# 282 Fixation 4 (473.7,295.5): la~3 gare~4
# 221 Fixation 5 (588.7,294.9): nulle~5
# 178 Fixation 6 (621.9,293.6): nulle~5 barre~6
# 135 Fixation 7 (676.5,298.7): barre~6
# 401 Fixation 8 (254,342.9): logements~8
# 195 Fixation 9 (395.8,350.1): ni~9 cabine~10
# 223 Fixation 10 (451.1,349.5): ni~9 cabine~10
# 224 Fixation 11 (567.7,349.4): téléphonique~11
# 127 Fixation 12 (558.7,350.5): téléphonique~11
# 225 Fixation 13 (714.5,361.8): dévastée~12
# 118 Fixation 14 (324.1,405.3): mais~13 habitat~15
# 188 Fixation 15 (275.8,395.6): mais~13 habitat~15
# 204 Fixation 16 (353,395): un~14 habitat~15
# 217 Fixation 17 (483.9,412.6): constitué~16
# 94 Fixation 18 (552.3,417.6): constitué~16
# 243 Fixation 19 (631,418.7): d'immeubles~17
# 300 Fixation 20 (281.8,465.8): réhabilités~18
# 247 Fixation 21 (256.9,461.5): réhabilités~18
# 169 Fixation 22 (300.2,460.6): réhabilités~18
# 126 Fixation 23 (352.5,463.2): réhabilités~18
%%
# Aux~0 Fixations= "2/"
# abords~1 Fixations= "2/"
# de~2 Fixations= "3/"
# la~3 Fixations= "4/"
# gare~4 Fixations= "4/"
# nulle~5 Fixations= "6/"
# barre~6 Fixations= "7/"
# de~7 Fixations= ""
# logements~8 Fixations= "8/"
# ni~9 Fixations= "10/"
# cabine~10 Fixations= "10/"
# téléphonique~11 Fixations= "12/"
# dévastée~12 Fixations= "13/"
# mais~13 Fixations= "15/"
# un~14 Fixations= "16/"
# habitat~15 Fixations= "16/"
# constitué~16 Fixations= "18/"
# d'immeubles~17 Fixations= "19/"
# réhabilités~18 Fixations= "23/"
# et~19 Fixations= ""
# de~20 Fixations= ""
# maisons~21 Fixations= ""
# en~22 Fixations= ""
# pierre~23 Fixations= ""
# meulière~24 Fixations= ""
# restaurées~25 Fixations= ""
# et~26 Fixations= ""
# ornées~27 Fixations= ""
# de~28 Fixations= ""
# charmants~29 Fixations= ""
# balcons~30 Fixations= ""
%%
#  AUX[2] ABORDS[2] DE[3] LA[4] GARE[4] NULLE[6] BARRE[7] de LOGEMENTS[8] NI[10] CABINE[10] TéLéPHONIQUE[12] DéVASTéE[13] MAIS[15] UN[16] HABITAT[16] CONSTITUé[18] D'IMMEUBLES[19] RéHABILITéS[23] et de maisons en pierre meulière restaurées et ornées de charmants balcons
# s06 rehabilitation_logement-f1 PercentageOfWordsProcessed= 0.580645161290323 PercentageOfWordsReallyProcessed= 0.947368421052632
