# 161 Fixation 1 (278.4,279): L'Irak~0
# 158 Fixation 2 (398.5,270.7): a~1 demandé~2
# 174 Fixation 3 (515.4,274.2): à~3 l'ONU~4
# 107 Fixation 4 (620.1,287.5): d'empêcher~5
# 318 Fixation 5 (301.3,341.9): survols~7
# 182 Fixation 6 (462,343.3): américains~8
# 201 Fixation 7 (537.9,344.9): et~9 britanniques~10
# 177 Fixation 8 (703.6,356.5): et~11
# 266 Fixation 9 (315.7,423): permettre~13
# 185 Fixation 10 (452,409.9): ses~15 avions~16
# 167 Fixation 11 (560.6,405.7): avions~16 civils~17
# 98 Fixation 12 (611.9,387.5): civils~17
# 216 Fixation 13 (325.5,446.4): transporter~19
# 168 Fixation 14 (466.2,461.8): des~20 scientifiques~21
# 253 Fixation 15 (642,462.6): étrangers~22
# 302 Fixation 16 (311.6,507.2): transporter~19
# 112 Fixation 17 (454.5,526.2): soumis~25 à~26
%%
# L'Irak~0 Fixations= "1/"
# a~1 Fixations= "2/"
# demandé~2 Fixations= "2/"
# à~3 Fixations= "3/"
# l'ONU~4 Fixations= "3/"
# d'empêcher~5 Fixations= "4/"
# les~6 Fixations= ""
# survols~7 Fixations= "5/"
# américains~8 Fixations= "6/"
# et~9 Fixations= "7/"
# britanniques~10 Fixations= "7/"
# et~11 Fixations= "8/"
# de~12 Fixations= ""
# permettre~13 Fixations= "9/"
# à~14 Fixations= ""
# ses~15 Fixations= "10/"
# avions~16 Fixations= "11/"
# civils~17 Fixations= "12/"
# de~18 Fixations= ""
# transporter~19 Fixations= "16/"
# des~20 Fixations= "14/"
# scientifiques~21 Fixations= "14/"
# étrangers~22 Fixations= "15/"
# L'Irak~23 Fixations= ""
# est~24 Fixations= ""
# soumis~25 Fixations= "17/"
# à~26 Fixations= "17/"
# un~27 Fixations= ""
# embargo~28 Fixations= ""
# aérien~29 Fixations= ""
# depuis~30 Fixations= ""
# 1990~31 Fixations= ""
%%
#  L'IRAK[1] A[2] DEMANDé[2] à[3] L'ONU[3] D'EMPêCHER[4] les SURVOLS[5] AMéRICAINS[6] ET[7] BRITANNIQUES[7] ET[8] de PERMETTRE[9] à SES[10] AVIONS[11] CIVILS[12] de TRANSPORTER[16] DES[14] SCIENTIFIQUES[14] éTRANGERS[15] L'Irak est SOUMIS[17] à[17] un embargo aérien depuis 1990
# s07 conflit_irak-f2 PercentageOfWordsProcessed= 0.65625 PercentageOfWordsReallyProcessed= 0.777777777777778
