# 145 Fixation 1 (371.1,283.7): un~1 accord~2
# 220 Fixation 2 (545.5,262.6): de~3 principe~4
# 246 Fixation 3 (292.3,330.7): nécessité~7
# 202 Fixation 4 (440.9,307.3): accord~2
# 198 Fixation 5 (555.2,329.8): réforme~9 comprenant~10
# 198 Fixation 6 (454.6,363.9): d'une~8 réforme~9
# 205 Fixation 7 (630.1,398.3): le~16 syndicat~17
# 164 Fixation 8 (497.4,452.6): affirmait~20
# 140 Fixation 9 (415.9,443.8): individualisée~13
# 134 Fixation 10 (608.7,469.5): qu'il~21 fallait~22
# 221 Fixation 11 (359.2,521.1): abandonner~23
# 149 Fixation 12 (292.3,518.8): abandonner~23
# 128 Fixation 13 (475.4,512.3): projet~25
%%
# Malgré~0 Fixations= ""
# un~1 Fixations= "1/"
# accord~2 Fixations= "4/"
# de~3 Fixations= "2/"
# principe~4 Fixations= "2/"
# sur~5 Fixations= ""
# la~6 Fixations= ""
# nécessité~7 Fixations= "3/"
# d'une~8 Fixations= "6/"
# réforme~9 Fixations= "6/"
# comprenant~10 Fixations= "5/"
# une~11 Fixations= ""
# aide~12 Fixations= ""
# individualisée~13 Fixations= "9/"
# aux~14 Fixations= ""
# élèves~15 Fixations= ""
# le~16 Fixations= "7/"
# syndicat~17 Fixations= "7/"
# des~18 Fixations= ""
# enseignants~19 Fixations= ""
# affirmait~20 Fixations= "8/"
# qu'il~21 Fixations= "10/"
# fallait~22 Fixations= "10/"
# abandonner~23 Fixations= "12/"
# ce~24 Fixations= ""
# projet~25 Fixations= "13/"
# et~26 Fixations= ""
# retravailler~27 Fixations= ""
# sur~28 Fixations= ""
# d'autres~29 Fixations= ""
# bases~30 Fixations= ""
%%
#  Malgré UN[1] ACCORD[4] DE[2] PRINCIPE[2] sur la NéCESSITé[3] D'UNE[6] RéFORME[6] COMPRENANT[5] une aide INDIVIDUALISéE[9] aux élèves LE[7] SYNDICAT[7] des enseignants AFFIRMAIT[8] QU'IL[10] FALLAIT[10] ABANDONNER[12] ce PROJET[13] et retravailler sur d'autres bases
# s07 reforme_enseignement-f2 PercentageOfWordsProcessed= 0.516129032258065 PercentageOfWordsReallyProcessed= 0.615384615384615
