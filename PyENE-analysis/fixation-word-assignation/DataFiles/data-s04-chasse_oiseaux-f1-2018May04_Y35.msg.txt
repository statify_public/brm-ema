# 290 Fixation 1 (371.6,296.6): le~1 Pas-de-Calais~2
# 179 Fixation 2 (460.4,290.8): Pas-de-Calais~2
# 237 Fixation 3 (594.3,284.5): les~3 chasseurs~4
# 185 Fixation 4 (332.6,336.5): réclament~5
# 100 Fixation 5 (293,339.3): réclament~5
# 186 Fixation 6 (446.6,342.9): le~6 droit~7
# 178 Fixation 7 (549.6,337.6): de~8 continuer~9
# 156 Fixation 8 (678.9,338.5): à~10
# 154 Fixation 9 (322.5,394.3): dans~12 huttes~14
# 172 Fixation 10 (394.8,396.1): les~13 huttes~14
# 157 Fixation 11 (293.8,398): dans~12 huttes~14
# 171 Fixation 12 (394.8,395.9): les~13 huttes~14
# 248 Fixation 13 (527.2,395.8): La~15 France~16
# 234 Fixation 14 (287.9,399): dans~12 huttes~14
# 141 Fixation 15 (545.1,399.1): La~15 France~16
# 167 Fixation 16 (625.6,401.8): risque~17
# 104 Fixation 17 (713.3,395.5): pourtant~18
# 175 Fixation 18 (315.9,453): une~19 lourde~20
# 190 Fixation 19 (403.1,458.6): lourde~20 amende~21
# 178 Fixation 20 (534.3,459.9): si~22 contrevient~24
# 329 Fixation 21 (609.1,463.2): elle~23 contrevient~24
# 215 Fixation 22 (660.5,456.4): contrevient~24
# 353 Fixation 23 (337.9,510.6): directives~26
# 195 Fixation 24 (290.6,510.9): directives~26
# 267 Fixation 25 (439,517.6): européennes~27
# 316 Fixation 26 (607.3,529.3): qui~28 protègent~29
# 276 Fixation 27 (644.8,530.8): protègent~29
# 198 Fixation 28 (323.7,580.4): oiseaux~31
# 131 Fixation 29 (280.9,579.4): oiseaux~31
# 251 Fixation 30 (388.8,586.7): migrateurs~32
# 154 Fixation 31 (682.4,535.6): protègent~29
# 108 Fixation 32 (700.3,530.7): protègent~29
%%
# Dans~0 Fixations= ""
# le~1 Fixations= "1/"
# Pas-de-Calais~2 Fixations= "2/"
# les~3 Fixations= "3/"
# chasseurs~4 Fixations= "3/"
# réclament~5 Fixations= "5/"
# le~6 Fixations= "6/"
# droit~7 Fixations= "6/"
# de~8 Fixations= "7/"
# continuer~9 Fixations= "7/"
# à~10 Fixations= "8/"
# chasser~11 Fixations= ""
# dans~12 Fixations= "14/"
# les~13 Fixations= "12/"
# huttes~14 Fixations= "14/"
# La~15 Fixations= "15/"
# France~16 Fixations= "15/"
# risque~17 Fixations= "16/"
# pourtant~18 Fixations= "17/"
# une~19 Fixations= "18/"
# lourde~20 Fixations= "19/"
# amende~21 Fixations= "19/"
# si~22 Fixations= "20/"
# elle~23 Fixations= "21/"
# contrevient~24 Fixations= "22/"
# aux~25 Fixations= ""
# directives~26 Fixations= "24/"
# européennes~27 Fixations= "25/"
# qui~28 Fixations= "26/"
# protègent~29 Fixations= "32/"
# les~30 Fixations= ""
# oiseaux~31 Fixations= "29/"
# migrateurs~32 Fixations= "30/"
%%
#  Dans LE[1] PAS-DE-CALAIS[2] LES[3] CHASSEURS[3] RéCLAMENT[5] LE[6] DROIT[6] DE[7] CONTINUER[7] à[8] chasser DANS[14] LES[12] HUTTES[14] LA[15] FRANCE[15] RISQUE[16] POURTANT[17] UNE[18] LOURDE[19] AMENDE[19] SI[20] ELLE[21] CONTREVIENT[22] aux DIRECTIVES[24] EUROPéENNES[25] QUI[26] PROTèGENT[32] les OISEAUX[29] MIGRATEURS[30]
# s04 chasse_oiseaux-f1 PercentageOfWordsProcessed= 0.878787878787879 PercentageOfWordsReallyProcessed= 0.878787878787879
