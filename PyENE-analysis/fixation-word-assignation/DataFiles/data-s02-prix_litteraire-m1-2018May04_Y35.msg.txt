# 122 Fixation 1 (333.3,277.9): jeu~1 favori~2
# 127 Fixation 2 (393.7,277.5): favori~2
# 94 Fixation 3 (462.7,279.4): des~3 biographes~4
# 228 Fixation 4 (529,280.2): biographes~4
# 375 Fixation 5 (677.8,279.7): de~5 Conrad~6
# 142 Fixation 6 (753.2,293.3): Conrad~6
# 154 Fixation 7 (324.5,334.4): célèbre~8
# 208 Fixation 8 (283.5,335.3): célèbre~8
# 222 Fixation 9 (385.7,334.8): écrivain~9
%%
# Le~0 Fixations= ""
# jeu~1 Fixations= "1/"
# favori~2 Fixations= "2/"
# des~3 Fixations= "3/"
# biographes~4 Fixations= "4/"
# de~5 Fixations= "5/"
# Conrad~6 Fixations= "6/"
# le~7 Fixations= ""
# célèbre~8 Fixations= "8/"
# écrivain~9 Fixations= "9/"
# anglais~10 Fixations= ""
# consiste~11 Fixations= ""
# à~12 Fixations= ""
# mesurer~13 Fixations= ""
# l'écart~14 Fixations= ""
# entre~15 Fixations= ""
# les~16 Fixations= ""
# événements~17 Fixations= ""
# de~18 Fixations= ""
# sa~19 Fixations= ""
# vie~20 Fixations= ""
# réelle~21 Fixations= ""
# et~22 Fixations= ""
# les~23 Fixations= ""
# romans~24 Fixations= ""
# qu'il~25 Fixations= ""
# en~26 Fixations= ""
# a~27 Fixations= ""
# tirés~28 Fixations= ""
%%
#  Le JEU[1] FAVORI[2] DES[3] BIOGRAPHES[4] DE[5] CONRAD[6] le CéLèBRE[8] éCRIVAIN[9] anglais consiste à mesurer l'écart entre les événements de sa vie réelle et les romans qu'il en a tirés
# s02 prix_litteraire-m1 PercentageOfWordsProcessed= 0.275862068965517 PercentageOfWordsReallyProcessed= 0.8
