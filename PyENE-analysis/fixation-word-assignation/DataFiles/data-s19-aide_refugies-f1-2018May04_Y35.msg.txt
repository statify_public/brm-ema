# 119 Fixation 1 (288.7,287.8): Lorsque~0 vague~2
# 160 Fixation 2 (418.1,271.7): la~1 vague~2
# 154 Fixation 3 (543.7,262): des~3 réfugiés~4
# 223 Fixation 4 (659.8,266.9): déferlait~5
# 123 Fixation 5 (320.3,336.1): le~7 camp~8
# 143 Fixation 6 (288.3,348.1): le~7 camp~8
# 136 Fixation 7 (384.2,348.5): improvisé~9
# 245 Fixation 8 (543.4,341.6): les~10 microbus~11
# 121 Fixation 9 (360.1,420.7): tracteurs~14 mobilisés~15
# 233 Fixation 10 (277.6,410.7): tracteurs~14
# 137 Fixation 11 (317.1,476.5): macédonienne~18
# 137 Fixation 12 (468.4,472.3): El~19 Hilal~20
# 205 Fixation 13 (566.8,471.1): assuraient~21
# 185 Fixation 14 (305.3,537.8): mini-pont~23
# 178 Fixation 15 (427.2,537.3): humanitaire~24
# 152 Fixation 16 (605.5,538.3): qui~25 permettait~26
# 151 Fixation 17 (301.8,600.1): d'éviter~27
# 155 Fixation 18 (400.1,604.3): le~28 pire~29
%%
# Lorsque~0 Fixations= "1/"
# la~1 Fixations= "2/"
# vague~2 Fixations= "2/"
# des~3 Fixations= "3/"
# réfugiés~4 Fixations= "3/"
# déferlait~5 Fixations= "4/"
# sur~6 Fixations= ""
# le~7 Fixations= "6/"
# camp~8 Fixations= "6/"
# improvisé~9 Fixations= "7/"
# les~10 Fixations= "8/"
# microbus~11 Fixations= "8/"
# et~12 Fixations= ""
# les~13 Fixations= ""
# tracteurs~14 Fixations= "10/"
# mobilisés~15 Fixations= "9/"
# par~16 Fixations= ""
# l'organisation~17 Fixations= ""
# macédonienne~18 Fixations= "11/"
# El~19 Fixations= "12/"
# Hilal~20 Fixations= "12/"
# assuraient~21 Fixations= "13/"
# un~22 Fixations= ""
# mini-pont~23 Fixations= "14/"
# humanitaire~24 Fixations= "15/"
# qui~25 Fixations= "16/"
# permettait~26 Fixations= "16/"
# d'éviter~27 Fixations= "17/"
# le~28 Fixations= "18/"
# pire~29 Fixations= "18/"
%%
#  LORSQUE[1] LA[2] VAGUE[2] DES[3] RéFUGIéS[3] DéFERLAIT[4] sur LE[6] CAMP[6] IMPROVISé[7] LES[8] MICROBUS[8] et les TRACTEURS[10] MOBILISéS[9] par l'organisation MACéDONIENNE[11] EL[12] HILAL[12] ASSURAIENT[13] un MINI-PONT[14] HUMANITAIRE[15] QUI[16] PERMETTAIT[16] D'éVITER[17] LE[18] PIRE[18]
# s19 aide_refugies-f1 PercentageOfWordsProcessed= 0.8 PercentageOfWordsReallyProcessed= 0.8
