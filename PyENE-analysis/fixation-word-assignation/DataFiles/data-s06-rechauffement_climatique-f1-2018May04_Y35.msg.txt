# 144 Fixation 1 (257,279.5): L'impact~0
# 93 Fixation 2 (305.8,278.1): L'impact~0
# 157 Fixation 3 (384.7,279.8): du~1 tourisme~2
# 106 Fixation 4 (484.9,280.1): tourisme~2
# 102 Fixation 5 (520.6,279.5): tourisme~2
# 271 Fixation 6 (621.7,286): l'évolution~4
# 111 Fixation 7 (717.4,279.6): l'évolution~4
# 201 Fixation 8 (238.7,336.7): climat~6
# 198 Fixation 9 (355.3,337.4): climat~6 pourrait~7
# 109 Fixation 10 (397.6,335.1): pourrait~7
# 139 Fixation 11 (484.8,339.9): plus~8
# 257 Fixation 12 (564.5,345.7): que~9 doubler~10
%%
# L'impact~0 Fixations= "2/"
# du~1 Fixations= "3/"
# tourisme~2 Fixations= "5/"
# sur~3 Fixations= ""
# l'évolution~4 Fixations= "7/"
# du~5 Fixations= ""
# climat~6 Fixations= "9/"
# pourrait~7 Fixations= "10/"
# plus~8 Fixations= "11/"
# que~9 Fixations= "12/"
# doubler~10 Fixations= "12/"
# dans~11 Fixations= ""
# les~12 Fixations= ""
# trente~13 Fixations= ""
# prochaines~14 Fixations= ""
# années~15 Fixations= ""
# en~16 Fixations= ""
# raison~17 Fixations= ""
# de~18 Fixations= ""
# l'effet~19 Fixations= ""
# de~20 Fixations= ""
# serre~21 Fixations= ""
# produit~22 Fixations= ""
# par~23 Fixations= ""
# les~24 Fixations= ""
# rejets~25 Fixations= ""
# massifs~26 Fixations= ""
# de~27 Fixations= ""
# carbone~28 Fixations= ""
# associés~29 Fixations= ""
# aux~30 Fixations= ""
# activités~31 Fixations= ""
# touristiques~32 Fixations= ""
%%
#  L'IMPACT[2] DU[3] TOURISME[5] sur L'éVOLUTION[7] du CLIMAT[9] POURRAIT[10] PLUS[11] QUE[12] DOUBLER[12] dans les trente prochaines années en raison de l'effet de serre produit par les rejets massifs de carbone associés aux activités touristiques
# s06 rechauffement_climatique-f1 PercentageOfWordsProcessed= 0.272727272727273 PercentageOfWordsReallyProcessed= 0.818181818181818
