# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

# %%
import pandas as pd
from ema import EyeMovementDataFrame
from ema import Model
import os

#%%
SHARE_PATH = '/home/bolivier/PyENE/PyENE-analysis/em-analysis/share/'
#MODEL_PATH = os.path.join(SHARE_PATH, 'models')
MODEL_PATH = '/home/bolivier/18-12'
DATA_PATH = os.path.join(SHARE_PATH, 'data')
PYENE_DATA_PATH = '/home/bolivier/PyENE/PyENE-data/em-analysis/data'
#%%
em_data = pd.read_excel(os.path.join(DATA_PATH, 'em-y35-fasttext.xlsx'))
em_data['READMODE_TMP'] = em_data['READMODE']
em_data.loc[em_data['READMODE'] == -2, 'READMODE_TMP'] = 0
em_data.loc[em_data['READMODE'] == -1, 'READMODE_TMP'] = 1
em_data.loc[em_data['READMODE'] == 0, 'READMODE_TMP'] = 2
em_data.loc[em_data['READMODE'] == 1, 'READMODE_TMP'] = 3
em_data.loc[em_data['READMODE'] == 2, 'READMODE_TMP'] = 4
em_data['READMODE'] = em_data['READMODE_TMP']
em_data.drop(['READMODE_TMP'], axis=1)
em_data['TEXT_TYPE_TMP'] = em_data['TEXT_TYPE']
em_data['TEXT_TYPE'] = em_data['TEXT_TYPE_2']
em_data['TEXT_TMP'] = em_data['TEXT']
em_data['COS_INST'] = em_data['COS_INST_FASTTEXT_1618']
em_data['WORD_FREQUENCY'] = em_data['WFREQ_RANK_FASTTEXT_1618']
em_data['COS_CUM'] = em_data['COS_CUM_FASTTEXT_2018']
em_df = EyeMovementDataFrame(em_data)

seq = em_df.get_input_sequence(['READMODE'])
#%%
"""
for k in [3, 5]:
    m = Model(em_df, model_type='HIDDEN_SEMI-MARKOV_CHAIN', output_process_name=['READMODE'], k=k,
          random_init=True, n_random_seq=30, n_init=50, n_iter_init=400, output_path=MODEL_PATH)
    #m.iterate_em(3000)
    #m.hsmm.save(os.path.join(MODEL_PATH, 'best-k%d-3400iter.hsmc' % k))
#%%
"""
m = Model(em_df, init_hsmc_file=os.path.join(MODEL_PATH, 'RandomInit_best_ll_model_k3.hsmc'), output_path=MODEL_PATH)
m.iterate_em(3000)
m.hsmm.save(os.path.join(MODEL_PATH, 'best_ll_model_k3-3400iter.hsmc'))

m = Model(em_df, init_hsmc_file=os.path.join(MODEL_PATH, 'RandomInit_best_bic_model_k3.hsmc'), output_path=MODEL_PATH)
m.iterate_em(3000)
m.hsmm.save(os.path.join(MODEL_PATH, 'best_bic_model_k3-3400iter.hsmc'))

m = Model(em_df, init_hsmc_file=os.path.join(MODEL_PATH, 'RandomInit_best_icl_model_k3.hsmc'), output_path=MODEL_PATH)
m.iterate_em(3000)
m.hsmm.save(os.path.join(MODEL_PATH, 'best_icl_model_k3-3400iter.hsmc'))

##
m = Model(em_df, init_hsmc_file=os.path.join(MODEL_PATH, 'RandomInit_best_ll_model_k4.hsmc'), output_path=MODEL_PATH)
m.iterate_em(3000)
m.hsmm.save(os.path.join(MODEL_PATH, 'best_ll_model_k4-3400iter.hsmc'))

m = Model(em_df, init_hsmc_file=os.path.join(MODEL_PATH, 'RandomInit_best_bic_model_k4.hsmc'), output_path=MODEL_PATH)
m.iterate_em(3000)
m.hsmm.save(os.path.join(MODEL_PATH, 'best_bic_model_k4-3400iter.hsmc'))

m = Model(em_df, init_hsmc_file=os.path.join(MODEL_PATH, 'RandomInit_best_icl_model_k4.hsmc'), output_path=MODEL_PATH)
m.iterate_em(3000)
m.hsmm.save(os.path.join(MODEL_PATH, 'best_icl_model_k4-3400iter.hsmc'))

##
m = Model(em_df, init_hsmc_file=os.path.join(MODEL_PATH, 'RandomInit_best_ll_model_k5.hsmc'), output_path=MODEL_PATH)
m.iterate_em(3000)
m.hsmm.save(os.path.join(MODEL_PATH, 'best_ll_model_k5-3400iter.hsmc'))

m = Model(em_df, init_hsmc_file=os.path.join(MODEL_PATH, 'RandomInit_best_bic_model_k5.hsmc'), output_path=MODEL_PATH)
m.iterate_em(3000)
m.hsmm.save(os.path.join(MODEL_PATH, 'best_bic_model_k5-3400iter.hsmc'))

m = Model(em_df, init_hsmc_file=os.path.join(MODEL_PATH, 'RandomInit_best_icl_model_k5.hsmc'), output_path=MODEL_PATH)
m.iterate_em(3000)
m.hsmm.save(os.path.join(MODEL_PATH, 'best_icl_model_k5-3400iter.hsmc'))


"""
m = Model(em_df, init_hsmc_file='/home/bolivier/benchmark-random-init/random-init-k-4-best-icl-at-4-niter-1000.hsmc',
          output_path=MODEL_PATH)
m.iterate_em(5)
A, B = m.empirical_transition_matrix()
import numpy as np
np.set_printoptions(precision=3, suppress=True)
print((A.T/A.sum(1)).T)
"""