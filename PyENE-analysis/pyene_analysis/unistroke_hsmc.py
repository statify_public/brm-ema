# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

import numpy as np
import matplotlib.pyplot as plt
import os, copy
from ema import EyeMovementDataFrame
from ema import Model
import pandas as pd
from hmmlearn import hmm
from matplotlib.pyplot import cm

from openalea.sequence_analysis import Estimate
from openalea.sequence_analysis import Sequences
from openalea.sequence_analysis._sequence_analysis import _MarkovianSequences


MODEL_PATH = '/home/bolivier/PyENE/PyENE-analysis/em-analysis/share/models'

def LetterPlot(X):
    plt.figure()
    plt.plot(X[:,0], X[:,1], 'bo-')
    plt.show()

pathname = "/home/bolivier/unistroke_dataset/unistroke/Unistroke/"
data = os.listdir(pathname)

# keep letter A only
dataA = [s for s in data if 'L' in s]

# Read and plot one trajectory
x_table = np.loadtxt(pathname+dataA[0])
# LetterPlot(x_table)

# data to angles
angA = []
lengths = []
for d in dataA:
    trajd = np.loadtxt(pathname + d)
    trajd = np.diff(trajd, axis=0)
    ang = np.array([np.arctan2(p[0], p[1]) for p in trajd])
    ang = ang[ang != 0]
    angA += [ang]  # .tolist()
    lengths += [len(ang)]

trajA = []
for l in angA:
    trajA.append(np.matrix([[np.cos(ang), np.sin(ang)] for ang in l]))


angA = np.concatenate(angA)
angA = angA.reshape(-1,1)
"""
df = pd.DataFrame(angA)
#df = df.reset_index(True)
df['ISLAST'] = 0
df.iloc[np.cumsum(lengths) - 1, -1] = 1
#df['ISLAST'] = df['ISLAST'].astype(str)
df = df.rename(columns={0:"angle"})
df = EyeMovementDataFrame(df)

hmcA = hmm.GaussianHMM(n_components=2, n_iter=1000, tol=1e-5, verbose=True).fit(angA, lengths)
#Y, Z = hmcA.sample(25)
Y = []
Z = []
for i in range(50):
    s = hmcA.sample(50)
    Y += [s[0].tolist()]
    Z += [s[1].tolist()]

"""


hsmm = Estimate(_MarkovianSequences(Sequences(angA)),
                'HIDDEN_SEMI-MARKOV', 'Ordinary', 2, 'Irreducible',
                OccupancyMean='Estimated', NbIteration=100,
                Estimator='CompleteLikelihood', StateSequences='Viterbi',
                Counting=False)

df2 = pd.DataFrame(Y)
df2['ISLAST'] = 0
df2.iloc[range(24, 250, 25), -1] = 1
#df['ISLAST'] = df['ISLAST'].astype(str)
df2 = df2.rename(columns={0:"x_1", 1:"x_2"})
df2 = EyeMovementDataFrame(df2)

m = Model(df2, k=2, output_path=MODEL_PATH, output_process_name=['x_1', 'x_2'])

hmcA = hmm.GaussianHMM(n_components=2, n_iter=1000, tol=1e-5, verbose=True).fit(angA, lengths)



def ViterbiLetterPlot(i, trajA, lengths, Z):
    ns = max(Z) # number of states
    color = iter(cm.rainbow(np.linspace(0,1,ns+2)))
    color = list(color)
    color = ['b','g','r','c','y','k']
    traj = np.array([np.cumsum(trajA[i][:,0]), np.cumsum(trajA[i][:,1])]).T
    cuml = np.array([0] + list(np.cumsum(lengths)))
    ztraj = Z[cuml[i]:cuml[i+1]]
    plt.figure()
    plt.subplot(211)
    for s in range(ns+1):
        c = color[s+1]
        plt.plot(trajA[i][ztraj==s,0], angA[i][ztraj==s,1], 'o-',c=c)
    plt.subplot(212)
    for s in range(ns+1):
        c = color[s+1]
        plt.plot(traj[ztraj==s,0], traj[ztraj==s,1], 'o-', c=c)
    plt.show()


Z_VM = hmcA.predict(angA)


ViterbiLetterPlot(0, trajA, lengths, Z_VM)




