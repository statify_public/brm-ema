# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

import pandas as pd
from ema import EyeMovementDataFrame
from ema import Model
import os

DATA_PATH = '/home/bolivier/PyENE/PyENE-analysis/em-analysis/share/data'
MODEL_PATH = '/home/bolivier/PyENE/PyENE-analysis/em-analysis/share/models'
POST_PROC_PATH = os.path.join(MODEL_PATH, "postproc")

em_data = pd.read_excel(os.path.join(DATA_PATH,'em-y35-fasttext.xlsx'))
em_data  = redefine_readmode(em_data , type=0)

# right-censored sojourn time model with knowledge injection
em_df = EyeMovementDataFrame(em_data)
our5_path = os.path.join(MODEL_PATH, 'model3_init5abs_rev_v2.hsmc')
mour5 = Model(em_df, init_hsmc_file=our5_path, output_path=MODEL_PATH)
mour5.iterate_em(3500)
mour5.hsmm.save(os.path.join(MODEL_PATH, 'model_knowledge_censored_st.hsmc'))

# non-censored sojourn time model with knowledge injection
# adding readmode factor
new_data = pd.DataFrame(columns=em_data.columns)
new_data_list = []
for index, row in em_data.iterrows():
    if row['ISLAST'] == 1:
        last_row = row.copy()
        fake_last_row = row.copy()
        last_row['ISLAST'] = 0
        new_data_list.append(last_row)
        fake_last_row['READMODE'] = 5
        new_data_list.append(fake_last_row)
    else:
        copy_row = row.copy()
        new_data_list.append(copy_row)
new_data = new_data.append(new_data_list, ignore_index=True)

em_df = EyeMovementDataFrame(new_data)
our5_path = os.path.join(MODEL_PATH, 'model_init_knowledge_non_censored_st.hsmc')
mour5 = Model(em_df, init_hsmc_file=our5_path, output_path=MODEL_PATH)
mour5.iterate_em(10000)
mour5.hsmm.save(os.path.join(MODEL_PATH, 'model_knowledge_non_censored_st.hsmc'))

rdf = mour5.eye_movement_data.restored_data.copy()
rows_to_drop = []
for index, row in rdf.iterrows():
    if row['ISLAST'] == 1:
        rows_to_drop.append(index)
        rdf.loc[index-1, 'ISLAST'] = 1
rdf = rdf.drop(rows_to_drop)

rdf.to_csv(os.path.join(postproc, 'rdf-mki.csv'))

import numpy as np
rseqs = mour5.hsmm.extract_data()
np.set_printoptions(precision=2, suppress=True)

empirical_pi = np.zeros(6)
for seq in rseqs:
    empirical_pi[seq[0][0]] +=1

empirical_A = np.zeros((6,6))
for seq in rseqs:
    last_state = -1
    current_state = -1
    for elem in seq:
        current_state = elem[0]
        if (last_state != -1) & (current_state != last_state):
            empirical_A[last_state, current_state] +=1
        last_state = current_state
empirical_A

empirical_B = np.zeros((5,5))
for seq in rseqs:
    for elem in seq:
        empirical_B[elem[0], elem[1]] +=1
empirical_B

# redefining readmode
def redefine_readmode(dataframe, type=0):
    df = dataframe.copy()
    if type == 0:
        df.at[df.WINC > 1, 'READMODE'] = 4
        df.at[df.WINC == 1, 'READMODE'] = 3
        df.at[df.WINC == 0, 'READMODE'] = 2
        df.at[df.WINC == -1, 'READMODE'] = 1
        df.at[df.WINC < -1, 'READMODE'] = 0
    if type == 1:
        df['READMODE'] = 5
        df.at[df.WINC == 2, 'READMODE'] = 4
        df.at[df.WINC == 1, 'READMODE'] = 3
        df.at[df.WINC == 0, 'READMODE'] = 2
        df.at[df.WINC == -1, 'READMODE'] = 1
        df.at[df.WINC < -1, 'READMODE'] = 0
    elif type == 2:
        df['READMODE'] = 6
        df.at[df.WINC == 2, 'READMODE'] = 5
        df.at[df.WINC == 1, 'READMODE'] = 4
        df.at[df.WINC == 0, 'READMODE'] = 3
        df.at[df.WINC == -1, 'READMODE'] = 2
        df.at[df.WINC == -2, 'READMODE'] = 1
        df.at[df.WINC < -2, 'READMODE'] = 0
    elif type == 3:
        df['READMODE'] = 2
        df.at[df.WINC > 2, 'READMODE'] = 4
        df.at[df.WINC == 2, 'READMODE'] = 3
        df.at[df.WINC == -1, 'READMODE'] = 1
        df.at[df.WINC < -1, 'READMODE'] = 0
    df['READMODE'] = pd.Categorical(df.READMODE)
    return df

for readmode_type in range(1, 4):
    em_data = pd.read_excel(os.path.join(DATA_PATH,'em-y35-fasttext.xlsx'))
    em_data = redefine_readmode(em_data, type=readmode_type)
    em_df = EyeMovementDataFrame(em_data)
    init_file = os.path.join(MODEL_PATH, 'model_init_knowledge_readmode_' + str(readmode_type) + '.hsmc')
    model = Model(em_df, init_hsmc_file=init_file, output_path=MODEL_PATH)
    model.iterate_em(3500)
    model.hsmm.save(os.path.join(MODEL_PATH, 'model_knowledge_readmode_' + str(readmode_type) + '.hsmc'))


