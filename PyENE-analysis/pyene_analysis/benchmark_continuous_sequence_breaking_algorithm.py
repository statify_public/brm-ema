# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

import pandas as pd
from ema import EyeMovementDataFrame
from ema import Model
import numpy as np
from openalea.sequence_analysis._sequence_analysis import _MarkovianSequences
from openalea.sequence_analysis import Estimate
from openalea.sequence_analysis import HiddenSemiMarkov
from openalea.sequence_analysis import Sequences
import os
import random


DATA_PATH = '/home/bolivier/PyENE/PyENE-analysis/em-analysis/share/data'
MODEL_PATH = '/home/bolivier/PyENE/PyENE-analysis/em-analysis/share/models'
POST_PROC_PATH = os.path.join(MODEL_PATH, "postproc")

K=3
nb_iter = 100

synthetic_seqs = [[[float(elem), float(elem)+1] for elem in np.random.normal(0, 10, 5)],
                  [[float(elem), float(elem)+1] for elem in np.random.normal(0, 10, 5)]]

estimated_model_standard_init = Estimate(_MarkovianSequences(Sequences(synthetic_seqs)),
                                         'HIDDEN_SEMI-MARKOV', 'Ordinary', K, 'Irreducible',
                                         OccupancyMean='Estimated', NbIteration=nb_iter,
                                         Estimator='CompleteLikelihood', StateSequences='Viterbi', Counting=False)

estimated_model_standard_init.save(os.path.join(MODEL_PATH, 'continuous_estimated_model_standard_init.hsmc'))
print estimated_model_standard_init.get_likelihood()


from sklearn.cluster import KMeans
kmeans_synthetic_seq = [point for seq in synthetic_seqs for point in seq]
kmeans = KMeans(n_clusters=K, random_state=None).fit(kmeans_synthetic_seq)
kmeans.cluster_centers_
kmean.clus