# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from ema import EyeMovementDataFrame
from ema.html_report import Htmlreport
from ema import Model
import os
import random

random.seed(1)

DATA_PATH = '/home/bolivier/PyENE/PyENE-analysis/em-analysis/share/data'
MODEL_PATH = '/home/bolivier/PyENE/PyENE-analysis/em-analysis/share/models'
post_proc = os.path.join(MODEL_PATH, "postproc")

rdf = pd.read_csv(os.path.join(post_proc, 'rdf-mki.csv'), encoding='utf-8')

# contrasting texts

rdf.groupby('TEXT_TYPE_2').ISLAST.sum()

rdf.groupby('TEXT_TYPE')['NEW_READ_WORDS'].sum().reset_index()['NEW_READ_WORDS'] /\
(rdf.groupby('TEXT_TYPE').sum().reset_index()['FDUR'] / 1000 / 60)

mean_fix_per_text = rdf.groupby('TEXT_TYPE_2')['PHASE'].count() / rdf.groupby('TEXT_TYPE_2').ISLAST.sum()

rdf_gb = rdf.groupby(['SUBJ_NAME', 'TEXT', 'TEXT_TYPE_2'])['PHASE'].count().reset_index()
var = {'a':[], 'f':[], 'f+':[], 'm':[]}
for index, row in rdf_gb.iterrows():
    var[row.TEXT_TYPE_2].append((row.PHASE - mean_fix_per_text[row.TEXT_TYPE_2])**2)
np.sqrt([np.mean(value) for _, value in var.iteritems()])




# contrasting reading strategies

rdf.groupby('PHASE')['FDUR'].mean()
rdf.groupby('PHASE')['FDUR'].std()

rdf.groupby('PHASE')['SACAMP'].mean()
rdf.groupby('PHASE')['SACAMP'].std()

rdf.groupby('PHASE')['NEW_READ_WORDS'].sum().reset_index()['NEW_READ_WORDS'] /\
(rdf.groupby('PHASE').sum().reset_index()['FDUR'] / 1000 / 60)

rdf.groupby('PHASE')['WINC'].mean()
rdf.groupby('PHASE')['WINC'].std()

rdf.groupby('PHASE')['CINC'].mean()
rdf.groupby('PHASE')['CINC'].std()

rdf['WINC_ABS'] = rdf.WINC.abs()
rdf['CINC_ABS'] = rdf.CINC.abs()
rdf['CINC_WINC_RATIO'] = rdf['CINC_ABS'] / rdf['WINC_ABS']
rdf['CINC_WINC_RATIO'] = rdf['CINC_WINC_RATIO'].fillna(0)
rdf['CINC_WINC_RATIO'] = rdf['CINC_WINC_RATIO'] * (5.34 / rdf['CINC_WINC_RATIO'].mean()) # to get 5.34 C/W
rdf.groupby('PHASE')['CINC_WINC_RATIO'].mean()
rdf.groupby('PHASE')['CINC_WINC_RATIO'].std()

sacdir = rdf.groupby(['PHASE', 'SACDIR'])['SACDIR'].size()
freq = []
for i in xrange(0, 5):
    freq.append(sacdir[i] / sacdir[i].sum())

rdf.groupby(['PHASE', 'TEXT_TYPE_2'])['COS_CUM'].mean()


#%%
rdf[['STATES']].apply(pd.value_counts) / rdf.shape[0]
#%%
rdf.groupby('TEXT_TYPE_2')['STATES'].value_counts() / rdf.groupby('TEXT_TYPE_2')['STATES'].count()
#%%
rdf.groupby('TEXT_TYPE_2')['STATES'].value_counts()
#%%
rdf[['TEXT_TYPE_2', 'ISFIRST']].groupby('TEXT_TYPE_2').count() / rdf[['TEXT_TYPE_2', 'ISFIRST']].groupby('TEXT_TYPE_2').sum()

#%%
rdf_type_phase = rdf.groupby('TEXT_TYPE_2')['PHASE'].value_counts().reset_index(False)
rdf.groupby('TEXT_TYPE_2')['PHASE'].value_counts() / rdf.groupby('TEXT_TYPE_2')['PHASE'].count()
rdf_type_phase.rename(columns={'PHASE': 'COUNT'}, inplace=True)
rdf_type_phase.to_csv(os.path.join(post_proc, 'rdf_type_phase.csv'))

# \# transition per text
tpt = {'a':[], 'f':[], 'f+':[], 'm':[]}
for index, row in rdf.iterrows():
    current_state = row.PHASE
    if row.ISFIRST == 1:
        nbt = 0.
        previous_state = current_state
    if previous_state != current_state:
        nbt += 1.
    previous_state = current_state
    if row.ISLAST == 1:
        tpt[row.TEXT_TYPE_2].append(nbt)
[np.mean(value) for _, value in tpt.iteritems()]

#%% DF for FCA

subj_phase_table = rdf.groupby(['SUBJ_NAME', 'PHASE']).count()
subj_phase_table = subj_phase_table.reset_index()
s13_p3 = subj_phase_table.reset_index().loc[0].copy()
s13_p3['SUBJ_NAME'] = 's13'
s13_p3['PHASE'] = 3
for col in s13_p3.keys():
    if col not in ['SUBJ_NAME', 'PHASE']:
        s13_p3[col] = 0
subj_phase_table = pd.concat((subj_phase_table.loc[:34], pd.DataFrame(s13_p3).T, subj_phase_table.loc[35:])).reset_index()
subj_phase_table.to_csv(os.path.join(post_proc, 'subj_phase_tab.csv'), index=None, encoding='utf-8')
rdf.groupby(['SUBJ_NAME', 'PHASE']).size().reset_index().to_csv(os.path.join(post_proc, 'subj_phase_tab2.csv'), index=None, encoding='utf-8')

#%%
sojourn_per_text = pd.DataFrame(columns=['Text', 'State', 'Duration'])
for i in rdf.index:
    if rdf.at[i, 'ISFIRST']:
        sojourn_time = 0
    elif (rdf.at[i, 'ISLAST']) | (rdf.at[i, 'PHASE'] != previous_state):
        if rdf.at[i, 'ISLAST']:
            sojourn_time += 1
        # sojourn_per_text[rdf.at[i, 'PHASE']].append(sojourn_time)
        sojourn_per_text = sojourn_per_text.append({'Text': rdf.at[i, 'TEXT_TYPE_2'],
                                                    'State': rdf.at[i, 'PHASE'],
                                                    'Duration': sojourn_time}, ignore_index=True)

        sojourn_time = 0
    sojourn_time += 1
    previous_state = rdf.at[i, 'PHASE']
sojourn_per_text.to_csv(os.path.join(post_proc, 'sojourn_per_text.csv'), index=None, encoding='utf-8')


#%% F
rdfFp = rdf.loc[rdf.TEXT_TYPE_2 == 'f+']
rdfFp = rdfFp.reset_index()
transition_target_distance_Fp = np.zeros((50, 4))
scanpath_transition = []
scanpath_id = 0
for i in rdfFp.index:
    if rdfFp.at[i, 'ISFIRST']:
        j = i
        cos_inst_max_1 = 0
        cos_inst_max_2 = 0
        jwmax_1 = np.infty
        jwmax_2 = np.infty
        while not rdfFp.at[j, 'ISLAST']:
            if rdfFp.at[j, 'COS_INST'] > cos_inst_max_1:
                cos_inst_max_2 = cos_inst_max_1
                jwmax_2 = jwmax_1
                cos_inst_max_1 = rdfFp.at[j, 'COS_INST']
                jwmax_1 = j
            elif rdfFp.at[j, 'COS_INST'] > cos_inst_max_2:
                cos_inst_max_2 = rdfFp.at[j, 'COS_INST']
                jwmax_2 = j
            j += 1
    elif rdfFp.at[i, 'PHASE'] != previous_state:
        if cos_inst_max_1 > 0.3:
            if cos_inst_max_2 > 0.3:
                dist = min(abs(jwmax_1 - i), abs(jwmax_2 - i))
            else:
                dist = abs(jwmax_1 - i)
            transition_target_distance_Fp[dist, rdfFp.at[i, 'PHASE']] += 1
    previous_state = rdfFp.at[i, 'PHASE']
freq_transition_target_distance_Fp = transition_target_distance_Fp / transition_target_distance_Fp.sum(axis=0)


rdfF = rdf.loc[rdf.TEXT_TYPE_2 == 'f']
rdfF = rdfF.reset_index()
transition_target_distance_F = np.zeros((50, 4))
scanpath_transition = []
scanpath_id = 0
for i in rdfF.index:
    if rdfF.at[i, 'ISFIRST']:
        j = i
        cos_inst_max_1 = 0
        cos_inst_max_2 = 0
        jwmax_1 = np.infty
        jwmax_2 = np.infty
        while not rdfF.at[j, 'ISLAST']:
            if rdfF.at[j, 'COS_INST'] > cos_inst_max_1:
                cos_inst_max_2 = cos_inst_max_1
                jwmax_2 = jwmax_1
                cos_inst_max_1 = rdfF.at[j, 'COS_INST']
                jwmax_1 = j
            elif rdfF.at[j, 'COS_INST'] > cos_inst_max_2:
                cos_inst_max_2 = rdfF.at[j, 'COS_INST']
                jwmax_2 = j
            j += 1
    elif rdfF.at[i, 'PHASE'] != previous_state:
        if cos_inst_max_1 > 0.3:
            if cos_inst_max_2 > 0.3:
                dist = min(abs(jwmax_1 - i), abs(jwmax_2 - i))
            else:
                dist = abs(jwmax_1 - i)
            transition_target_distance_F[dist, rdfF.at[i, 'PHASE']] += 1
    previous_state = rdfF.at[i, 'PHASE']
freq_transition_target_distance_F = transition_target_distance_F / transition_target_distance_F.sum(axis=0)
print(transition_target_distance_F)
print(transition_target_distance_F / transition_target_distance_F.sum(axis=0))
print(transition_target_distance_F.sum(axis=0))

rdfA = rdf.loc[rdf.TEXT_TYPE_2 == 'a']
rdfA = rdfA.reset_index()
transition_target_distance_A = np.zeros((50, 4))
scanpath_transition = []
scanpath_id = 0
for i in rdfA.index:
    if rdfA.at[i, 'ISFIRST']:
        j = i
        cos_inst_min_1 = 1
        cos_inst_min_2 = 1
        jwmin_1 = np.infty
        jwmin_2 = np.infty
        while not rdfA.at[j, 'ISLAST']:
            if rdfA.at[j, 'COS_INST'] < cos_inst_min_1:
                cos_inst_min_2 = cos_inst_min_1
                jwmin_2 = jwmin_1
                cos_inst_min_1 = rdfA.at[j, 'COS_INST']
                jwmin_1 = j
            elif rdfA.at[j, 'COS_INST'] > cos_inst_min_2:
                cos_inst_min_2 = rdfA.at[j, 'COS_INST']
                jwmin_2 = j
            j += 1
    elif rdfA.at[i, 'PHASE'] != previous_state:
        if jwmin_1 != np.infty:
            dist = min(abs(jwmin_1 - i), abs(jwmin_2 - i))
            transition_target_distance_A[dist, rdfA.at[i, 'PHASE']] += 1
    previous_state = rdfA.at[i, 'PHASE']
freq_transition_target_distance_A = transition_target_distance_A / transition_target_distance_A.sum(axis=0)

rdfM = rdf.loc[rdf.TEXT_TYPE_2 == 'm']
rdfM = rdfM.reset_index()
transition_target_distance_M = np.zeros((50, 4))
scanpath_transition = []
scanpath_id = 0
for i in rdfM.index:
    if rdfM.at[i, 'ISFIRST']:
        j = i
        cos_inst_min = 1
        cos_inst_max = 1
        jwmin = np.infty
        jwmax = np.infty
        while not rdfM.at[j, 'ISLAST']:
            if rdfM.at[j, 'COS_INST'] < cos_inst_min:
                cos_inst_min = rdfM.at[j, 'COS_INST']
                jwmin = j
            if rdfM.at[j, 'COS_INST'] > cos_inst_max:
                cos_inst_max = rdfM.at[j, 'COS_INST']
                jwmax = j
            j += 1
    elif rdfM.at[i, 'PHASE'] != previous_state:
        dist = min(abs(jwmin - i), abs(jwmax - i))
        if dist != np.infty:
            transition_target_distance_M[dist, rdfM.at[i, 'PHASE']] += 1
    previous_state = rdfM.at[i, 'PHASE']
freq_transition_target_distance_M = transition_target_distance_M / transition_target_distance_M.sum(axis=0)

print('Number of phase transitions in text F+: ', transition_target_distance_Fp.sum())
print('Number of phase transitions in text F: ', transition_target_distance_F.sum())
print('Number of phase transitions in text A: ', transition_target_distance_A.sum())
print('Number of phase transitions in text M: ', transition_target_distance_M.sum())

dist_fix_max_cos = pd.DataFrame(index=range(0, 800), columns=['Text', 'ToState', 'Dist', 'Freq'])
dist_fix_max_cos.loc[0:199, 'Text'] = 'F'
dist_fix_max_cos.loc[200:399, 'Text'] = 'A'
dist_fix_max_cos.loc[400:599, 'Text'] = 'M'
dist_fix_max_cos.loc[600:799, 'Text'] = 'F+'

dist_fix_max_cos.loc[0:49, 'ToState'] = 0
dist_fix_max_cos.loc[50:99, 'ToState'] = 1
dist_fix_max_cos.loc[100:149, 'ToState'] = 2
dist_fix_max_cos.loc[150:199, 'ToState'] = 3

dist_fix_max_cos.loc[200:249, 'ToState'] = 0
dist_fix_max_cos.loc[250:299, 'ToState'] = 1
dist_fix_max_cos.loc[300:349, 'ToState'] = 2
dist_fix_max_cos.loc[350:399, 'ToState'] = 3

dist_fix_max_cos.loc[400:449, 'ToState'] = 0
dist_fix_max_cos.loc[450:499, 'ToState'] = 1
dist_fix_max_cos.loc[500:549, 'ToState'] = 2
dist_fix_max_cos.loc[550:599, 'ToState'] = 3

dist_fix_max_cos.loc[600:649, 'ToState'] = 0
dist_fix_max_cos.loc[650:699, 'ToState'] = 1
dist_fix_max_cos.loc[700:749, 'ToState'] = 2
dist_fix_max_cos.loc[750:799, 'ToState'] = 3

dist_fix_max_cos['Dist'] = range(0,50)*16

dist_fix_max_cos.loc[0:49, 'Freq'] = freq_transition_target_distance_F[:, 0]
dist_fix_max_cos.loc[50:99, 'Freq'] = freq_transition_target_distance_F[:, 1]
dist_fix_max_cos.loc[100:149, 'Freq'] = freq_transition_target_distance_F[:, 2]
dist_fix_max_cos.loc[150:199, 'Freq'] = freq_transition_target_distance_F[:, 3]

dist_fix_max_cos.loc[200:249, 'Freq'] = freq_transition_target_distance_A[:, 0]
dist_fix_max_cos.loc[250:299, 'Freq'] = freq_transition_target_distance_A[:, 1]
dist_fix_max_cos.loc[300:349, 'Freq'] = freq_transition_target_distance_A[:, 2]
dist_fix_max_cos.loc[350:399, 'Freq'] = freq_transition_target_distance_A[:, 3]

dist_fix_max_cos.loc[400:449, 'Freq'] = freq_transition_target_distance_M[:, 0]
dist_fix_max_cos.loc[450:499, 'Freq'] = freq_transition_target_distance_M[:, 1]
dist_fix_max_cos.loc[500:549, 'Freq'] = freq_transition_target_distance_M[:, 2]
dist_fix_max_cos.loc[550:599, 'Freq'] = freq_transition_target_distance_M[:, 3]

dist_fix_max_cos.loc[600:649, 'Freq'] = freq_transition_target_distance_Fp[:, 0]
dist_fix_max_cos.loc[650:699, 'Freq'] = freq_transition_target_distance_Fp[:, 1]
dist_fix_max_cos.loc[700:749, 'Freq'] = freq_transition_target_distance_Fp[:, 2]
dist_fix_max_cos.loc[750:799, 'Freq'] = freq_transition_target_distance_Fp[:, 3]

dist_fix_max_cos.to_csv(os.path.join(post_proc, 'to_state_dist_fix_max_cos.csv'), index=None, encoding='utf-8')

#%%
rdfFp = rdf.loc[rdf.TEXT_TYPE_2 == 'f+']
rdfFp = rdfFp.reset_index()
transition_target_distance_Fp = np.zeros((50, 4))
scanpath_transition = []
scanpath_id = 0
for i in rdfFp.index:
    if rdfFp.at[i, 'ISFIRST']:
        j = i
        cos_inst_max_1 = 0
        cos_inst_max_2 = 0
        jwmax_1 = np.infty
        jwmax_2 = np.infty
        while not rdfFp.at[j, 'ISLAST']:
            if rdfFp.at[j, 'COS_INST'] > cos_inst_max_1:
                cos_inst_max_2 = cos_inst_max_1
                jwmax_2 = jwmax_1
                cos_inst_max_1 = rdfFp.at[j, 'COS_INST']
                jwmax_1 = j
            elif rdfFp.at[j, 'COS_INST'] > cos_inst_max_2:
                cos_inst_max_2 = rdfFp.at[j, 'COS_INST']
                jwmax_2 = j
            j += 1
    elif rdfFp.at[i, 'PHASE'] != previous_state:
        if cos_inst_max_1 > 0.3:
            if cos_inst_max_2 > 0.3:
                dist = min(abs(jwmax_1 - i), abs(jwmax_2 - i))
            else:
                dist = abs(jwmax_1 - i)
            transition_target_distance_Fp[dist, previous_state] += 1
    previous_state = rdfFp.at[i, 'PHASE']
freq_transition_target_distance_Fp = transition_target_distance_Fp / transition_target_distance_Fp.sum(axis=0)


rdfF = rdf.loc[rdf.TEXT_TYPE_2 == 'f']
rdfF = rdfF.reset_index()
transition_target_distance_F = np.zeros((50, 4))
scanpath_transition = []
scanpath_id = 0
for i in rdfF.index:
    if rdfF.at[i, 'ISFIRST']:
        j = i
        cos_inst_max_1 = 0
        cos_inst_max_2 = 0
        jwmax_1 = np.infty
        jwmax_2 = np.infty
        while not rdfF.at[j, 'ISLAST']:
            if rdfF.at[j, 'COS_INST'] > cos_inst_max_1:
                cos_inst_max_2 = cos_inst_max_1
                jwmax_2 = jwmax_1
                cos_inst_max_1 = rdfF.at[j, 'COS_INST']
                jwmax_1 = j
            elif rdfF.at[j, 'COS_INST'] > cos_inst_max_2:
                cos_inst_max_2 = rdfF.at[j, 'COS_INST']
                jwmax_2 = j
            j += 1
    elif rdfF.at[i, 'PHASE'] != previous_state:
        if cos_inst_max_1 > 0.3:
            if cos_inst_max_2 > 0.3:
                dist = min(abs(jwmax_1 - i), abs(jwmax_2 - i))
            else:
                dist = abs(jwmax_1 - i)
            transition_target_distance_F[dist, previous_state] += 1
    previous_state = rdfF.at[i, 'PHASE']
freq_transition_target_distance_F = transition_target_distance_F / transition_target_distance_F.sum(axis=0)

rdfA = rdf.loc[rdf.TEXT_TYPE_2 == 'a']
rdfA = rdfA.reset_index()
transition_target_distance_A = np.zeros((50, 4))
scanpath_transition = []
scanpath_id = 0
for i in rdfA.index:
    if rdfA.at[i, 'ISFIRST']:
        j = i
        cos_inst_min_1 = 1
        cos_inst_min_2 = 1
        jwmin_1 = np.infty
        jwmin_2 = np.infty
        while not rdfA.at[j, 'ISLAST']:
            if rdfA.at[j, 'COS_INST'] < cos_inst_min_1:
                cos_inst_min_2 = cos_inst_min_1
                jwmin_2 = jwmin_1
                cos_inst_min_1 = rdfA.at[j, 'COS_INST']
                jwmin_1 = j
            elif rdfA.at[j, 'COS_INST'] > cos_inst_min_2:
                cos_inst_min_2 = rdfA.at[j, 'COS_INST']
                jwmin_2 = j
            j += 1
    elif rdfA.at[i, 'PHASE'] != previous_state:
        if jwmin_1 != np.infty:
            dist = min(abs(jwmin_1 - i), abs(jwmin_2 - i))
            transition_target_distance_A[dist, previous_state] += 1
    previous_state = rdfA.at[i, 'PHASE']
freq_transition_target_distance_A = transition_target_distance_A / transition_target_distance_A.sum(axis=0)


rdfM = rdf.loc[rdf.TEXT_TYPE_2 == 'm']
rdfM = rdfM.reset_index()
transition_target_distance_M = np.zeros((50, 4))
scanpath_transition = []
scanpath_id = 0
for i in rdfM.index:
    if rdfM.at[i, 'ISFIRST']:
        j = i
        cos_inst_min = 1
        cos_inst_max = 1
        jwmin = np.infty
        jwmax = np.infty
        while not rdfM.at[j, 'ISLAST']:
            if rdfM.at[j, 'COS_INST'] < cos_inst_min:
                cos_inst_min = rdfM.at[j, 'COS_INST']
                jwmin = j
            if rdfM.at[j, 'COS_INST'] > cos_inst_max:
                cos_inst_max = rdfM.at[j, 'COS_INST']
                jwmax = j
            j += 1
    elif rdfM.at[i, 'PHASE'] != previous_state:
        dist = min(abs(jwmin - i), abs(jwmax - i))
        if dist != np.infty:
            transition_target_distance_M[dist, previous_state] += 1
    previous_state = rdfM.at[i, 'PHASE']
freq_transition_target_distance_M = transition_target_distance_M / transition_target_distance_M.sum(axis=0)

dist_fix_max_cos = pd.DataFrame(index=range(0, 800), columns=['Text', 'FromState', 'Dist', 'Freq'])
dist_fix_max_cos.loc[0:199, 'Text'] = 'F'
dist_fix_max_cos.loc[200:399, 'Text'] = 'A'
dist_fix_max_cos.loc[400:599, 'Text'] = 'M'
dist_fix_max_cos.loc[600:799, 'Text'] = 'F+'

dist_fix_max_cos.loc[0:49, 'FromState'] = 0
dist_fix_max_cos.loc[50:99, 'FromState'] = 1
dist_fix_max_cos.loc[100:149, 'FromState'] = 2
dist_fix_max_cos.loc[150:199, 'FromState'] = 3

dist_fix_max_cos.loc[200:249, 'FromState'] = 0
dist_fix_max_cos.loc[250:299, 'FromState'] = 1
dist_fix_max_cos.loc[300:349, 'FromState'] = 2
dist_fix_max_cos.loc[350:399, 'FromState'] = 3

dist_fix_max_cos.loc[400:449, 'FromState'] = 0
dist_fix_max_cos.loc[450:499, 'FromState'] = 1
dist_fix_max_cos.loc[500:549, 'FromState'] = 2
dist_fix_max_cos.loc[550:599, 'FromState'] = 3

dist_fix_max_cos.loc[600:649, 'FromState'] = 0
dist_fix_max_cos.loc[650:699, 'FromState'] = 1
dist_fix_max_cos.loc[700:749, 'FromState'] = 2
dist_fix_max_cos.loc[750:799, 'FromState'] = 3

dist_fix_max_cos['Dist'] = range(0,50)*16

dist_fix_max_cos.loc[0:49, 'Freq'] = freq_transition_target_distance_F[:, 0]
dist_fix_max_cos.loc[50:99, 'Freq'] = freq_transition_target_distance_F[:, 1]
dist_fix_max_cos.loc[100:149, 'Freq'] = freq_transition_target_distance_F[:, 2]
dist_fix_max_cos.loc[150:199, 'Freq'] = freq_transition_target_distance_F[:, 3]

dist_fix_max_cos.loc[200:249, 'Freq'] = freq_transition_target_distance_A[:, 0]
dist_fix_max_cos.loc[250:299, 'Freq'] = freq_transition_target_distance_A[:, 1]
dist_fix_max_cos.loc[300:349, 'Freq'] = freq_transition_target_distance_A[:, 2]
dist_fix_max_cos.loc[350:399, 'Freq'] = freq_transition_target_distance_A[:, 3]

dist_fix_max_cos.loc[400:449, 'Freq'] = freq_transition_target_distance_M[:, 0]
dist_fix_max_cos.loc[450:499, 'Freq'] = freq_transition_target_distance_M[:, 1]
dist_fix_max_cos.loc[500:549, 'Freq'] = freq_transition_target_distance_M[:, 2]
dist_fix_max_cos.loc[550:599, 'Freq'] = freq_transition_target_distance_M[:, 3]

dist_fix_max_cos.loc[600:649, 'Freq'] = freq_transition_target_distance_Fp[:, 0]
dist_fix_max_cos.loc[650:699, 'Freq'] = freq_transition_target_distance_Fp[:, 1]
dist_fix_max_cos.loc[700:749, 'Freq'] = freq_transition_target_distance_Fp[:, 2]
dist_fix_max_cos.loc[750:799, 'Freq'] = freq_transition_target_distance_Fp[:, 3]

dist_fix_max_cos.to_csv(os.path.join(post_proc, 'from_state_dist_fix_max_cos.csv'), index=None, encoding='utf-8')



#%%
rdfFp = rdf.loc[rdf.TEXT_TYPE_2 == 'f+']
rdfFp = rdfFp.reset_index()
transition_target_distance_Fp = np.zeros((50, 4))
transition_target_distance_Fp_neg = np.zeros((50, 4))
for i in rdfFp.index:
    if rdfFp.at[i, 'ISFIRST']:
        subject_name = rdfFp.at[i, 'SUBJ_NAME']
        text_name = rdfFp.at[i, 'TEXT']
        trial = rdfFp[(rdfFp['TEXT'] == text_name) & (rdfFp['SUBJ_NAME'] == subject_name)]
        trigger_fixations = trial.nlargest(2, ['COS_INST'])
        cos_inst_max_1 = trigger_fixations['COS_INST'].values[0]
        cos_inst_max_2 = trigger_fixations['COS_INST'].values[1]
        jwmax_1 = trigger_fixations['COS_INST'].index[0]
        jwmax_2 = trigger_fixations['COS_INST'].index[1]
        trigger_word_1 = trigger_fixations['FIXED_WORD'].values[0]
        trigger_word_2 = trigger_fixations['FIXED_WORD'].values[1]
        #print('%s, %s - trigger_words : %s %.2f - %s %.2f' % (subject_name, text_name, trigger_word_1, cos_inst_max_1,
        #                                                      trigger_word_2, cos_inst_max_2))
    elif rdfFp.at[i, 'PHASE'] != previous_state:
        if cos_inst_max_1 > 0.3:
            if cos_inst_max_2 > 0.3:
                dist1 = abs(jwmax_1 - i)
                dist2 = abs(jwmax_2 - i)
                argdist = np.argmin([dist1, dist2])
                dist = jwmax_1 - i if argdist == 0 else jwmax_2 - i
            else:
                dist = jwmax_1 - i
            if dist >=0:
                transition_target_distance_Fp[dist, previous_state] += 1
            else:
                transition_target_distance_Fp_neg[-dist, previous_state] += 1
    previous_state = rdfFp.at[i, 'PHASE']

freq_transition_target_distance_Fp = transition_target_distance_Fp / (transition_target_distance_Fp + transition_target_distance_Fp_neg).sum(axis=0)
freq_transition_target_distance_Fp_neg = transition_target_distance_Fp_neg / (transition_target_distance_Fp + transition_target_distance_Fp_neg).sum(axis=0)
freq_transition_target_distance_Fp_neg = np.flip(freq_transition_target_distance_Fp_neg, axis=0)
freq_transition_target_distance_Fp = np.concatenate((freq_transition_target_distance_Fp_neg[45:, :], freq_transition_target_distance_Fp[:8, :]))


rdfF = rdf.loc[rdf.TEXT_TYPE_2 == 'f']
rdfF = rdfF.reset_index()
transition_target_distance_F = np.zeros((50, 4))
transition_target_distance_F_neg = np.zeros((50, 4))
for i in rdfF.index:
    if rdfF.at[i, 'ISFIRST']:
        subject_name = rdfF.at[i, 'SUBJ_NAME']
        text_name = rdfF.at[i, 'TEXT']
        trial = rdfF[(rdfF['TEXT'] == text_name) & (rdfF['SUBJ_NAME'] == subject_name)]
        trigger_fixations = trial.nlargest(2, ['COS_INST'])
        cos_inst_max_1 = trigger_fixations['COS_INST'].values[0]
        cos_inst_max_2 = trigger_fixations['COS_INST'].values[1]
        jwmax_1 = trigger_fixations['COS_INST'].index[0]
        jwmax_2 = trigger_fixations['COS_INST'].index[1]
        trigger_word_1 = trigger_fixations['FIXED_WORD'].values[0]
        trigger_word_2 = trigger_fixations['FIXED_WORD'].values[1]
        #print('%s, %s - trigger_words : %s %.2f - %s %.2f' % (subject_name, text_name, trigger_word_1, cos_inst_max_1,
        #                                                      trigger_word_2, cos_inst_max_2))
    elif rdfF.at[i, 'PHASE'] != previous_state:
        if cos_inst_max_1 > 0.3:
            if cos_inst_max_2 > 0.3:
                dist1 = abs(jwmax_1 - i)
                dist2 = abs(jwmax_2 - i)
                argdist = np.argmin([dist1, dist2])
                dist = jwmax_1 - i if argdist == 0 else jwmax_2 - i
            else:
                dist = jwmax_1 - i
            if dist >=0:
                transition_target_distance_F[dist, previous_state] += 1
            else:
                transition_target_distance_F_neg[-dist, previous_state] += 1
    previous_state = rdfF.at[i, 'PHASE']

freq_transition_target_distance_F = transition_target_distance_F / (transition_target_distance_F + transition_target_distance_F_neg).sum(axis=0)
freq_transition_target_distance_F_neg = transition_target_distance_F_neg / (transition_target_distance_F + transition_target_distance_F_neg).sum(axis=0)
freq_transition_target_distance_F_neg = np.flip(freq_transition_target_distance_F_neg, axis=0)
freq_transition_target_distance_F = np.concatenate((freq_transition_target_distance_F_neg[45:, :], freq_transition_target_distance_F[:8, :]))


rdfA = rdf.loc[rdf.TEXT_TYPE_2 == 'a']
rdfA = rdfA.reset_index()
transition_target_distance_A = np.zeros((50, 4))
transition_target_distance_A_neg = np.zeros((50, 4))
rdfA['COS_INST_WFREQ'] = ((1 - rdfA['COS_INST']) * np.log(1 / (rdfA['WORD_FREQUENCY'] / rdfA['WORD_FREQUENCY'].max())))
for i in rdfA.index:
    if rdfA.at[i, 'ISFIRST']:
        subject_name = rdfA.at[i, 'SUBJ_NAME']
        text_name = rdfA.at[i, 'TEXT']
        trial = rdfA[(rdfA['TEXT'] == text_name) & (rdfA['SUBJ_NAME'] == subject_name)]
        trigger_fixations = trial.nsmallest(2, ['COS_INST_WFREQ'])
        cos_inst_min_1 = trigger_fixations['COS_INST_WFREQ'].values[0]
        cos_inst_min_2 = trigger_fixations['COS_INST_WFREQ'].values[1]
        jwmin_1 = trigger_fixations['COS_INST_WFREQ'].index[0]
        jwmin_2 = trigger_fixations['COS_INST_WFREQ'].index[1]
        trigger_word_1 = trigger_fixations['FIXED_WORD'].values[0]
        trigger_word_2 = trigger_fixations['FIXED_WORD'].values[1]
        print('%s, %s - trigger_words : %s %.2f - %s %.2f' % (subject_name, text_name, trigger_word_1, cos_inst_min_1,
                                                              trigger_word_2, cos_inst_min_2))
    elif rdfA.at[i, 'PHASE'] != previous_state:
        if jwmin_1 != np.infty:
            dist1 = abs(jwmin_1 - i)
            dist2 = abs(jwmin_2 - i)
            argdist = np.argmin([dist1, dist2])
            dist = jwmin_1 - i if argdist == 0 else jwmin_2 - i
            if dist >=0:
                transition_target_distance_A[dist, previous_state] += 1
            else:
                transition_target_distance_A_neg[-dist, previous_state] += 1
    previous_state = rdfA.at[i, 'PHASE']

freq_transition_target_distance_A = transition_target_distance_A / (transition_target_distance_A + transition_target_distance_A_neg).sum(axis=0)
freq_transition_target_distance_A_neg = transition_target_distance_A_neg / (transition_target_distance_A + transition_target_distance_A_neg).sum(axis=0)
freq_transition_target_distance_A_neg = np.flip(freq_transition_target_distance_A_neg, axis=0)
freq_transition_target_distance_A = np.concatenate((freq_transition_target_distance_A_neg[45:, :], freq_transition_target_distance_A[:8, :]))



rdfM = rdf.loc[rdf.TEXT_TYPE_2 == 'm']
rdfM['COS_INST_WFREQ'] = ((1 - rdfM['COS_INST']) * np.log(1 / (rdfM['WORD_FREQUENCY'] / rdfM['WORD_FREQUENCY'].max())))
rdfM = rdfM.reset_index()

transition_target_distance_M = np.zeros((50, 4))
transition_target_distance_M_neg = np.zeros((50, 4))
for i in rdfM.index:
    if rdfM.at[i, 'ISFIRST']:
        subject_name = rdfM.at[i, 'SUBJ_NAME']
        text_name = rdfM.at[i, 'TEXT']
        trial = rdfM[(rdfM['TEXT'] == text_name) & (rdfM['SUBJ_NAME'] == subject_name)]
        cos_inst_max = trial['COS_INST'].max()
        cos_inst_min = trial['COS_INST_WFREQ'].min()
        jwmin = trial['COS_INST_WFREQ'].idxmin()
        jwmax = trial['COS_INST'].idxmax()
        trigger_word_1 = trial.loc[jwmin, 'FIXED_WORD']
        trigger_word_2 = trial.loc[jwmax, 'FIXED_WORD']
        #print('%s, %s - trigger_words : %s %.2f - %s %.2f' % (subject_name, text_name, trigger_word_1, cos_inst_min,
        #                                                      trigger_word_2, cos_inst_max))
    elif rdfM.at[i, 'PHASE'] != previous_state:
        dist1 = abs(jwmin - i)
        dist2 = abs(jwmax - i)
        argdist = np.argmin([dist1, dist2])
        dist = jwmin - i if argdist == 0 else jwmax - i
        if dist != np.infty:
            if dist >=0:
                transition_target_distance_M[dist, previous_state] += 1
            else:
                transition_target_distance_M_neg[-dist, previous_state] += 1
    previous_state = rdfM.at[i, 'PHASE']

freq_transition_target_distance_M = transition_target_distance_M / (transition_target_distance_M + transition_target_distance_M_neg).sum(axis=0)
freq_transition_target_distance_M_neg = transition_target_distance_M_neg / (transition_target_distance_M + transition_target_distance_M_neg).sum(axis=0)
freq_transition_target_distance_M_neg = np.flip(freq_transition_target_distance_M_neg, axis=0)
freq_transition_target_distance_M = np.concatenate((freq_transition_target_distance_M_neg[45:, :], freq_transition_target_distance_M[:8, :]))

#%%
nrow = freq_transition_target_distance_M.shape[0]
ncol = freq_transition_target_distance_M.shape[1]

dist_fix_max_cos = pd.DataFrame(index=range(0, 208), columns=['Text', 'FromState', 'Dist', 'Freq'])
dist_fix_max_cos.loc[range(nrow*ncol), 'Text'] = 'F'
dist_fix_max_cos.loc[range(nrow*ncol, nrow*ncol*2), 'Text'] = 'A'
dist_fix_max_cos.loc[range(nrow*ncol*2, nrow*ncol*3), 'Text'] = 'M'
dist_fix_max_cos.loc[range(nrow*ncol*3, nrow*ncol*4), 'Text'] = 'F+'

for i in range(16):
    from_state = i % 4
    dist_fix_max_cos.loc[range(nrow*i, nrow*(i+1)), 'FromState'] = from_state

dist_fix_max_cos['Dist'] = range(-5, 8) * 16

for i in range(4):
    dist_fix_max_cos.loc[range(nrow*i, nrow*(i+1)), 'Freq'] = freq_transition_target_distance_F[:, i]

for i in range(4, 8):
    dist_fix_max_cos.loc[range(nrow * i, nrow * (i + 1)), 'Freq'] = freq_transition_target_distance_A[:, i % 4]

for i in range(8, 12):
    dist_fix_max_cos.loc[range(nrow * i, nrow * (i + 1)), 'Freq'] = freq_transition_target_distance_M[:, i % 4]

for i in range(12, 16):
    dist_fix_max_cos.loc[range(nrow * i, nrow * (i + 1)), 'Freq'] = freq_transition_target_distance_Fp[:, i % 4]


dist_fix_max_cos.to_csv(os.path.join(post_proc, 'from_state_neg_dist_fix_max_cos.csv'), \
    index=None, encoding='utf-8')


def redefine_readmode(dataframe, type=0):
    df = dataframe.copy()
    if type == 0:
        df.at[df.WINC > 1, 'READMODE'] = 4
        df.at[df.WINC == 1, 'READMODE'] = 3
        df.at[df.WINC == 0, 'READMODE'] = 2
        df.at[df.WINC == -1, 'READMODE'] = 1
        df.at[df.WINC < -1, 'READMODE'] = 0
    if type == 1:
        df['READMODE'] = 5
        df.at[df.WINC == 2, 'READMODE'] = 4
        df.at[df.WINC == 1, 'READMODE'] = 3
        df.at[df.WINC == 0, 'READMODE'] = 2
        df.at[df.WINC == -1, 'READMODE'] = 1
        df.at[df.WINC < -1, 'READMODE'] = 0
    elif type == 2:
        df['READMODE'] = 6
        df.at[df.WINC == 2, 'READMODE'] = 5
        df.at[df.WINC == 1, 'READMODE'] = 4
        df.at[df.WINC == 0, 'READMODE'] = 3
        df.at[df.WINC == -1, 'READMODE'] = 2
        df.at[df.WINC == -2, 'READMODE'] = 1
        df.at[df.WINC < -2, 'READMODE'] = 0
    elif type == 3:
        df['READMODE'] = 2
        df.at[df.WINC > 2, 'READMODE'] = 4
        df.at[df.WINC == 2, 'READMODE'] = 3
        df.at[df.WINC == -1, 'READMODE'] = 1
        df.at[df.WINC < -1, 'READMODE'] = 0
    df['READMODE'] = pd.Categorical(df.READMODE)
    return df
