# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

from autoreject import AutoReject
import glob2
import mne
import scipy.io


eeg_epochs_paths = glob2.glob('/home/bolivier/PyENE/PyENE-data/eeg-analysis/eeg/**/*/Trig_S1001_XLS/*.set')
mat_eeg_epochs_paths = [path.split('.')[0] + '.mat' for path in eeg_epochs_paths]

for i in range(len(eeg_epochs_paths)):
    mat_data = scipy.io.loadmat(mat_eeg_epochs_paths[i], squeeze_me=True, struct_as_record=False)
    epochs = mne.io.read_epochs_eeglab(eeg_epochs_paths[i])
    ar = AutoReject()
    epochs_clean = ar.fit_transform(epochs)
    mat_data['EEG'].data = epochs_clean.get_data()
    subject_name = eeg_epochs_paths[i].split("/")[-4]
    text_type = eeg_epochs_paths[i].split("/")[-3]
    scipy.io.savemat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/%s-%s.mat'
                     % (subject_name, text_type), mat_data)
