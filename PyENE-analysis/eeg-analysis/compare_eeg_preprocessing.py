# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

import matplotlib.pyplot as plt
import mne
import numpy as np
import os
import pandas as pd
import scipy.io
import sea
from sea import SynchronizedEEGTrial

##%%
channel_info = mne.read_epochs_eeglab(os.path.join('/home/bolivier/PyENE/PyENE/synchronized-eeg-analysis/sample/data', 'synchro_s01_test.set')).info
sea.config.TEXT_COL = 'TEXT_UNIDECODE'

#print(em_data.groupby(['SUBJ_NAME','PHASE']).size())

#%% autorejected, time domain standardized, normalized power spectrum, clustered EEG data
subject_name = 's01'
rois = [['F7', 'F3', 'FC5'], ['F4', 'F8', 'FC6'], ['C3', 'Cz', 'C4'],
            ['CP5', 'P3', 'P7'], ['CP6', 'P4', 'P8'], ['O1', 'Oz', 'O2']]

roi_names = ['Front L', 'Front R', 'Centerline', 'C-Parietal L', 'C-Parietal R', 'Occipital']


eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = np.swapaxes(eeg_data.data, 1, 2) * 1e6
eeg_data.data = np.swapaxes(eeg_data.data , 0, 2)
# em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-randomInit.xlsx')
em_data.loc[em_data['SUBJ_NAME'] == 's08'].groupby(['PHASE']).count()
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    print('trial %s %s' % (subject_name, text_name))
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info.copy())
    if se is not None:
        for roi_id, roi in enumerate(rois):
            se.aggregate_channels(roi, roi_names[roi_id])
        se.subsample_channels([roi_name for roi_name in roi_names])
        mmd = se.compute_modwt(standardize_trial=0)
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
concat_mmd.plot_corr_heatmap(last_x_scales=4)
concat_mmd.plot_var_heatmap(last_x_scales=4, normalize_power_spectrum=True)
# concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4, normalize_power_spectrum=True)

#%%
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = np.swapaxes(eeg_data.data, 1, 2) * 1e6
eeg_data.data = np.swapaxes(eeg_data.data , 0, 2)
# em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-randomInit.xlsx')
em_data.loc[em_data['SUBJ_NAME'] == 's08'].groupby(['PHASE']).count()
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    print('trial %s %s' % (subject_name, text_name))
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info.copy())
    if se is not None:
        mmd = se.compute_modwt(standardize_trial=0)
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], last_x_scales=4, normalize_power_spectrum=True)


#%% S08 ALL TEXTS
subject_name = 's14'
rois = [['F7', 'F3', 'FC5'], ['F4', 'F8', 'FC6'], ['C3', 'Cz', 'C4'],
            ['CP5', 'P3', 'P7'], ['CP6', 'P4', 'P8'], ['O1', 'Oz', 'O2']]
roi_names = ['Front L', 'Front R', 'Centerline', 'C-Parietal L', 'C-Parietal R', 'Occipital']
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-randomInit.xlsx')

concat_mmd = []
for text_type in ['A', 'M', 'F']:
    eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                                '-' + text_type + '.mat', squeeze_me=True, struct_as_record=False)['EEG']
    eeg_data.data = np.swapaxes(eeg_data.data, 1, 2) * 1e6
    eeg_data.data = np.swapaxes(eeg_data.data , 0, 2)
    # em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
    for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                          (em_data['TEXT_TYPE'] == text_type.lower())]['TEXT_UNIDECODE'].unique()):
        em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
        print('trial %s %s' % (subject_name, text_name))
        se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info.copy())
        if se is not None:
            for roi_id, roi in enumerate(rois):
                se.aggregate_channels(roi, roi_names[roi_id])
            se.subsample_channels([roi_name for roi_name in roi_names])
            mmd = se.compute_modwt(standardize_trial=0)
            concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
concat_mmd.plot_corr_heatmap(last_x_scales=4)
concat_mmd.plot_var_heatmap(last_x_scales=4, normalize_power_spectrum=True)

#%%




#%% additive vs gain model
subject_name = 's01'
text_name = 'chasse_oiseaux-f1'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = np.swapaxes(eeg_data.data, 1, 2) * 1e6
eeg_data.data = np.swapaxes(eeg_data.data , 0, 2)
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)

wt = se.compute_modwt()
normalized_wt = wt.copy()
normalized_wt['VALUE'] = wt['VALUE']**2 / (wt['SCALE'].astype(int) + 1)

gain_time_wt = se.compute_modwt(standardize_trial=0)
additive_time_wt = se.compute_modwt(standardize_trial=1)
gain_frequency_wt = se.compute_modwt(standardize_trial=2)
additive_frequency_wt = se.compute_modwt(standardize_trial=3)

normalized_wt.plot_var_heatmap(last_x_scales=4)
normalized_wt.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)

channels = ['Oz', 'Cz', 'Pz', 'Fz', 'F7', 'F8']
for channel in channels:
    fig, axes = plt.subplots(nrows=7, ncols=6, sharex=True)
    for i in range(7):
        axes[i, 0].plot(wt[(wt['CHANNEL'] == channel) & (wt['SCALE'] == i)].VALUE.tolist())
        axes[i, 1].plot(normalized_wt[(normalized_wt['CHANNEL'] == channel) & (normalized_wt['SCALE'] == i)].VALUE.tolist())
        axes[i, 2].plot(gain_time_wt[(gain_time_wt['CHANNEL'] == channel) & (gain_time_wt['SCALE'] == i)].VALUE.tolist())
        axes[i, 3].plot(additive_time_wt[(additive_time_wt['CHANNEL'] == channel) & (additive_time_wt['SCALE'] == i)].VALUE.tolist())
        axes[i, 4].plot(gain_frequency_wt[(gain_frequency_wt['CHANNEL'] == channel) & (gain_frequency_wt['SCALE'] == i)].VALUE.tolist())
        axes[i, 5].plot(additive_frequency_wt[(additive_frequency_wt['CHANNEL'] == channel) & (additive_frequency_wt['SCALE'] == i)].VALUE.tolist())
    plt.show()

#%% Channels clustering vs subsampling
subject_name = 's01'
text_name = 'chasse_oiseaux-f1'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = np.swapaxes(eeg_data.data, 1, 2) * 1e6
eeg_data.data = np.swapaxes(eeg_data.data , 0, 2)
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')

rois = [['F7', 'F3', 'FC5'], ['F4', 'F8', 'FC6'], ['Fz', 'Cz', 'Pz'],
       ['CP5', 'P3', 'P7'], ['CP6', 'P4', 'P8'], ['O1', 'Oz', 'O2']]
se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info.copy())
for roi_id, roi in enumerate(rois):
    se.aggregate_channels(roi, 'roi%d' % roi_id)
se.subsample_channels(['roi%d' % roi_id for roi_id, _ in enumerate(rois)])
wt = se.compute_modwt()
normalized_wt = wt.copy()
normalized_wt['VALUE'] = wt['VALUE'] / (wt['SCALE'].astype(int) + 1)
normalized_wt.plot_corr_heatmap(last_x_scales=4)
normalized_wt.plot_var_heatmap(last_x_scales=4)
normalized_wt.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)

old_cs = ['Oz', 'Cz', 'Pz', 'Fz', 'F7', 'F8']
se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info.copy())
se.subsample_channels(old_cs)
wt = se.compute_modwt()
normalized_wt = wt.copy()
normalized_wt['VALUE'] = wt['VALUE'] / (wt['SCALE'].astype(int) + 1)
normalized_wt.plot_corr_heatmap(last_x_scales=4)
normalized_wt.plot_var_heatmap(last_x_scales=4)
normalized_wt.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)

new_cs = ['F7', 'F8', 'Oz', 'P7', 'P8', 'Oz']
channel_info = mne.read_epochs_eeglab(os.path.join('/home/bolivier/PyENE/PyENE/synchronized-eeg-analysis/sample/data', 'synchro_s01_test.set')).info
se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info.copy())
se.subsample_channels(old_cs)
wt = se.compute_modwt()
normalized_wt = wt.copy()
normalized_wt['VALUE'] = wt['VALUE'] / (wt['SCALE'].astype(int) + 1)
normalized_wt.plot_corr_heatmap(last_x_scales=4)
normalized_wt.plot_var_heatmap(last_x_scales=4)
normalized_wt.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)

#%% autorejected, time domain standardization, normalized spectrum and clustered EEG data
subject_name = 's01'
rois = [['F7', 'F3', 'FC5'], ['F4', 'F8', 'FC6'], ['Fz', 'Cz', 'Pz'],
            ['CP5', 'P3', 'P7'], ['CP6', 'P4', 'P8'], ['O1', 'Oz', 'O2']]
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = np.swapaxes(eeg_data.data, 1, 2) * 1e6
eeg_data.data = np.swapaxes(eeg_data.data , 0, 2)
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    if i > 20:
        break
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    print('trial %s %s' % (subject_name, text_name))
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info.copy())
    if se is not None:
        for roi_id, roi in enumerate(rois):
            se.aggregate_channels(roi, 'roi%d' % roi_id)
        se.subsample_channels(['roi%d' % roi_id for roi_id, _ in enumerate(rois)])
        mmd = se.compute_modwt(standardize_trial=0)
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
concat_mmd.plot_corr_heatmap(last_x_scales=4)
concat_mmd.plot_var_heatmap(last_x_scales=4)
concat_mmd.plot_var_heatmap(last_x_scales=4, normalize_power_spectrum=True)
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4, normalize_power_spectrum=True)


#%% autrejected, standardized and clustering EEG data
subject_name = 's01'
rois = [['F7', 'F3', 'FC5'], ['F4', 'F8', 'FC6'], ['Fz', 'Cz', 'Pz'],
            ['CP5', 'P3', 'P7'], ['CP6', 'P4', 'P8'], ['O1', 'Oz', 'O2']]
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = np.swapaxes(eeg_data.data, 1, 2) * 1e6
eeg_data.data = np.swapaxes(eeg_data.data , 0, 2)
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    if i > 20:
        break
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    print('trial %s %s' % (subject_name, text_name))
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info.copy())
    if se is not None:
        for roi_id, roi in enumerate(rois):
            se.aggregate_channels(roi, 'roi%d' % roi_id)
        se.subsample_channels(['roi%d' % roi_id for roi_id, _ in enumerate(rois)])
        mmd = se.compute_modwt(standardize_trial=2)
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
concat_mmd.plot_corr_heatmap(last_x_scales=4)
concat_mmd.plot_var_heatmap(last_x_scales=4)
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)

#%% 'autorejected' & normalized coeff EEG data
subject_name = 's02'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = np.swapaxes(eeg_data.data, 1, 2) * 1e6
eeg_data.data = np.swapaxes(eeg_data.data , 0, 2)
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    if i > 30:
        break
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    print('trial %s %s' % (subject_name, text_name))
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
    if se is not None:
        mmd = se.compute_modwt()
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
concat_mmd['VALUE'] = concat_mmd['VALUE'] / (concat_mmd['SCALE'].astype(int) + 1)
#concat_mmd = MeltedMODWTDataFrame.concat(concat_mmd)
concat_mmd.plot_corr_heatmap(last_x_scales=4)
concat_mmd.plot_var_heatmap(last_x_scales=4)
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)


subject_name = 's02'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = np.swapaxes(eeg_data.data, 1, 2) * 1e6
eeg_data.data = np.swapaxes(eeg_data.data , 0, 2)
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    if i > 30:
        break
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    print('trial %s %s' % (subject_name, text_name))
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
    if se is not None:
        mmd = se.compute_modwt(standardize_trial=2)
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
#concat_mmd = MeltedMODWTDataFrame.concat(concat_mmd)
concat_mmd.plot_corr_heatmap(last_x_scales=4)
concat_mmd.plot_var_heatmap(last_x_scales=4)
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)


#%% raw EEG data
subject_name = 's01'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/eeg/' + subject_name +
                            '/F/Trig_S1001_XLS/synchro_' + subject_name + '_test.mat',
                            squeeze_me=True, struct_as_record=False)['EEG']
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    if i > 5:
        break
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    print('trial %s %s' % (subject_name, text_name))
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
    if se is not None:
        mmd = se.compute_modwt()
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
#concat_mmd = MeltedMODWTDataFrame.concat(concat_mmd)
concat_mmd.plot_corr_heatmap(last_x_scales=4)
concat_mmd.plot_var_heatmap(last_x_scales=4)
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)

#%% normalized WT and 'autorejected' EEG data
subject_name = 's01'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = np.swapaxes(eeg_data.data, 1, 2) * 1e6
eeg_data.data = np.swapaxes(eeg_data.data , 0, 2)
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    if i > 10:
        break
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    print('trial %s %s' % (subject_name, text_name))
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
    if se is not None:
        mmd = se.compute_modwt(standardize_trial=2)
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
#concat_mmd = MeltedMODWTDataFrame.concat(concat_mmd)
concat_mmd.plot_corr_heatmap(last_x_scales=4)
concat_mmd.plot_var_heatmap(last_x_scales=4)
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)



#%% 'autorejected' EEG data
subject_name = 's01'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = np.swapaxes(eeg_data.data, 1, 2) * 1e6
eeg_data.data = np.swapaxes(eeg_data.data , 0, 2)
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    if i > 30:
        break
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    print('trial %s %s' % (subject_name, text_name))
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
    if se is not None:
        mmd = se.compute_modwt()
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
#concat_mmd = MeltedMODWTDataFrame.concat(concat_mmd)
concat_mmd.plot_corr_heatmap(last_x_scales=4)
concat_mmd.plot_var_heatmap(last_x_scales=4)
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)

##%% standardized EEG data
subject_name = 's01'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/eeg/' + subject_name +
                            '/F/Trig_S1001_XLS/synchro_' + subject_name + '_test.mat',
                            squeeze_me=True, struct_as_record=False)['EEG']
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    if i > 30:
        break
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    print('trial %s %s' % (subject_name, text_name))
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
    if se is not None:
        mmd = se.compute_modwt(standardize_trial=2)
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
#concat_mmd = MeltedMODWTDataFrame.concat(concat_mmd)
concat_mmd.plot_corr_heatmap(last_x_scales=4)
concat_mmd.plot_var_heatmap(last_x_scales=4)
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)


#%% standardized and 'autorejected' EEG data
subject_name = 's01'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = np.swapaxes(eeg_data.data, 1, 2) * 1e6
eeg_data.data = np.swapaxes(eeg_data.data , 0, 2)
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    if i > 30:
        break
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    print('trial %s %s' % (subject_name, text_name))
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
    if se is not None:
        mmd = se.compute_modwt(standardize_trial=2)
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
#concat_mmd = MeltedMODWTDataFrame.concat(concat_mmd)
concat_mmd.plot_corr_heatmap(last_x_scales=4)
concat_mmd.plot_var_heatmap(last_x_scales=4)
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)


#%%
subject_name = 's01'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = eeg_data.data.reshape((30, 10146, 59))
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    if i > 30:
        break
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
    if se is not None:
        mmd = se.compute_modwt(standardize_trial=2)
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
#concat_mmd = MeltedMODWTDataFrame.concat(concat_mmd)
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=False, last_x_scales=4)

#%%
subject_name = 's02'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/eeg/' + subject_name +
                            '/F/Trig_S1001_XLS/synchro_' + subject_name + '_test.mat',
                            squeeze_me=True, struct_as_record=False)['EEG']
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    if i > 30:
        break
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    print('trial %s %s' % (subject_name, text_name))
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
    if se is not None:
        mmd = se.compute_modwt(standardize_trial=2)
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
#concat_mmd = MeltedMODWTDataFrame.concat(concat_mmd)
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True, last_x_scales=4)

#%%
subject_names = ['s01', 's02', 's04', 's06', 's11', 's19', 's20', 's21']
for subject_name in subject_names:
    eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                                '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
    eeg_data.data = eeg_data.data.reshape((30, 10146, eeg_data.data.shape[0]))
    eeg_data.data = np.swapaxes(eeg_data.data, 0, 2)
    em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
    concat_mmd = []
    for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                          (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
        if i > 30:
            break
        em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
        se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
        if se is not None:
            mmd = se.compute_modwt(standardize_trial=2)
            concat_mmd.append(mmd)

    concat_mmd = pd.concat(concat_mmd)
    concat_mmd.channel_info = mmd.channel_info
    #concat_mmd = MeltedMODWTDataFrame.concat(concat_mmd)
    concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=False, last_x_scales=4)

#%%
subject_name = 's02'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = eeg_data.data.reshape((30, 10146, 54))

em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == 'f')]['TEXT_UNIDECODE'].unique()):
    if i > 30:
        break
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
    if se is not None:
        mmd = se.compute_modwt(standardize_trial=2)
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
#concat_mmd = MeltedMODWTDataFrame.concat(concat_mmd)
concat_mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=False, last_x_scales=4)
#%%
subject_name = 's02'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/eeg/' + subject_name +
                            '/F/Trig_S1001_XLS/synchro_' + subject_name + '_test.mat',
                            squeeze_me=True, struct_as_record=False)['EEG']
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
em_data = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT'] == text_name)]
se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
mmd = se.compute_modwt()
mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True)
mmd = se.compute_modwt(standardize_trial=2)
mmd.plot_var_heatmap(last_x_scales=4)
#mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=False)
mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True)
#%%

#%%
subject_name = 's04'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/eeg/' + subject_name +
                            '/F/Trig_S1001_XLS/synchro_' + subject_name + '_test.mat',
                            squeeze_me=True, struct_as_record=False)['EEG']
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
em_data = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT'] == text_name)]
se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
mmd = se.compute_modwt()
mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True)
mmd = se.compute_modwt(standardize_trial=2)
# mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=False)
mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True)

subject_name = 's06'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/eeg/' + subject_name +
                            '/F/Trig_S1001_XLS/synchro_' + subject_name + '_test.mat',
                            squeeze_me=True, struct_as_record=False)['EEG']
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
em_data = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT'] == text_name)]
se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
mmd = se.compute_modwt()
mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True)
mmd = se.compute_modwt(standardize_trial=2)
# mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=False)
mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True)

subject_name = 's19'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/eeg/' + subject_name +
                            '/F/Trig_S1001_XLS/synchro_' + subject_name + '_test.mat',
                            squeeze_me=True, struct_as_record=False)['EEG']
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
em_data = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT'] == text_name)]
se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
mmd = se.compute_modwt()
mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True)
mmd = se.compute_modwt(standardize_trial=2)
# mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=False)
mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True)

subject_name = 's20'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/eeg/' + subject_name +
                            '/F/Trig_S1001_XLS/synchro_' + subject_name + '_test.mat',
                            squeeze_me=True, struct_as_record=False)['EEG']
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-unidecode.xlsx')
em_data = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT'] == text_name)]
se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info)
mmd = se.compute_modwt()
mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True)
mmd = se.compute_modwt(standardize_trial=2)
# mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=False)
mmd.plot_topomap(groupby=['SCALE', 'PHASE'], robust=True)
