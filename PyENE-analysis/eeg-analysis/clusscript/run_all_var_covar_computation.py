# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

import glob2
import matplotlib.pyplot as plt
import mne
import numpy as np
import os
import pandas as pd
import scipy.io
import sea
from sea import SynchronizedEEGTrial

##%%
channel_info = mne.read_epochs_eeglab(os.path.join('/home/bolivier/PyENE/PyENE/synchronized-eeg-analysis/sample/data',
                                                   'synchro_s01_test.set')).info
sea.config.TEXT_COL = 'TEXT_UNIDECODE'

#print(em_data.groupby(['SUBJ_NAME','PHASE']).size())

#%% autorejected, time domain standardized, normalized power spectrum, clustered EEG data
rois = [['F7', 'F3', 'FC5'], ['F4', 'F8', 'FC6'], ['C3', 'Cz', 'C4'],
            ['CP5', 'P3', 'P7'], ['CP6', 'P4', 'P8'], ['O1', 'Oz', 'O2']]

roi_names = ['Front L', 'Front R', 'Centerline', 'C-Parietal L', 'C-Parietal R', 'Occipital']

eeg_epochs_paths = glob2.glob('eeg/**/*/Trig_S1001_XLS/*.mat')

text_type = 'f'
subject_name = 's01'
eeg_data = scipy.io.loadmat('/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/' + subject_name +
                            '-F.mat', squeeze_me=True, struct_as_record=False)['EEG']
eeg_data.data = np.swapaxes(eeg_data.data, 1, 2) * 1e6
eeg_data.data = np.swapaxes(eeg_data.data , 0, 2)
em_data = pd.read_excel('/home/bolivier/PyENE/PyENE-data/em-analysis/data/em-ry35-randomInit.xlsx')
concat_mmd = []
for i, text_name in enumerate(em_data[(em_data['SUBJ_NAME'] == subject_name) &
                                      (em_data['TEXT_TYPE'] == text_type.lower())]['TEXT_UNIDECODE'].unique()):
    em_trial = em_data[(em_data['SUBJ_NAME'] == subject_name) & (em_data['TEXT_UNIDECODE'] == text_name)]
    print('trial %s %s %s' % (text_type, subject_name, text_name))
    se = SynchronizedEEGTrial(eeg_data, em_data, subject_name, text_name, channel_info.copy())
    if se is not None:
        for roi_id, roi in enumerate(rois):
            se.aggregate_channels(roi, roi_names[roi_id])
        se.subsample_channels([roi_name for roi_name in roi_names])
        mmd = se.compute_modwt(standardize_trial=0)
        concat_mmd.append(mmd)

concat_mmd = pd.concat(concat_mmd)
concat_mmd.channel_info = mmd.channel_info
concat_mmd.plot_corr_heatmap(last_x_scales=4)
concat_mmd.plot_var_heatmap(last_x_scales=4, normalize_power_spectrum=True)