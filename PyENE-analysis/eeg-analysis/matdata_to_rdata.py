# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

import matplotlib.pyplot as plt
import re
import glob2
import os
import numpy as np
import scipy.io
import rpy2.robjects as robjects
import mne
import pandas as pd

DATA_PATH = '/home/bolivier/PyENE/PyENE-data/eeg-analysis/eeg'

channel_info = mne.io.read_epochs_eeglab('/home/bolivier/PyENE/PyENE-data/eeg-analysis/eeg/s01/F/Trig_S1001_XLS/synchro_s01_test.set', verbose='CRITICAL').info
ch_names = channel_info['ch_names']
channel_loc = pd.DataFrame(np.matrix([channel['loc'][0:3] for channel in channel_info['chs']]))
channel_loc = channel_loc.rename(index={i:val for i, val in enumerate(ch_names)}, columns = {i:val for i, val in enumerate(["x", "y", "z"])})
channel_loc.to_csv('/home/bolivier/chap3/data/channels.csv', header=True, index=True)


eeg_epochs_paths = glob2.glob(os.path.join(DATA_PATH, '/home/bolivier/PyENE/PyENE-data/eeg-analysis/eeg/**/*/Trig_S1001_XLS/*.mat'))
#eeg_epochs_paths = glob2.glob(os.path.join(DATA_PATH, '/home/bolivier/PyENE/PyENE-data/eeg-analysis/clean-eeg/*.mat'))
for eeg_epochs_path in eeg_epochs_paths:
    subject_name = re.findall('\s*/s[0-9][0-9]\s*', eeg_epochs_path)[0].split('/')[1]
    text_type = re.findall('\s*/[AFM]/\s*', eeg_epochs_path)[0].split('/')[1]
    eeg_data = scipy.io.loadmat(eeg_epochs_path,
                                squeeze_me=True, struct_as_record=False)['EEG']
    if eeg_data.data.shape[2] != len(eeg_data.epoch):
        print('s %s, tt %s SKIPPED' % (subject_name, text_type))
    else:
        for text_id in range(len(eeg_data.epoch)):
            eeg = robjects.r.matrix(robjects.FloatVector(eeg_data.data[:,:,text_id].T.flatten()), nrow=30)
            times = robjects.IntVector(eeg_data.times)
            numlinexls = robjects.IntVector([line  if type(line) == int else -1 for line in eeg_data.epoch[text_id].numlinexls])
            eventlatency = robjects.IntVector(eeg_data.epoch[text_id].eventlatency)
            text_name = eeg_data.epoch[text_id].textname
            print('s %s, tid %d, t %s, tt %s, ' % (subject_name, text_id, text_name, text_type))
            if type(text_name) == str:
                trial = robjects.ListVector({'subject_name': subject_name, 'text_name': text_name, 'text_type': text_type,
                                             'eeg': eeg, 'times': times,
                                             'numlinexls': numlinexls, 'eventlatency': eventlatency})
                robjects.r.assign("eeg_trial", trial)
                robjects.r("save(eeg_trial, file='/home/bolivier/chap3/data/%s_%s_%d.Rdata')" % (subject_name, text_type, text_id))